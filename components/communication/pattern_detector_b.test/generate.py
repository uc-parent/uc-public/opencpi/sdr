#!/usr/bin/env python3

# Generates the input binary file for pattern_detector_b testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for pattern_detector_b testing."""

import os
import random

import numpy as np

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class PatternDetectorGenerator(ocpi_testing.generator.BooleanGenerator):
    """Custom generator of input binaries for pattern detector testing."""

    def __init__(self, pattern, bitmask):
        """Initialise the class.

        Args:
            pattern (int): Value to be detected.
            bitmask (int): Selects which bits to use for pattern matching.
        """
        super().__init__()

        self._pattern = [
            bool(bit) for bit in np.binary_repr(pattern, width=64)]
        self._bitmask = [
            bool(bit) for bit in np.binary_repr(bitmask, width=64)]

        self.CASES["pattern_cross_messages"] = self.pattern_cross_messages
        self.CASES["ignored_pattern_bits"] = self.ignored_pattern_bits

    def typical(self, seed, subcase):
        """Generate a sample message with typical data inputs.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the typical case and the stated subcase.
        """
        messages = super().typical(seed, subcase)
        return self._add_pattern(messages)

    def property(self, seed, subcase):
        """Generate a sample message for property testing.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().property(seed, subcase)
        return self._add_pattern(messages)

    def sample(self, seed, subcase):
        """Generate a sample message for testing.

        Uses the standard BooleanGenerator but ensures the generated message
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().sample(seed, subcase)
        return self._add_pattern(messages)

    def message_size(self, seed, subcase):
        """Generate different size messages for testing.

        Uses the standard BooleanGenerator but ensures the generated messages
        contains the pattern to be detected.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        messages = super().message_size(seed, subcase)
        return self._add_pattern(messages)

    def pattern_cross_messages(self, seed, subcase):
        """Generate messages with pattern split over two sample messages.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)

        # Split the pattern into two and place into two messages
        split_pattern = (self._pattern[0:len(self._pattern)//2],
                         self._pattern[len(self._pattern)//2:])
        stream_data = [self._get_sample_values(2*len(self._pattern)) for
                       _ in range(2)]

        messages = [
            {"opcode": "sample", "data": stream_data[0] + split_pattern[0]}]
        if subcase == "sample_only":
            # stream_only is a valid opcode, but nothing needs to be done
            pass
        elif subcase == "with_time":
            messages.append({
                "opcode": "time",
                "data": random.uniform(self.TIME_MIN,
                                       float(self.TIME_MAX))})
        elif subcase == "with_sample_interval":
            messages.append({
                "opcode": "sample_interval",
                "data": random.uniform(self.SAMPLE_INTERVAL_MIN,
                                       float(self.SAMPLE_INTERVAL_MAX))})
        elif subcase == "with_flush":
            messages.append({"opcode": "flush", "data": None})
        elif subcase == "with_discontinuity":
            messages.append({"opcode": "discontinuity", "data": None})
        elif subcase == "with_metadata":
            messages.append({
                "opcode": "metadata",
                "data": {
                    "id": random.randint(0, self.METADATA_ID_MAX),
                    "value": random.randint(
                        0, self.METADATA_VALUE_MAX)}})
        else:
            raise ValueError(
                f"subcase of {subcase} is not supported for a " +
                "pattern_cross_messages test case")

        messages.append(
            {"opcode": "sample", "data": stream_data[1] + split_pattern[1]})
        return messages

    def ignored_pattern_bits(self, seed, subcase):
        """Generate messages with pattern only match when masked.

        While the mask and pattern property case will test a match, this test
        case is to test when the data does not match the pattern but incorrect
        bits are set to be ignored by the mask.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        data = self._pattern + self._get_sample_values(2*len(self._pattern))

        for index, mask_bit in enumerate(self._bitmask):
            if mask_bit is False:
                data[index] = not data[index]

        return [{"opcode": "sample", "data": data}]

    def _add_pattern(self, messages):
        # For a random position in a random message insert the pattern to be
        # matched against
        pattern_length = len(self._pattern)
        possible_messages = [index for index, message in enumerate(
            messages) if len(message) >= pattern_length]
        if len(possible_messages) > 0:
            modify_message = random.choice(possible_messages)
            pattern_index = random.randint(
                0, len(messages[modify_message]["data"]) - pattern_length)
            messages[modify_message]["data"][
                pattern_index: pattern_index + pattern_length] = pattern

        return messages


arguments = ocpi_testing.get_generate_arguments(
    ["pattern_cross_messages", "ignored_pattern_bits"])

subcase = os.environ["OCPI_TEST_subcase"]
pattern_property = int(os.environ.get("OCPI_TEST_pattern"))
bitmask_property = int(os.environ.get("OCPI_TEST_bitmask"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  pattern_property,
                                  bitmask_property)

generator = PatternDetectorGenerator(pattern_property, bitmask_property)

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
