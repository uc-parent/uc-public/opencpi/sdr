-- crc_generator_c HDL implementation
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.crc_parallel;

architecture rtl of worker is

  -- Function to convert polynomial taps into a std_logic_vector
  function taps_to_poly(taps : in uchar_array_t)
    return std_logic_vector is
    variable poly : std_logic_vector(to_integer(taps(0)) - 1 downto 0);
    variable tap  : std_logic_vector(to_integer(taps(0)) - 1 downto 0);
  begin
    poly := (others => '0');
    for i in 1 to taps'length - 1 loop
      tap := std_logic_vector(
        to_unsigned(1, tap'length) sll to_integer(taps(i)));
      poly := poly or tap;
    end loop;
    return poly;
  end function;

  constant poly_size_c      : integer          := to_integer(polynomial_taps(0));
  constant log2_poly_size_c : integer          := integer(ceil(log2(real(poly_size_c))));
  constant polynomial_c     : std_logic_vector := taps_to_poly(polynomial_taps);
  constant input_reflect_c  : std_logic        := input_reflect;
  constant output_reflect_c : std_logic        := output_reflect;
  constant delay_c          : integer          := 1;

  signal take           : std_logic;
  signal taken          : std_logic;
  signal valid_data     : std_logic;
  signal flush          : std_logic;
  signal discontinuity  : std_logic;
  signal get_next       : std_logic;
  signal crc_available  : std_logic;
  signal crc_reset      : std_logic;
  signal crc            : std_logic_vector(poly_size_c - 1 downto 0);
  signal crc_result     : std_logic_vector(poly_size_c - 1 downto 0);
  signal crc_result_msb : std_logic_vector(7 downto 0);
  signal seed           : std_logic_vector(poly_size_c - 1 downto 0);
  signal final_xor      : std_logic_vector(poly_size_c - 1 downto 0);
  signal hold_count     : unsigned(log2_poly_size_c downto 0);
  signal output_out_reg : worker_output_out_t;

begin

  -- take input when both input and output ports are ready
  take           <= output_in.ready and get_next;
  taken          <= take and input_in.ready;
  input_out.take <= take;

  -- test for valid input
  valid_data <= '1' when take = '1' and input_in.valid = '1' and
                input_in.opcode = char_timed_sample_sample_op_e else '0';

  -- Signal for flush opcode
  flush <= '1' when taken = '1' and
           input_in.opcode = char_timed_sample_flush_op_e else '0';

  -- Signal for discontinuity opcode
  discontinuity <= '1' when taken = '1' and
                   input_in.opcode = char_timed_sample_discontinuity_op_e else '0';

  -- Interface delay module
  -- Delays streaming interface signals
  interface_delay_i : entity work.character_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '0'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => take,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => X"00",
      output_out          => output_out_reg
      );

  -- Decrement hold count and apply backpressure whilst outputting crc and
  -- also indicate when data has been clocked in and crc is available
  hold_counter_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        crc_available <= '0';
        crc_result    <= (others => '0');
        hold_count    <= (others => '0');
      elsif discontinuity = '1' or flush = '1' then
        crc_available <= '0';
        crc_result    <= crc;
        hold_count    <= to_unsigned(poly_size_c / 8, hold_count'length);
      elsif valid_data = '1' then
        crc_available <= '1';
      elsif output_in.ready = '1' and hold_count > 0 then
        hold_count <= hold_count - 1;
        crc_result <= crc_result(crc_result'length - 9 downto 0) & X"00";
      end if;
    end if;
  end process;

  -- Take new data only when hold count is zero
  get_next <= '1' when hold_count = 0 else '0';

  -- crc primitive reset
  crc_reset <= ctl_in.reset or discontinuity or flush or
               (not crc_available and not valid_data);

  -- Convert seed and final_xor into std_logic_vectors
  seed      <= std_logic_vector(resize(props_in.seed, poly_size_c));
  final_xor <= std_logic_vector(resize(props_in.final_xor, poly_size_c));

  -- crc module
  crc_inst : crc_parallel
    generic map (
      polynomial_g     => polynomial_c,
      input_reflect_g  => input_reflect_c,
      output_reflect_g => output_reflect_c
      )
    port map (
      clk       => ctl_in.clk,
      rst       => crc_reset,
      clk_en    => valid_data,
      data_in   => input_in.data,
      seed      => seed,
      final_xor => final_xor,
      crc_out   => crc
      );

  -- Most significant byte of crc_result
  crc_result_msb <=
    crc_result(crc_result'length - 1 downto crc_result'length - 8);

  -- Inject flush signals into output interface
  output_mux_p : process(get_next, output_out_reg, output_in.ready, crc_result_msb,
                         hold_count)
  begin
    -- During normal operation
    output_out <= output_out_reg;
    -- During a discontinuity or flush
    if get_next = '0' then
      output_out.valid       <= '1';
      output_out.byte_enable <= (others => '1');
      output_out.data        <= crc_result_msb;
      output_out.opcode      <= char_timed_sample_sample_op_e;
      if hold_count = 1 then
        output_out.eom <= '1';
      else
        output_out.eom <= '0';
      end if;
    end if;
  end process;

end rtl;
