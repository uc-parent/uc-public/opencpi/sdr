// RCC implementation of bit_stuffer_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include <vector>

#include "bit_stuffer_b-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Bit_stuffer_bWorkerTypes;

class Bit_stuffer_bWorker : public Bit_stuffer_bWorkerBase {
  // Message length limiting variables
  uint16_t index_start = 0;
  std::vector<bool> output_buffer;

  // Initialise input buffer size
  const uint8_t input_buffer_size = BIT_STUFFER_B_CONSECUTIVE_ONES - 1;
  std::vector<bool> input_buffer =
      std::vector<bool>(this->input_buffer_size, 0);

  // Flush variables
  uint8_t flush_length = 0;
  uint8_t data_flushed_flag = false;  // Flags when flush data has been handled,
                                      // and flush opcode should be passed

  // Bit stuffing implementation
  void bit_stuff_data(const std::vector<bool> &data_in,
                      std::vector<bool> *data_out) {
    data_out->resize(0);
    for (uint16_t i = 0; i < data_in.size(); i++) {
      // Insert a zero when input_buffer contains all 1s and input data is a 1
      if (data_in[i] && this->input_buffer ==
                            std::vector<bool>(this->input_buffer.size(), 1)) {
        // Insert a 0 after this sample
        data_out->push_back(1);
        data_out->push_back(0);
        this->input_buffer.push_back(1);
        this->input_buffer.push_back(0);
        this->input_buffer.erase(this->input_buffer.begin(),
                                 this->input_buffer.begin() + 2);
      } else {
        // Don't insert a 0
        data_out->push_back(data_in[i]);
        this->input_buffer.push_back(data_in[i]);
        this->input_buffer.erase(this->input_buffer.begin());
      }
    }
  }

  RCCResult flush_length_written() {
    this->flush_length = properties().flush_length;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      uint16_t length = input.sample().data().size();
      const bool *inData =
          reinterpret_cast<const bool *>(input.sample().data().data());
      bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
      output.setOpCode(Bool_timed_sampleSample_OPERATION);
      // Determine if new message
      if (!this->index_start) {
        // Buffer resultant bit stuffed data in output_buffer
        const std::vector<bool> input_data(inData, inData + length);
        bit_stuff_data(input_data, &this->output_buffer);
      }
      // Limit the maximum message size
      uint16_t length_limited;
      uint16_t length_remaining =
          this->output_buffer.size() - this->index_start;
      if (length_remaining > BIT_STUFFER_B_OCPI_MAX_BYTES_OUTPUT) {
        length_limited = BIT_STUFFER_B_OCPI_MAX_BYTES_OUTPUT;
      } else {
        length_limited = length_remaining;
      }
      // Copy data to the output
      uint16_t index_end = this->index_start + length_limited;
      std::copy(this->output_buffer.begin() + this->index_start,
                this->output_buffer.begin() + index_end, outData);
      this->index_start += length_limited;
      // Determine whether or not all the data is sent this time around
      output.sample().data().resize(length_limited);
      if (this->index_start != this->output_buffer.size()) {
        output.advance(length_limited);
        return RCC_OK;
      } else {
        this->index_start = 0;
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and data
      output.setOpCode(Bool_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Bool_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      if (!this->data_flushed_flag && this->flush_length > 0) {
        // Pass through any data remaining in buffer
        output.setOpCode(Bool_timed_sampleSample_OPERATION);
        bool *outData = reinterpret_cast<bool *>(output.sample().data().data());
        const std::vector<bool> zeros(this->flush_length, 0);
        bit_stuff_data(zeros, &this->output_buffer);
        output.setLength(this->output_buffer.size());
        std::copy(this->output_buffer.begin(), this->output_buffer.end(),
                  outData);
        this->data_flushed_flag = true;
        output.advance();  // Advance output only as no more input data is
                           // needed until after the flush opcode is sent.
        return RCC_OK;
      } else {
        this->data_flushed_flag = false;
        // Pass through flush opcode
        output.setOpCode(Bool_timed_sampleFlush_OPERATION);
        return RCC_ADVANCE;
      }
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Set input buffer to all 0s
      std::fill(this->input_buffer.begin(), this->input_buffer.end(), 0);
      // Pass through discontinuity opcode
      output.setOpCode(Bool_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Bool_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

BIT_STUFFER_B_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
BIT_STUFFER_B_END_INFO
