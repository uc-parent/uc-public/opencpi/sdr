// RCC implementation of symbol_mapper_b_l worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "symbol_mapper_b_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Symbol_mapper_b_lWorkerTypes;

class Symbol_mapper_b_lWorker : public Symbol_mapper_b_lWorkerBase {
  int32_t space_symbol = -1;
  int32_t mark_symbol = +1;
  size_t sent_samples = 0;

  RCCResult start() {
    this->sent_samples = 0;
    return RCC_OK;
  }

  RCCResult space_symbol_written() {
    this->space_symbol = properties().space_symbol;
    return RCC_OK;
  }

  RCCResult mark_symbol_written() {
    this->mark_symbol = properties().mark_symbol;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Bool_timed_sampleSample_OPERATION) {
      return handle_samples();
    } else if (input.opCode() == Bool_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Long_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Long_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Bool_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Long_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }

  RCCResult handle_samples() {
    size_t input_length = input.sample().data().size();
    const RCCBoolean *inData = input.sample().data().data();
    int32_t *outData = output.sample().data().data();

    output.setOpCode(Long_timed_sampleSample_OPERATION);
    size_t max_output_length =
        SYMBOL_MAPPER_B_L_OCPI_MAX_BYTES_OUTPUT / sizeof(*outData);
    inData = inData + this->sent_samples;
    size_t unsent_samples = input_length - this->sent_samples;
    if (unsent_samples <= max_output_length) {
      max_output_length = unsent_samples;
    }

    output.sample().data().resize(max_output_length);
    for (size_t i = 0; i < max_output_length; i++) {
      if (*inData == 0) {
        *outData = this->space_symbol;
      } else {
        *outData = this->mark_symbol;
      }
      inData++;
      outData++;
    }
    this->sent_samples += max_output_length;

    if (this->sent_samples == input_length) {
      // All input data has been processed prepare for next input message
      this->sent_samples = 0;
      return RCC_ADVANCE;
    }

    output.advance(max_output_length);
    return RCC_OK;
  }
};

SYMBOL_MAPPER_B_L_START_INFO

SYMBOL_MAPPER_B_L_END_INFO
