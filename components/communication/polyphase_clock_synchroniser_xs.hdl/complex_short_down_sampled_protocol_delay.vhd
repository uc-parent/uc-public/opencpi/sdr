-- Delay module for Complex short Protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes stream data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes stream data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.polyphase_clock_synchroniser_xs_worker_defs.all;

library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_fifo_v2;
use sdr_interface.sdr_interface.protocol_interface_delay_v2;

entity complex_short_protocol_delay is
  generic (
    data_in_width_g      : integer  := 32;
    max_message_length_g : positive := 4096
    );
  port (
    clk      : in std_logic;
    reset    : in std_logic;
    enable   : in std_logic;            -- high when output is ready
    take_in  : in std_logic;            -- high when data is taken from input
    input_in : in worker_input_in_t;    -- input streaming interface

    group_delay_seconds_in    : in std_logic_vector(31 downto 0);
    group_delay_fractional_in : in std_logic_vector(63 downto 0);

    processed_valid_in  : in  std_logic;
    processed_last_in   : in  std_logic;
    processed_stream_in : in  std_logic_vector(data_in_width_g - 1 downto 0);
    processed_ready_out : out std_logic;

    output_out : out worker_output_out_t  -- output streaming interface
    );
end complex_short_protocol_delay;

architecture rtl of complex_short_protocol_delay is
  constant opcode_width_c : integer := integer(
    ceil(log2(real(complex_short_timed_sample_opcode_t'pos(
      complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := input_in.byte_enable'length;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0))
    return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal pd_take_out   : std_logic;
  signal pd_data_ready : std_logic;

  signal input_opcode : std_logic_vector(opcode_width_c - 1 downto 0);

  signal sample_interval_r : std_logic;
  signal sample_interval   : std_logic_vector(input_in.data'length - 1 downto 0);
  signal input_data        : std_logic_vector(input_in.data'range);

  signal output_valid  : std_logic;
  signal output_data   : std_logic_vector(output_out.data'range);
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);

begin

  -- The sample interval output from this component is always double that of
  -- the input rate (i.e. half as many samples).
  -- As such a multiply by 2, through shifting 1 bit (with a carry from the top
  -- bit of a data word to the bottom of the following data word) is undertaken
  -- The sample interval is sent as | Fractional (64bits) | Integer (32 bits) |
  -- (with the bits split as needed to fit into the data width).
  sample_interval_clk_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' or (input_in.eom = '1' and take_in = '1') or
        input_in.opcode /= complex_short_timed_sample_sample_interval_op_e then
        sample_interval_r <= '0';
      elsif take_in = '1' and input_in.valid = '1' and input_in.opcode = complex_short_timed_sample_sample_interval_op_e then
        sample_interval_r <= input_in.data(input_in.data'high);
      end if;
    end if;
  end process;

  sample_interval <= input_in.data(input_in.data'high-1 downto 0) & sample_interval_r;

  sample_interval_p : process(take_in, input_in.valid, input_in.opcode, input_in.data, sample_interval)
  begin
    if take_in = '1' and input_in.valid = '1' and input_in.opcode = complex_short_timed_sample_sample_interval_op_e then
      input_data <= sample_interval;
    else
      input_data <= input_in.data;
    end if;
  end process;

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_fifo_i : protocol_interface_fifo_v2
    generic map (
      enforce_msg_boundary_g  => true,
      data_in_width_g         => input_in.data'length,
      data_out_width_g        => output_out.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => byte_enable_width_c,
      fifo_depth_g            => 8,
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e),
      metadata_opcode_g       => opcode_to_slv(complex_short_timed_sample_metadata_op_e),
      max_message_length_g    => max_message_length_g
      )
    port map (
      clk     => clk,
      reset   => reset,
      enable  => enable,
      take_in => take_in,

      processed_stream_in => processed_stream_in,
      processed_valid_in  => processed_valid_in,
      processed_end_in    => processed_last_in,
      processed_ready_out => pd_data_ready,
      -- Input interface signals
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_data,
      take_out            => pd_take_out,
      -- Output interface signals
      output_som          => output_out.som,
      output_eom          => output_out.eom,
      output_eof          => output_out.eof,
      output_valid        => output_valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );
  output_out.valid  <= output_valid;
  output_out.opcode <= slv_to_opcode(output_opcode);

  output_out.data <= output_data;

  processed_ready_out <= pd_data_ready and enable;

end rtl;
