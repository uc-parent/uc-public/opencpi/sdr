.. polyphase_clock_synchroniser_s RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _polyphase_clock_synchroniser_s-RCC-worker:


``polyphase_clock_synchroniser_s`` RCC worker
=============================================

Due to the RCC implementation only processing data on each new incoming message, the internal buffers are stored between runs. As such they are stored within the worker class, as a class variable.

Note that when changing the taps, the ``taps_per_branch`` property should be updated before passing in the new ``taps`` array to ensure the input array can be properly bounds-checked.

It is assumed that the incoming sample data is in the range (-1,+1) - so for short values (16bits), these are treated as S0.15 format (i.e. 1 sign bit and 15 fractional bits) and can divided by :math:`2^{15}` to convert to floating point values.

Where conversion from float to integer is undertaken the value is currently rounded to nearest integer before conversion.


Detail
------
.. ocpi_documentation_worker::
