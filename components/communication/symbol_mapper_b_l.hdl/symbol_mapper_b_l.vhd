-- symbol_mapper_b_l HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  constant delay_c   : integer := 0;
  signal symbol_data : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  -- Take input data whenever the output is ready
  input_out.take <= output_in.ready;

  -- This handles the translation of non-sample message data from
  -- the narrow input to wider output.
  -- It has zero delay to match the calculation below.
  interface_delay_i : entity work.bool_to_long_protocol_delay
    generic map (
      delay_g          => delay_c,
      data_in_width_g  => input_in.data'length,
      data_out_width_g => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => symbol_data,
      output_out          => output_out
      );


  -- Perform symbol lookup
  lookup_p : process (input_in.data) is
  begin
    if input_in.data(0) = '0' then
      symbol_data <= std_logic_vector(props_in.space_symbol);
    else
      symbol_data <= std_logic_vector(props_in.mark_symbol);
    end if;
  end process;

end rtl;
