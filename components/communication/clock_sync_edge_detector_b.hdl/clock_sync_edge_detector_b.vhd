-- HDL Implementation of an edge based clock synchroniser
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is
  -- Worker constants
  constant delay_c        : integer := 1;
  -- Input signals
  signal data_in          : std_logic;
  signal data_in_r        : std_logic;
  signal edge_detected    : std_logic;
  signal sample_select    : std_logic;
  signal sample_counter   : unsigned(props_in.samples_per_symbol'length - 1 downto 0);
  -- Interface signals
  signal take             : std_logic;
  signal taken            : std_logic;
  signal input_hold       : std_logic;
  signal input_data_valid : std_logic;
  signal input_data_flush : std_logic;
  signal data_in_r_slv    : std_logic_vector(input_in.data'length - 1 downto 0);

begin

  -- Take input data whenever both the input and the output are ready
  take           <= output_in.ready and not input_hold;
  taken          <= input_in.ready and take;
  input_out.take <= take;

  input_data_valid <= '1' when input_in.opcode = bool_timed_sample_sample_op_e and
                      input_in.valid = '1' and output_in.ready = '1' and input_hold = '0'
                      else '0';

  -- Flush on flush or discontinunity opcode
  input_data_flush <= '1' when (input_in.opcode = bool_timed_sample_flush_op_e or
                                input_in.opcode = bool_timed_sample_discontinuity_op_e) and
                      taken = '1'
                      else '0';

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the clock sync algorithm.
  interface_delay_i : entity work.boolean_down_sampled_protocol_delay
    generic map (
      delay_g         => delay_c,
      data_in_width_g => input_in.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => data_in_r_slv,
      processed_mask_in   => sample_select,
      output_out          => output_out,
      input_hold_out      => input_hold
      );

  -- Input data bit is the LSB
  data_in <= input_in.data(0);

  input_register_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' or input_data_flush = '1' then
        data_in_r <= '0';
      elsif (input_data_valid = '1') then
        -- Everytime a new sample comes in register it
        data_in_r <= data_in;
      end if;
    end if;
  end process;

  data_in_r_slv <= "0000000" & data_in_r;

  -- Goes high whenever an edge is detected in the input data
  edge_detected <= data_in_r xor data_in;

  -- Counts up the current location within a symbol between 0
  -- and samples_per_symbol - 1
  sample_counter_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' or input_data_flush = '1' then
        sample_counter <= (others => '0');
      elsif (input_data_valid = '1') then
        if (edge_detected = '1') then
          -- Reset the sample_counter on a symbol transition (edge)
          -- This aligns the counter the input data stream
          sample_counter <= (others => '0');
        -- Use >= rather than = to prevent counting to a large value
        -- if samples_per_symbol is changed while running.
        elsif sample_counter >= props_in.samples_per_symbol - 1 then
          sample_counter <= (others => '0');
        else
          sample_counter <= sample_counter + 1;
        end if;
      end if;
    end if;
  end process;

  -- High when sampling the input stream
  sample_select <= '1' when sample_counter = props_in.sample_point else '0';

end rtl;
