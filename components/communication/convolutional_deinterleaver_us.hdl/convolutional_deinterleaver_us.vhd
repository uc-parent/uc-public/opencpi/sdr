-- Convolutional De-Interleaver
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------
-- Convolutional De-Interleaver
-------------------------------------------------------------------------------
--
-- Description:
--
-- Implementation of a convolutional de-interleaver.
--
-- The convolutional de-interleaver worker uses shift registers to implement
-- the delay lines.
-- The shift registers were written to infer an SRL.
--
-- The worker allows for setting a port_data_width parameter property to define
-- the widths of the input and output ports.
--
-- References:
-- https://en.wikipedia.org/wiki/Burst_error-correcting_code#Interleaved_codes
-- https://www.etsi.org/deliver/etsi_en/300400_300499/300421/01.01.02_60/en_300421v010102p.pdf

-- LINT EXCEPTION: vhdl_003: -3: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -3: URL cannot be shortened to fit line length

-- TODO: Add marshalling and demarshalling primitves to handle the protocols
-- used for the input and output ports.
-- Some of this code will have to be rewritten when the marshalling primitives
-- are added. Note that only the data associated with the sample opcode is
-- de-interleaved by this worker. All other opcodes pass through this component
-- without any effect.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions
library ocpi;
use ocpi.util.all;

architecture rtl of worker is

  constant N_c              : natural := to_integer(N);
  constant bit_width_c      : natural := to_integer(bit_width);
  signal delay_line_addr_r  : unsigned(width_for_max(N_c-1)-1 downto 0);
  signal shift_regs_addr_r  : unsigned(width_for_max(N_c-2)-1 downto 0);
  signal dout               : std_logic_vector(((N_c-1)*bit_width_c)-1 downto 0);
  signal take               : std_logic;
  signal en                 : std_logic_vector(N_c-2 downto 0);
  signal en_r               : std_logic_vector(N_c-2 downto 0);
  signal samples_vld        : std_logic;
  signal deinterleaved_data : std_logic_vector(bit_width_c-1 downto 0);
  signal output_data        : std_logic_vector(output_out.data'range);

begin

  deinterleaved_data <= input_in.data(bit_width_c-1 downto 0) when (delay_line_addr_r = N_c-1) else
                        dout((bit_width_c*(to_integer(shift_regs_addr_r)+1))-1 downto bit_width_c*to_integer(shift_regs_addr_r));

  -- Remove the below line when the marshalling and demarshalling
  -- primitves are added
  samples_vld <= input_in.valid when (input_in.opcode = ushort_timed_sample_sample_op_e) else '0';

  -- Enable for the delay lines registers
  en <= en_r when (take = '1' and samples_vld = '1') else (others => '0');

  convolutional_delay_lines_inst : entity work.convolutional_delay_lines
    generic map (
      N     => N_c,
      WIDTH => bit_width_c,
      D     => to_integer(D),
      MODE  => '1')
    port map (
      clk  => input_in.clk,
      rst  => input_in.reset,
      en   => en,
      din  => input_in.data(bit_width_c-1 downto 0),
      dout => dout);

  take <= input_in.ready and output_in.ready;

  -- Need to size the output properly depending on the bit width of the data
  -- and the port width
  output_condition : if bit_width_c = port_data_width generate
    output_data     <= deinterleaved_data when (input_in.opcode = ushort_timed_sample_sample_op_e) else input_in.data;
    output_out.data <= output_data;
  end generate output_condition;

  add_zeros_gen : if bit_width_c < port_data_width generate
    output_data(bit_width_c-1 downto 0)              <= deinterleaved_data;
    output_data(output_data'left downto bit_width_c) <= (others => '0');
    output_out.data                                  <= output_data when (input_in.opcode = ushort_timed_sample_sample_op_e) else input_in.data;
  end generate add_zeros_gen;

  input_out.take    <= take;
  output_out.give   <= take;
  output_out.valid  <= input_in.valid and take;
  output_out.som    <= input_in.som;
  output_out.eom    <= input_in.eom;
  output_out.opcode <= input_in.opcode;
  output_out.eof    <= input_in.eof;

  -- Counter used to determine which delay line currently on
  delay_line_addr_counter : process (input_in.clk)
  begin
    if rising_edge(input_in.clk) then
      if input_in.reset = '1' then
        delay_line_addr_r <= (others => '0');
      elsif (take = '1' and samples_vld = '1') then
        if delay_line_addr_r = N_c-1 then
          delay_line_addr_r <= (others => '0');
        else
          delay_line_addr_r <= delay_line_addr_r + 1;
        end if;
      end if;
    end if;
  end process delay_line_addr_counter;

  -- Shifts the enable bit so that each clock cycle,
  -- a different delay line register is enabled.
  -- Enabled only when not on a zero delay delay line.
  en_reg : process (input_in.clk)
  begin
    if rising_edge(input_in.clk) then
      if input_in.reset = '1' then
        en_r <= (en_r'left downto 1 => '0') & '1';
      elsif (take = '1' and samples_vld = '1') then
        if (delay_line_addr_r = N_c-1) then
          en_r <= (en_r'left downto 1 => '0') & '1';
        else
          en_r <= en_r(en_r'left-1 downto 0) & '0';
        end if;
      end if;
    end if;
  end process en_reg;

  -- Counter for indexing output of non zero delay, delay line
  -- shift registers
  shift_regs_addr_counter : process (input_in.clk)
  begin
    if rising_edge(input_in.clk) then
      if input_in.reset = '1' then
        shift_regs_addr_r <= (others => '0');
      elsif (take = '1' and samples_vld = '1') then
        if delay_line_addr_r = N_c-2 then
          shift_regs_addr_r <= (others => '0');
        elsif (delay_line_addr_r < N_c-2) then
          shift_regs_addr_r <= shift_regs_addr_r + 1;
        end if;
      end if;
    end if;
  end process shift_regs_addr_counter;

end rtl;
