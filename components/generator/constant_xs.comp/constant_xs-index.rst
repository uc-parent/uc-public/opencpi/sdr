.. constant_xs documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _constant_xs:


Constant Generator (``constant_xs``)
====================================
Constant generator.

Design
------
Generates a constant stream of samples whose values are set by the ``real_value`` and ``imaginary_value`` properties.

The component can be enabled or disabled during run time. When enabled values are produced whenever the output is ready. When the component is disabled no output values are generated.

The generated output values are settable during run time. There is an option to output a discontinuity message after a change in output value.

The size of the generated stream messages can be set during run time and must be between 1 and ``ocpi_max_bytes_output``/``sample_size_bytes``.

Interface
---------
.. literalinclude:: ../specs/constant_xs-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Will generate messages with sample opcode. Discontinuity opcode messages will only be generated if ``discontinuity_on_value_change`` is set.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../constant_xs.hdl ../constant_xs.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface generator primitive v2 <protocol_interface_generator_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``constant_xs`` are:

 * Setting ``message_length`` to 0 or greater than ``ocpi_max_bytes_output``/``sample_size_bytes`` results in unsupported behaviour.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
