#!/usr/bin/env python3

# Python implementation of constant_s for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of constant for testing."""

import math

import opencpi.ocpi_testing as ocpi_testing


class ConstantS(ocpi_testing.Implementation):
    """Python implementation of constant for testing."""

    def __init__(self, enable, message_length, value,
                 discontinuity_on_value_change):
        """Initialise the constant generator class.

        Args:
            enable (bool): Enables/disables the output.
            message_length (int): Output message length.
            value (int): The output value.
            discontinuity_on_value_change (bool): Sets if a discontinuity
                         opcode is generated when the value property is changed.
        """
        super().__init__(
            enable=enable,
            message_length=message_length,
            value=value,
            discontinuity_on_value_change=discontinuity_on_value_change)

        self.input_ports = []

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, *args):
        """Handle incoming Sample opcode.

        Raises:
            TypeError: Constant generator does not have any input ports.
        """
        raise TypeError("constant_s does not have any input ports")

    def time(self, *args):
        """Handle incoming Time opcode.

        Raises:
            TypeError: Constant generator does not have any input ports.
        """
        raise TypeError("constant_s does not have any input ports")

    def sample_interval(self, *args):
        """Handle incoming Sample Interval opcode.

        Raises:
            TypeError: Constant generator does not have any input ports.
        """
        raise TypeError("constant_s does not have any input ports")

    def flush(self, *args):
        """Handle incoming Flush opcode.

        Raises:
            TypeError: Constant generator does not have any input ports.
        """
        raise TypeError("constant_s does not have any input ports")

    def discontinuity(self, *args):
        """Handle incoming Discontinuity opcode.

        Raises:
            TypeError: Constant generator does not have any input ports.
        """
        raise TypeError("constant_s does not have any input ports")

    def metadata(self, *args):
        """Handle incoming Metadata opcode.

        Raises:
            TypeError: Constant generator does not have any input ports.
        """
        raise TypeError("constant_s does not have any input ports")

    def generate(self, total_output_length):
        """Generate output samples.

        Args:
            total_output_length (int): Number of output samples to generate.

        Returns:
            Formatted messages
        """
        if self.enable:
            number_of_messages = math.ceil(
                total_output_length / self.message_length)
            messages = [{"opcode": "sample",
                         "data": [self.value] * self.message_length}
                        ] * number_of_messages
            return self.output_formatter(messages)
        else:
            return self.output_formatter([])
