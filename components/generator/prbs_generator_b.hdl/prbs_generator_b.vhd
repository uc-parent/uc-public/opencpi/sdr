-- HDL Implementation of PRBS Generator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.prbs_generator;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator_v2;

architecture rtl of worker is

  constant delay_c           : integer := 1;
  constant lfsr_poly_order_c : integer := to_integer(polynomial_taps(0));
  constant opcode_width_c    : integer :=
    integer(ceil(log2(real(bool_timed_sample_opcode_t'pos(bool_timed_sample_opcode_t'high)))));

  function opcode_to_slv(inop : in bool_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(bool_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return bool_timed_sample_opcode_t is
  begin
    return bool_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal lfsr_seed            : std_logic_vector(props_in.initial_seed'length - 1 downto 0);
  signal lfsr_seed_slv        : std_logic_vector(lfsr_poly_order_c - 1 downto 0);
  signal prbs_reset           : std_logic;
  signal prbs_bit, prbs_bit_r : std_logic;
  signal prbs_bit_r_slv       : std_logic_vector(output_out.data'length - 1 downto 0);
  signal prbs_enable          : std_logic;
  signal output_hold          : std_logic;
  signal output_opcode        : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data          : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  -- PRBS generator module held in reset when enable property is 0
  prbs_reset <= '1' when ctl_in.reset = '1' or props_in.enable = '0'
                or ctl_in.is_operating = '0'
                else '0';
  prbs_enable <= output_in.ready and not output_hold;

  lfsr_seed     <= std_logic_vector(props_in.initial_seed);
  lfsr_seed_slv <= lfsr_seed(lfsr_poly_order_c - 1 downto 0);

  prbs_generator_i : prbs_generator
    generic map (
      lfsr_width_g => lfsr_poly_order_c,
      lfsr_tap_1_g => to_integer(polynomial_taps(1)),
      lfsr_tap_2_g => to_integer(polynomial_taps(2)),
      lfsr_tap_3_g => to_integer(polynomial_taps(3)),
      lfsr_tap_4_g => to_integer(polynomial_taps(4)),
      lfsr_tap_5_g => to_integer(polynomial_taps(5))
      )
    port map (
      clk        => ctl_in.clk,
      reset      => prbs_reset,
      clk_en     => prbs_enable,
      invert_i   => props_in.invert_output,
      initial_i  => lfsr_seed_slv,
      data_o     => open,
      data_msb_o => prbs_bit
      );

  output_delay_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if prbs_enable = '1' then
        prbs_bit_r <= prbs_bit;
      end if;
    end if;
  end process;

  prbs_bit_r_slv <= "0000000" & prbs_bit_r;

  interface_generator_i : protocol_interface_generator_v2
    generic map (
      delay_g                 => delay_c,
      data_width_g            => output_out.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => output_out.byte_enable'length,
      max_message_size_g      => to_integer(ocpi_max_bytes_output),
      processed_data_opcode_g => opcode_to_slv(bool_timed_sample_sample_op_e),
      discontinuity_opcode_g  => opcode_to_slv(bool_timed_sample_discontinuity_op_e)
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_in.ready,
      discontinuity_trigger => '0',
      generator_enable      => '1',
      generator_reset       => prbs_reset,
      processed_stream_in   => prbs_bit_r_slv,
      message_length        => props_in.message_length,
      output_hold           => output_hold,
      output_som            => open,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_opcode,
      output_data           => output_data
      );

  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
