.. frequency_modulator_l_xs HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _frequency_modulator_l_xs-HDL-worker:


``frequency_modulator_l_xs`` HDL Worker
=======================================
HDL implementation which uses the CORDIC algorithm for sine and cosine approximations.

Detail
------
The frequency modulator is implemented as a numerically controlled oscillator. The input signal is used as the step size in a phase accumulator unit which generates a sawtooth waveform between :math:`0` and :math:`2^{32}-1`. The frequency of the sawtooth wave determines the frequency of the output waveform.

The sawtooth waveform is input into a phase-to-amplitude converter (PAC) which generates a complex sinusoid at the desired frequency. The PAC is implemented using 16 iterations of the CORDIC (Coordinate Rotation Digital Computer) algorithm to approximate the sine and cosine of the input phase. The sine and cosine are combined together to generate a complex short output signal.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilization::
