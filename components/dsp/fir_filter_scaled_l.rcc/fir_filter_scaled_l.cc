// RCC implementation of fir_filter_scaled_l worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../common/fir/fir_core.hh"
#include "../../math/common/time_utils.hh"
#include "../../math/common/128bit.hh"
#include "fir_filter_scaled_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fir_filter_scaled_lWorkerTypes;

class Fir_filter_scaled_lWorker : public Fir_filter_scaled_lWorkerBase {
  // fir_core <SAMPLE_TYPE, ACCUMULATOR_TYPE>
  // Note : The ACCUMULATOR_TYPE needs to be sized appropriately to allow the
  // roll over of any internal mathematical calculations. The size of the input
  // data or the taps can require an internal accumulator size 4 times the
  // size of the SAMPLE_TYPE.
  // Use 32 bit sample/output, 128 bit working size
  fir_core<int32_t, sdr_math::int128_t> fir{};

  static const uint8_t number_of_taps = FIR_FILTER_SCALED_L_NUMBER_OF_TAPS;

  // Local copy of the group_delay properties
  uint32_t group_delay_seconds = 0;
  uint64_t group_delay_fractional = 0;

  // Tracks how much data has been flushed when flush length is too long to be
  // contained within a single message
  size_t flushed_data_length = 0;

  // Only flush buffers if data has been received.
  bool data_received = false;

  RCCResult start() {
    if (this->number_of_taps == 0) {
      setError("Number of taps must be > 0");
      return RCC_FATAL;
    }

    // Convert properties().rounding_type into dsp enum types
    // Default is convergent rounding with wrapping.
    sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    sdr_math::overflow_type overflow = sdr_math::overflow_type::MATH_WRAP;

    Rounding_type property_value = properties().rounding_type;

    if (property_value == ROUNDING_TYPE_HALF_EVEN) {
      rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    } else if (property_value == ROUNDING_TYPE_HALF_UP) {
      rounding = sdr_math::rounding_type::MATH_HALF_UP;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE_WITH_SATURATION) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
      overflow = sdr_math::overflow_type::MATH_SATURATE;
    }

    this->fir.set(this->number_of_taps, properties().taps, rounding, overflow);
    return RCC_OK;
  }

  // Notification that scale_factor property has been written
  RCCResult scale_factor_written() {
    this->fir.set_scale_factor(properties().scale_factor);
    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() {
    this->fir.set_taps(this->number_of_taps, properties().taps);
    return RCC_OK;
  }

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_fractional = properties().group_delay_fractional;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Long_timed_sampleSample_OPERATION) {
      const size_t length = input.sample().data().size();
      const int32_t *input_data = input.sample().data().data();

      if (length > 0) {
        output.setOpCode(Long_timed_sampleSample_OPERATION);
        output.sample().data().resize(length);

        this->fir.do_work(input_data, length, output.sample().data().data());
        this->data_received = true;
      }

      return RCC_ADVANCE;
    }
    if (input.opCode() == Long_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data - after removing group delay
      output.setOpCode(Long_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      time_utils::subtract(&output.time().seconds(), &output.time().fraction(),
                           this->group_delay_seconds,
                           this->group_delay_fractional);
      return RCC_ADVANCE;
    }
    if (input.opCode() == Long_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    }
    if (input.opCode() == Long_timed_sampleFlush_OPERATION) {
      if (this->flushed_data_length < this->number_of_taps &&
          this->data_received) {

        // When a flush is requested input the set number of zeros, but need to
        // limit to maximum amount of data that will fit into a single message
        // (ocpi.comp.sdr limit for long samples in message is given by
        // FIR_FILTER_SCALED_L_OCPI_MAX_BYTES_OUTPUT/sizeof(int32_t))
        constexpr size_t max_length =
            (FIR_FILTER_SCALED_L_OCPI_MAX_BYTES_OUTPUT / sizeof(int32_t));
        const int32_t zero_input_data[max_length] = {0};

        const size_t length = std::min(
            max_length, (this->number_of_taps - this->flushed_data_length));

        output.setOpCode(Long_timed_sampleSample_OPERATION);
        output.sample().data().resize(length);

        this->fir.do_work(zero_input_data, length,
                          output.sample().data().data());

        this->flushed_data_length += length;
        output.advance();
        return RCC_OK;
      }
      // Zeros have been flushed, now pass through flush opcode
      output.setOpCode(Long_timed_sampleFlush_OPERATION);
      this->flushed_data_length = 0;
      this->data_received = false;
      return RCC_ADVANCE;
    }
    if (input.opCode() == Long_timed_sampleDiscontinuity_OPERATION) {
      this->fir.reset();
      // Pass through discontinuity opcode
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      this->data_received = false;
      return RCC_ADVANCE;
    }
    if (input.opCode() == Long_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Long_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    }

    setError("Unknown OpCode Received");
    return RCC_FATAL;
  }
};

FIR_FILTER_SCALED_L_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FIR_FILTER_SCALED_L_END_INFO
