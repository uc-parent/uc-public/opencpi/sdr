#!/usr/bin/env python3

# Runs checks for fir_filter_xf testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verification script for fir_filter_xf implementation."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from fir_filter import FirFilter

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case()
log_level = int(os.environ.get("OCPI_LOG_LEVEL", 0))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logging.basicConfig(
    filename=f"{test_id}.fir_filter_xf.python.log",
    filemode="w",
    level=verify_log_level)

number_of_taps = int(os.environ.get("OCPI_TEST_number_of_taps"))
group_delay_seconds = int(os.environ.get("OCPI_TEST_group_delay_seconds", 0))
group_delay_fractional = int(os.environ.get("OCPI_TEST_group_delay_fractional",
                                            0))

taps_raw_input = os.environ.get("OCPI_TEST_taps").split(",")
# OCPI will trim multiple consecutive zeros from the end of the array, and will
# also add an extra 0 at the end. Initialise tap array to enough values (zeros)
# and then populate with given values.
filter_taps = [0] * number_of_taps
for i, x in enumerate(taps_raw_input):
    if i < number_of_taps:
        filter_taps[i] = float(x)
    else:
        if float(x) != 0:
            raise ValueError(
                "Taps contains too many values (expected: {}, got: {})\n{}"
                .format(number_of_taps, len(taps_raw_input), taps_raw_input))


fir_filter_implementation = FirFilter(
    taps=filter_taps,
    group_delay_seconds=group_delay_seconds,
    group_delay_fractional=group_delay_fractional)

# Check the result
verifier = ocpi_testing.Verifier(fir_filter_implementation)
verifier.set_port_types(["complex_float_timed_sample"],
                        ["complex_float_timed_sample"],
                        ["relative"])

if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)
