.. windower_l_proxy documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _windower_l_proxy:


Windower Proxy (``windower_l_proxy``)
=====================================
A proxy worker to provide coefficients for the :ref:`windower_l` component.

Design
------
During start, and when the ``window_type`` property is written, this proxy worker calculates the coefficients for a Hann, Blackman-Harris or Hamming window and passes the data on to a window slave worker.

The window type and length is set by the ``window_type`` and ``window_length`` properties, respectively.

The window proxy worker calculates ``window_length`` coefficients for the selected window type. Each coefficient is converted to an integer using the following formula:

:math:`Integer \space Coefficient = truncate(Max \space Integer \times Coefficient)`

``Integer Coefficient`` is set to ``Max Integer`` if it is greater than ``Max Integer``.

These coefficients are used to set the window slave worker's ``coefficients`` property.


For Hann windows the coefficient formula is:

.. math::
   (\sin(\pi * n) / \texttt{window_length}) ^ 2
   :label: windower_l_proxy-hann-equation

For Blackman-Harris windows the coefficient formula is:

.. math::
   a0 - (a1 * cos((2 * \pi * n) / \texttt{window_length})) + (a2 * cos((4 * \pi * n) / \texttt{window_length})) - (a3 * cos((6 * \pi * n) / \texttt{window_length}))
   :label: windower_l_proxy-blackman-harris-equation

Where:

* :math:`a0 = 0.35875`

* :math:`a1 = 0.48829`

* :math:`a2 = 0.14128`

* :math:`a3 = 0.01168`

For Hamming windows the coefficient formula is:

.. math::
   0.54 - (0.46 * cos((2 * \pi * n) / \texttt{window_length}))
   :label: windower_l_proxy-hamming-equation

Interface
---------
.. literalinclude:: ../specs/windower_proxy-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
N/A

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::
   :component_spec: ../specs/windower_proxy-spec.xml

Ports
~~~~~
None

Implementations
---------------
.. ocpi_documentation_implementations:: ../windower_l_proxy.rcc

Example Application
-------------------
.. literalinclude:: ../windower_l.comp/example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Windower component <windower_l>`

Limitations
-----------
None

Testing
-------
None
