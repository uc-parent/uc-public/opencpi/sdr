// RCC implementation of frequency_modulator_l_xs worker
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>

#include "frequency_modulator_l_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Frequency_modulator_l_xsWorkerTypes;

class Frequency_modulator_l_xsWorker
    : public Frequency_modulator_l_xsWorkerBase {
  uint16_t modulator_amplitude = 0;
  double_t phase = 0;
  // This component matches the behavior of another worker which uses the CORDIC
  // algorithm to calculate sine and cosine. The CORDIC algorithm scales the
  // output by a fixed constant. As such, we scale the amplitude of sine and
  // cosine to match that of the output from the worker using the CORDIC
  // algorithm.
  const double_t cordic_gain = 1.646760258;

  // Notification that modulator_amplitude property has been written
  RCCResult modulator_amplitude_written() {
    this->modulator_amplitude = properties().modulator_amplitude;

    if (this->modulator_amplitude > 19872) {
      setError("modulator amplitude must be less than 19872");
      return RCC_FATAL;
    }
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Long_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const int32_t *inData =
          reinterpret_cast<const int32_t *>(input.sample().data().data());
      Complex_short_timed_sampleSampleData *outData =
          output.sample().data().data();

      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);

      for (size_t i = 0; i < length; i++) {
        // Apply frequency modulation to input data
        outData->real = static_cast<int16_t>(
            cordic_gain * this->modulator_amplitude * cos(this->phase));
        outData->imaginary = static_cast<int16_t>(
            cordic_gain * this->modulator_amplitude * sin(this->phase));

        double_t delta = 2 * M_PI * *inData / ((uint64_t)1 << 32);
        this->phase = std::fmod(this->phase + delta, 2 * M_PI);

        // Wrap around handler for negative values, as fmod calculates remainder
        // instead of modulus
        if (this->phase < 0) {
          this->phase += (2 * M_PI);
        }

        outData++;
        inData++;
      }

      output.sample().data().resize(length);
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Long_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

FREQUENCY_MODULATOR_L_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FREQUENCY_MODULATOR_L_XS_END_INFO
