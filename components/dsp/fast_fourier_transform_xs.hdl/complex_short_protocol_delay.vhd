-- Delay module for complex short protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Creates a variable length delay pipeline for all interface signals in order
-- to align interface signals with a module that processes sample data.
-- delay_g should be set to the delay in clock cycles of the module that
-- processes sample data.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.fast_fourier_transform_xs_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_fifo_v2;

entity complex_short_protocol_delay is
  generic (
    enforce_msg_boundary_g : boolean  := true;
    data_in_width_g        : natural  := 32;
    fifo_depth_g           : positive := 8
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    enable              : in  std_logic;
    take_in             : in  std_logic;
    drop_message        : in  std_logic;
    drop_last_message   : in  std_logic;
    input_in            : in  worker_input_in_t;
    -- Connect output from data processing module.
    processed_stream_in : in  std_logic_vector(data_in_width_g - 1 downto 0);
    processed_valid_in  : in  std_logic;
    processed_end_in    : in  std_logic;
    processed_ready_out : out std_logic;
    fifo_full_error     : out std_logic;
    take_out            : out std_logic;
    output_out          : out worker_output_out_t
    );
end complex_short_protocol_delay;

architecture rtl of complex_short_protocol_delay is

  constant opcode_width_c : integer := integer(ceil(log2(real(
    complex_short_timed_sample_opcode_t'pos(complex_short_timed_sample_opcode_t'high)))));
  constant byte_enable_width_c : integer := integer(ceil(log2(real(input_in.byte_enable'length)))) + 1;

  function opcode_to_slv(inop : in complex_short_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(complex_short_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return complex_short_timed_sample_opcode_t is
  begin
    return complex_short_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal input_opcode  : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_data   : std_logic_vector(data_in_width_g - 1 downto 0);

begin

  input_opcode <= opcode_to_slv(input_in.opcode);

  protocol_delay_i : protocol_interface_fifo_v2
    generic map (
      enforce_msg_boundary_g  => enforce_msg_boundary_g,
      data_in_width_g         => data_in_width_g,
      data_out_width_g        => data_in_width_g,
      opcode_width_g          => opcode_width_c,
      fifo_depth_g            => fifo_depth_g,
      byte_enable_width_g     => byte_enable_width_c,
      processed_data_opcode_g => opcode_to_slv(complex_short_timed_sample_sample_op_e)
      )
    port map (
      clk                 => clk,
      reset               => reset,
      enable              => enable,
      drop_last_message   => drop_last_message,
      take_in             => take_in,
      take_out            => take_out,
      drop_message        => drop_message,
      fifo_full_error     => fifo_full_error,
      processed_stream_in => processed_stream_in,
      processed_valid_in  => processed_valid_in,
      processed_ready_out => processed_ready_out,
      processed_end_in    => processed_end_in,
      input_som           => input_in.som,
      input_eom           => input_in.eom,
      input_eof           => input_in.eof,
      input_valid         => input_in.valid,
      input_ready         => input_in.ready,
      input_byte_enable   => input_in.byte_enable,
      input_opcode        => input_opcode,
      input_data          => input_in.data,
      output_som          => output_out.som,
      output_eom          => output_out.eom,
      output_eof          => output_out.eof,
      output_valid        => output_out.valid,
      output_give         => output_out.give,
      output_byte_enable  => output_out.byte_enable,
      output_opcode       => output_opcode,
      output_data         => output_data
      );

  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
