-- Fast Fourier Transform Worker
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

architecture rtl of worker is
  -- The delay through the component has been found through analysis with
  -- differing fft lengths. This is probably caused by the following:
  -- - 2 * fft_length Loading and parsing of the FFT core
  -- - log2(fft_length)*20 Butterfly addition, and recursive DFT folding
  -- - 154 constant due to the values being relative to an fft length of 1024
  -- As this is variable on FFT length, a non-deterministic protocol delay
  -- method is used within this component.

  signal pd_take           : std_logic;
  signal in_other          : std_logic;
  signal discontinuity     : std_logic;
  signal discontinuity_r   : std_logic;
  signal drop_message      : std_logic;
  signal drop_last_message : std_logic;
  signal ip_reset          : std_logic;
  signal fft_enable        : std_logic;

  signal input_out_internal      : worker_input_out_t;
  signal input_interface_limited : worker_input_in_t;
  signal metadata_take           : std_logic;
  signal processed_ready_out     : std_logic;

  -- Signals for output of FFT
  signal output_0bin        : std_logic;
  signal fft_output_data    : std_logic_vector(output_out.data'range);
  signal processed_valid_in : std_logic;
  signal processed_end_in   : std_logic;
  signal pd_output_out      : worker_output_out_t;

begin

  in_other <= '1' when input_interface_limited.opcode /= complex_short_timed_sample_sample_op_e and input_interface_limited.ready = '1' else '0';

  ------
  -- Discontinuity registering, and data resetting.
  -- The data path is reset on a discontinuity flag.
  discontinuity_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if pd_take = '1' then
        discontinuity_r <= discontinuity;
      end if;
    end if;
  end process;
  ip_reset <= discontinuity_r or ctl_in.reset;

  -- Messages should be limited to ensure that the protocol delay has EOM flags
  -- in the expected placed (either per FFT buffer, or OCPI_MAX_BYTES, which
  -- ever is the smallest)
  message_limit_i : entity work.complex_short_message_limit
    generic map (
      data_in_width_g      => 32,
      max_message_length_g => to_integer(fft_length),
      metadata_depth_g     => to_integer(metadata_fifo_size)
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      data_reset          => ip_reset,
      enable              => pd_take,
      input_in            => input_in,
      processed_end_in    => processed_end_in,
      processed_ready_out => processed_ready_out,
      output_out          => input_interface_limited,
      input_out           => input_out_internal,
      discontinuity       => discontinuity,
      drop_message        => drop_message
      );
  drop_last_message <= drop_message;

  input_out  <= input_out_internal;
  fft_enable <= pd_take and processed_ready_out and (not in_other);

  -- FFT Sample processing (the transform itself)
  fft_samples_i : entity work.fft_sample_processing
    generic map (
      data_width_g  => 32,
      inverse_fft_g => to_boolean(inverse_fft),
      fft_length_g  => to_integer(fft_length)
      )
    port map (
      clk           => ctl_in.clk,
      reset         => ip_reset,
      enable        => fft_enable,
      discontinuity => discontinuity,
      gain_factor   => props_in.gain_factor,
      input_in      => input_interface_limited,
      fft_0bin      => output_0bin,

      output_data          => fft_output_data,
      processed_data_valid => processed_valid_in,
      processed_end_in     => processed_end_in,
      processed_ready_out  => processed_ready_out
      );

  delay_i : entity work.complex_short_protocol_delay
    generic map (
      enforce_msg_boundary_g => true,
      fifo_depth_g           => to_integer(opcode_fifo_depth)
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => metadata_take,
      take_in             => metadata_take,
      take_out            => pd_take,
      drop_message        => '0',
      drop_last_message   => drop_last_message,
      fifo_full_error     => ctl_out.error,
      input_in            => input_interface_limited,
      -- Connect output from data processing module.
      processed_stream_in => fft_output_data,
      processed_valid_in  => processed_valid_in,
      processed_end_in    => processed_end_in,
      processed_ready_out => processed_ready_out,
      output_out          => pd_output_out
      );

  meta_gen : if indicate_start_of_fft_block = '1' generate
    metadata_inserter_i : entity work.fft_metadata_inserter
      port map (
        clk      => ctl_in.clk,
        reset    => ctl_in.reset,
        enable   => output_in.ready,
        take_in  => output_in.ready,
        take_out => metadata_take,

        increment_fft_block_value => props_in.increment_fft_block_value,
        metadata_id_written       => props_in.start_of_fft_block_id_written,
        metadata_value_written    => props_in.start_of_fft_block_value_written,
        metadata_id               => std_logic_vector(props_in.start_of_fft_block_id),
        metadata_value            => std_logic_vector(props_in.start_of_fft_block_value),

        fft_0bin => output_0bin,
        input_in => pd_output_out,

        output_in  => output_in,
        output_out => output_out
        );
  end generate;

  non_meta_gen : if indicate_start_of_fft_block = '0' generate
    output_out    <= pd_output_out;
    metadata_take <= output_in.ready;
  end generate;

end rtl;
