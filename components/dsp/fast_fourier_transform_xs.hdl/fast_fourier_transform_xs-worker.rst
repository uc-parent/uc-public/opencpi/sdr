.. fast_fourier_transform_xs HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fast_fourier_transform_xs-HDL-worker:


``fast_fourier_transform_xs`` HDL Worker
========================================

Detail
------
.. ocpi_documentation_worker::

The HDL version of the FFT worker is implemented using the block-based approach to handle non-sample opcodes received during the processing of an FFT frame. A state machine inside the message limiter component controls the flow of messages and data according to the opcode handling section of the FFT specification.

When a frame is not being processed or only sample data is present on the input port, the message limiter remains in the pass-through state and simply forwards all messages to the output buffer.

When a time or metadata message is encountered in the middle of processing an FFT frame, the frame is allowed to be completed before these are forwarded to the output buffer. This is handled by using flags to indicate that one of these messages has been encountered, so that they can be passed through once a frame has completed.

Timestamp values are calculated using a timestamp recovery primitive, that can update the timestamp value according to the current sample interval value. However only the most recent timestamp message is forwarded to the output if the timestamp changes twice within a single FFT frame.

Once all outstanding messages have been sent to the output buffer, the message limiter always returns to the pass-through state to allow new messages to pass to the output buffer.


Utilisation
-----------
.. ocpi_documentation_utilization::
