-- Message Limiter for complex short protocol
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.fast_fourier_transform_xs_worker_defs.all;
library sdr_interface;
use sdr_interface.sdr_interface.timestamp_recovery;

entity complex_short_message_limit is
  generic (
    data_in_width_g      : integer := 32;
    max_message_length_g : integer := 1024;
    metadata_depth_g     : integer := 4
    );
  port (
    clk                 : in  std_logic;
    reset               : in  std_logic;
    data_reset          : in  std_logic;
    enable              : in  std_logic;          -- High when output is ready
    processed_ready_out : in  std_logic;
    input_in            : in  worker_input_in_t;  -- Input streaming interface
    processed_end_in    : in  std_logic;
    flush_length        : out unsigned(integer(ceil(log2(real(max_message_length_g))))-1 downto 0);
    discontinuity       : out std_logic;
    drop_message        : out std_logic;
    input_out           : out worker_input_out_t;
    output_out          : out worker_input_in_t   -- Output streaming interface
    );
end complex_short_message_limit;

architecture rtl of complex_short_message_limit is
  constant timestamp_unit_width_c     : integer := 32;
  constant timestamp_fraction_width_c : integer := 64;
  constant message_counter_size_c     : integer := integer(ceil(log2(real(max_message_length_g))));

  type state_t is (passthrough_message_s, get_samples_s, flush_samples_s,
                   flush_s, discontinuity_s, sample_interval_send_s,
                   timestamp_send_s, metadata_send_s);

  signal current_state         : state_t;
  signal timestamp_valid       : std_logic;
  signal sample_interval_valid : std_logic;
  signal data_valid            : std_logic;
  signal take_opcode           : std_logic;
  signal take                  : std_logic;
  signal discontinuity_i       : std_logic;
  signal drop_message_i        : std_logic;
  signal drop_next_message     : std_logic;
  signal metadata_seen         : std_logic;
  signal sample_interval_seen  : std_logic;
  signal timestamp_seen        : std_logic;
  signal monitor_end_in        : std_logic;
  signal time_after_flush      : std_logic;
  signal frame_empty_on_eom    : std_logic;

  -- Timestamp signals
  signal timestamp_units          : std_logic_vector(timestamp_unit_width_c - 1 downto 0);
  signal timestamp_fraction       : std_logic_vector(timestamp_fraction_width_c - 1 downto 0);
  signal timestamp_units_r        : std_logic_vector(timestamp_unit_width_c - 1 downto 0);
  signal timestamp_fraction_r     : std_logic_vector(timestamp_fraction_width_c - 1 downto 0);
  signal sample_interval_units    : std_logic_vector(timestamp_unit_width_c - 1 downto 0);
  signal sample_interval_fraction : std_logic_vector(timestamp_fraction_width_c - 1 downto 0);

  -- Metadata is 32bits ID, 64 bits value.
  type metadata_array_t is array (metadata_depth_g-1 downto 0) of std_logic_vector(95 downto 0);
  signal metadata_fifo            : metadata_array_t := (others => (others => '0'));
  signal metadata_value           : std_logic_vector(95 downto 0);
  signal metadata_value_r         : std_logic_vector(95 downto 0);
  signal output_metadata_value    : std_logic_vector(95 downto 0);
  signal metadata_depth           : integer range 0 to metadata_depth_g;
  signal metadata_fifo_empty      : std_logic;
  signal metadata_fifo_full       : std_logic;
  signal metadata_message_valid   : std_logic;
  signal metadata_msg_in_progress : std_logic;

  -- The FFT has up-to 4 frames being processed at any one time.
  signal frame_counter  : unsigned(2 downto 0);
  signal sample_counter : unsigned(message_counter_size_c - 1 downto 0);

  signal message_eom         : std_logic;
  signal message_in_progress : std_logic;
  signal message_sr          : std_logic_vector(3 downto 0);
  signal output_eom_sent     : std_logic;
  signal output_opcode       : complex_short_timed_sample_opcode_t;
  signal output_ready        : std_logic;
  signal output_valid        : std_logic;
  signal output_byte_enable  : std_logic_vector(input_in.byte_enable'range);
  signal output_som          : std_logic;
  signal output_som_r        : std_logic;
  signal output_eom          : std_logic;
  signal output_eof          : std_logic := '0';
  signal output_data         : std_logic_vector(input_in.data'range);

begin

  -- Note this does not use the primitive: message_length_limiter, due to that
  -- not handling the EOF and enable flags in the required fashion.

  ------
  -- Counters:
  -- * Frame counter counts processed FFT frames, incrementing on EOM being
  --   outputted, and decrementing on processed_end_in being asserted (the FFT
  --   completing the outputting of a frame).
  -- * Message counter counts the samples within a single frame, and asserts
  --   EOM on the completion of an FFT frame (unless the FFT is being flushed
  --   of existing frames).
  frame_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' then
        frame_counter  <= (others => '0');
        monitor_end_in <= '0';
      elsif enable = '1' then
        -- Add a frame for each completion of a message counter
        -- Subtract a frame for each completion of a processed_end
        -- (fft output completion)
        if frame_counter /= 0 and processed_end_in = '1' then
          frame_counter <= frame_counter - 1;
        elsif frame_counter = 1 and discontinuity_i = '1' then
          monitor_end_in <= '1';
        elsif (frame_counter < 7 and sample_counter = 1 and
               output_valid = '1' and discontinuity_i = '0' and
               output_opcode = complex_short_timed_sample_sample_op_e) then
          frame_counter <= frame_counter + 1;
        end if;
      end if;
    end if;
  end process;

  message_counter_p : process (clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' then
        sample_counter <= (others => '0');
        message_eom    <= '0';

      elsif enable = '1' and processed_ready_out = '1' then
        if output_valid = '1' and output_opcode = complex_short_timed_sample_sample_op_e then
          sample_counter <= sample_counter - 1;

          if sample_counter = 2 and input_in.opcode /= complex_short_timed_sample_discontinuity_op_e then
            message_eom <= '1';
          else
            message_eom <= '0';
          end if;
        end if;
      end if;
    end if;
  end process;
  message_in_progress <= '1' when sample_counter /= 0 else '0';
  flush_length        <= sample_counter;

  ------
  -- Input state controller
  -- This state machine changes the operation based on the incoming opcode, and
  -- location within a frame.
  -- Non-sample opcodes need to be buffered and then transmitted out after the
  -- frame of sample data in the order defined within block-based processing.
  -- This state machine therefore also needs to store internal state, as well
  -- as the incoming opcodes.

  output_eom_sent <= output_eom and output_ready;
  -- Mark if the completion of a frame will occur when there are no frames
  -- currently being processed by the FFT (related to controlling discontinuity)
  frame_empty_on_eom <= '1' when (frame_counter = 0 and
                                  (output_eom_sent = '1' or drop_message_i = '1'))
                        else '0';

  state_control_p : process (clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' then
        current_state        <= passthrough_message_s;
        discontinuity_i      <= '0';
        metadata_seen        <= '0';
        sample_interval_seen <= '0';
        timestamp_seen       <= '0';
        drop_message_i       <= '0';
        drop_next_message    <= '0';
        time_after_flush     <= '0';

      elsif enable = '1' then
        drop_message_i <= '0';

        if input_in.ready = '1' then
          case input_in.opcode is
            when complex_short_timed_sample_sample_op_e =>
              if take = '1' then
                current_state <= get_samples_s;
              end if;

            when complex_short_timed_sample_time_op_e =>
              if current_state = get_samples_s then
                timestamp_seen <= '1';
              end if;

            when complex_short_timed_sample_metadata_op_e =>
              if current_state = get_samples_s then
                metadata_seen <= '1';
              end if;

            when complex_short_timed_sample_flush_op_e =>
              if current_state = get_samples_s then
                current_state <= flush_samples_s;
              end if;

            when complex_short_timed_sample_sample_interval_op_e =>
              if current_state = get_samples_s then
                current_state        <= flush_samples_s;
                sample_interval_seen <= '1';
                if timestamp_seen = '1' then
                  time_after_flush <= '1';
                end if;
              end if;

            when complex_short_timed_sample_discontinuity_op_e =>
              if (current_state = get_samples_s or
                  current_state = flush_samples_s) then
                if discontinuity_i = '0' then
                  if current_state /= passthrough_message_s then
                    discontinuity_i <= '1';
                  end if;
                  if message_in_progress = '1' then
                    drop_message_i <= '1';
                  else
                    drop_next_message <= '1';
                  end if;
                end if;
                if frame_counter = 0 or
                  (frame_counter = 1 and processed_end_in = '1') then
                  if drop_next_message = '1' then
                    drop_next_message <= '0';
                    drop_message_i    <= '1';
                  end if;
                else
                  discontinuity_i <= '1';
                  current_state   <= flush_samples_s;
                end if;
              end if;

            when others =>
              null;
          end case;
        end if;

        if current_state = discontinuity_s and take = '1' then
          drop_next_message <= '0';
          drop_message_i    <= '0';
        end if;

        if metadata_fifo_empty = '1' and metadata_msg_in_progress = '0' then
          metadata_seen <= '0';
        end if;

        if (discontinuity_i = '1' and frame_empty_on_eom = '1') or
          (discontinuity_i = '0' and output_eom_sent = '1') or
          (monitor_end_in = '1' and processed_end_in = '1') then
          if discontinuity_i = '0' and current_state = flush_samples_s then
            current_state <= flush_s;
          elsif sample_interval_seen = '1' then
            sample_interval_seen <= '0';
            current_state        <= passthrough_message_s;
          elsif timestamp_seen = '1' then
            timestamp_seen <= '0';
            current_state  <= timestamp_send_s;
          elsif metadata_seen = '1' and (metadata_fifo_empty = '0' or metadata_msg_in_progress = '1') then
            current_state <= metadata_send_s;
          elsif discontinuity_i = '1' then
            current_state <= discontinuity_s;
          else
            time_after_flush <= '0';
            current_state    <= passthrough_message_s;
          end if;
        end if;
      end if;
    end if;
  end process;

  ------
  -- Input Take controller
  -- Update whether `take` should be allowed in the current state (which is
  -- controlled in `state_control_p`). The `take_opcode` is then used with the
  -- state of the downstream ready to control to actual output port.
  take_control_p : process (clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' then
        take_opcode <= '0';

      else
        take_opcode <= '0';
        if input_in.opcode /= complex_short_timed_sample_discontinuity_op_e then
          case current_state is
            when passthrough_message_s =>
              -- Force a bubble state on EOM, this could be replaced with a 1
              -- deep register on the input signals if throughput is required.
              if input_in.eom = '1' and take = '1' then
                take_opcode <= '0';
              elsif enable = '1' then
                take_opcode <= '1';
              end if;

            when get_samples_s =>
              if enable = '1' and input_in.ready = '1' then
                if (input_in.opcode = complex_short_timed_sample_flush_op_e or
                    input_in.opcode = complex_short_timed_sample_discontinuity_op_e or
                    input_in.opcode = complex_short_timed_sample_sample_interval_op_e) or
                  (input_in.eom = '1' and take = '1') then
                  take_opcode <= '0';
                else
                  take_opcode <= '1';
                end if;
              end if;

            when metadata_send_s | timestamp_send_s =>
              take_opcode <= '0';

            when flush_samples_s =>
              if (input_in.opcode = complex_short_timed_sample_flush_op_e and
                  output_eom = '1' and output_ready = '1' and take = '0') then
                take_opcode <= '1';
              else
                take_opcode <= '0';
              end if;

            when flush_s =>
              take_opcode <= '0';

            when others =>
              if enable = '1' then
                take_opcode <= '1';
              end if;
          end case;
        end if;
      end if;
    end if;
  end process;

  ------
  -- Output control process (Combinatorial)
  -- The output is controlled based on the state the system is in (driven by
  -- the opcode being outputted).
  output_control_p : process (data_reset, input_in, current_state, enable,
                              processed_ready_out, take_opcode, discontinuity_i,
                              message_in_progress, message_eom, message_sr,
                              processed_end_in,
                              frame_counter, output_som_r, time_after_flush,
                              timestamp_units_r, timestamp_fraction_r,
                              timestamp_units, timestamp_fraction,
                              output_metadata_value)
  begin
    take               <= '0';
    output_valid       <= '0';
    output_ready       <= '0';
    output_opcode      <= input_in.opcode;
    output_byte_enable <= input_in.byte_enable;
    output_data        <= input_in.data;
    output_eom         <= message_eom;
    output_som         <= output_som_r;
    output_eof         <= '0';

    case current_state is
      when passthrough_message_s | get_samples_s =>
        if current_state = get_samples_s and input_in.opcode = complex_short_timed_sample_discontinuity_op_e then
          take         <= '0';
          output_valid <= '0';
          output_ready <= '0';
        elsif current_state = get_samples_s and message_in_progress = '1' and input_in.opcode /= complex_short_timed_sample_sample_op_e then
          take         <= take_opcode and enable;
          output_valid <= '0';
          output_ready <= '0';
        else
          if input_in.opcode = complex_short_timed_sample_sample_op_e then
            take <= processed_ready_out and enable;
          else
            take <= enable;
          end if;
          output_valid <= input_in.valid;
          output_ready <= input_in.ready;
        end if;

        if input_in.opcode = complex_short_timed_sample_sample_op_e then
          output_eom <= message_eom;
        else
          output_eom <= input_in.eom;
        end if;

        output_opcode <= input_in.opcode;

        if input_in.eof = '1' then
          output_eof    <= input_in.eof;
          output_opcode <= complex_short_timed_sample_sample_op_e;
        end if;

      when flush_samples_s =>
        take               <= take_opcode and enable;
        output_valid       <= '1';
        output_ready       <= '1';
        output_eom         <= (not discontinuity_i and message_eom) or (discontinuity_i and processed_end_in and monitor_end_in);
        output_opcode      <= complex_short_timed_sample_sample_op_e;
        output_byte_enable <= (others => '1');
        output_data        <= (others => '0');

      when discontinuity_s =>
        if input_in.opcode = complex_short_timed_sample_discontinuity_op_e then
          take <= enable;
        else
          take <= '0';
        end if;
        output_ready       <= '1';
        output_opcode      <= complex_short_timed_sample_discontinuity_op_e;
        output_byte_enable <= (others => '0');
        output_eom         <= '1';
        output_som         <= '1';

      when flush_s =>
        take               <= take_opcode;
        output_ready       <= enable;
        output_opcode      <= complex_short_timed_sample_flush_op_e;
        output_byte_enable <= (others => '0');
        output_eom         <= '1';
        output_som         <= '1';

      when sample_interval_send_s =>
        take               <= take_opcode and enable;
        output_valid       <= input_in.valid;
        output_ready       <= input_in.ready;
        output_som         <= input_in.som;
        output_eom         <= input_in.eom;
        output_opcode      <= input_in.opcode;
        output_byte_enable <= (others => '1');
        output_data        <= input_in.data;

      when timestamp_send_s =>
        output_valid       <= '1';
        output_ready       <= '1';
        output_som         <= message_sr(0);
        output_eom         <= message_sr(2);
        output_opcode      <= complex_short_timed_sample_time_op_e;
        output_byte_enable <= (others => '1');
        -- If there has been a flush or sample interval, then the timestamp
        -- needs to be kept relating to the start of the next frame.
        -- (i.e. The timestamp derived with the old sample interval value).
        if message_sr(2) = '1' then
          if time_after_flush = '1' then
            output_data <= timestamp_units_r;
          else
            output_data <= timestamp_units;
          end if;
        elsif message_sr(1) = '1' then
          if time_after_flush = '1' then
            output_data <= timestamp_fraction_r(63 downto 32);
          else
            output_data <= timestamp_fraction(63 downto 32);
          end if;
        else
          if time_after_flush = '1' then
            output_data <= timestamp_fraction_r(31 downto 0);
          else
            output_data <= timestamp_fraction(31 downto 0);
          end if;
        end if;

      when metadata_send_s =>
        output_valid       <= '1';
        output_ready       <= '1';
        output_som         <= message_sr(0);
        output_eom         <= message_sr(3);
        output_byte_enable <= (others => '1');
        output_opcode      <= complex_short_timed_sample_metadata_op_e;
        output_data        <= output_metadata_value(95 downto 64);

      when others =>
        take          <= '0';
        output_valid  <= '0';
        output_ready  <= '0';
        output_opcode <= input_in.opcode;
        output_eom    <= message_eom;
    end case;

    if data_reset = '1' then
      take         <= '0';
      output_valid <= '0';
      output_ready <= '0';
    end if;
  end process;

  input_out.take         <= take;
  output_out.byte_enable <= output_byte_enable;
  output_out.valid       <= output_valid;
  output_out.ready       <= output_ready;
  output_out.eom         <= output_eom;
  output_out.som         <= output_som;
  output_out.eof         <= output_eof;
  output_out.opcode      <= output_opcode;
  output_out.data        <= output_data;
  discontinuity          <= discontinuity_i when current_state = discontinuity_s else '0';
  drop_message           <= drop_message_i;

  ------
  -- SOM Following EOM
  -- By default the SOM is set whenever the EOM is sent out,
  -- to cause a zero length message.
  som_p : process (clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' then
        output_som_r <= '1';
      elsif enable = '1' and (output_ready = '1' or output_valid = '1') then
        if output_eom = '1' then
          output_som_r <= '1';
        else
          output_som_r <= '0';
        end if;
      end if;
    end if;
  end process;

  ------
  -- Metadata FIFO
  -- Metadata messages are 128 bits long, of the format:
  -- Metadata ID (32 bits) | Padding (32 bits) | Metadata Value (64 bits) |
  -- As such the `message_sr` is ignored for position 1 (the padding), with the
  -- remaining fields being inserted into the FIFO.
  metadata_value <= metadata_value_r(63 downto 0) & input_in.data;

  metadata_p : process (clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' then
        metadata_depth           <= 0;
        metadata_message_valid   <= '0';
        metadata_msg_in_progress <= '0';

      elsif enable = '1' then
        metadata_message_valid <= '0';

        if (input_in.opcode = complex_short_timed_sample_metadata_op_e and input_in.valid = '1' and take = '1'
            and message_sr(1) = '0' and current_state /= passthrough_message_s) then
          metadata_msg_in_progress <= '1';
          metadata_value_r         <= metadata_value;
          if message_sr(2) = '1' then
            metadata_message_valid <= '1';
          end if;
          if message_sr(3) = '1' then
            metadata_msg_in_progress <= '0';
          end if;
        end if;

        if current_state = metadata_send_s then
          if message_sr(3) = '1' and metadata_fifo_empty = '0' then
            output_metadata_value <= metadata_fifo(metadata_depth-1);
          -- Metadata messages have 32 0's between the index and the value.
          elsif message_sr(1) = '0' then
            output_metadata_value <= output_metadata_value(63 downto 0) & output_metadata_value(95 downto 64);
            if message_sr(0) = '1' and metadata_fifo_empty = '0' then
              metadata_depth <= metadata_depth - 1;
            end if;
          end if;
        else
          if metadata_fifo_empty = '1' then
            output_metadata_value <= metadata_value;
          else
            output_metadata_value <= metadata_fifo(metadata_depth-1);
          end if;
        end if;

        if metadata_message_valid = '1' then
          metadata_message_valid <= '0';
          for i in 1 to metadata_depth_g-1 loop
            metadata_fifo(i) <= metadata_fifo(i-1);
          end loop;
          metadata_fifo(0) <= metadata_value;
          if metadata_fifo_full = '0' then
            metadata_depth <= metadata_depth + 1;
          end if;
        end if;

      end if;
    end if;
  end process;

  metadata_fifo_empty <= '1' when metadata_depth = 0                else '0';
  metadata_fifo_full  <= '1' when metadata_depth = metadata_depth_g else '0';

  -- The `message_sr` shifts through to indicate which 32bit word is currently
  -- being handled. This is used for all data related, non-sample opcodes.
  data_opcode_counter_p : process (clk)
  begin
    if rising_edge(clk) then
      if data_reset = '1' or (enable = '1' and output_eom = '1') then
        message_sr <= std_logic_vector(to_unsigned(1, message_sr'length));
      elsif enable = '1' and (
        (input_in.opcode = complex_short_timed_sample_metadata_op_e and input_in.valid = '1' and take = '1') or
        current_state = metadata_send_s or
        current_state = sample_interval_send_s or
        current_state = timestamp_send_s) then
        -- Message shift register used for output control when sending metadata
        -- and timestamp messages.
        message_sr <= message_sr(message_sr'high-1 downto 0) & message_sr(message_sr'high);
      end if;
    end if;
  end process;

  -- Catch updated timestamp
  timestamp_reg_p : process (clk)
  begin
    if rising_edge(clk) then
      if time_after_flush = '0' then
        timestamp_units_r    <= timestamp_units;
        timestamp_fraction_r <= timestamp_fraction;
      end if;
    end if;
  end process;

  -- Timestamp calculation related signals.
  timestamp_valid <= '1' when input_in.opcode = complex_short_timed_sample_time_op_e and
                     input_in.valid = '1' and take = '1' else '0';

  sample_interval_valid <= '1' when input_in.opcode = complex_short_timed_sample_sample_interval_op_e and
                           input_in.valid = '1' and take = '1' else '0';

  data_valid <= '1' when input_in.opcode = complex_short_timed_sample_sample_op_e and
                input_in.valid = '1' and take = '1' else '0';

  timestamp_i : timestamp_recovery
    generic map (
      adder_width_g   => 32,
      data_in_width_g => data_in_width_g
      )
    port map (
      clk                          => clk,
      reset                        => reset,
      enable                       => '1',
      timestamp_valid_in           => timestamp_valid,
      sample_interval_valid_in     => sample_interval_valid,
      data_valid_in                => data_valid,
      data_in                      => input_in.data,
      timestamp_units_out          => timestamp_units,
      timestamp_fraction_out       => timestamp_fraction,
      sample_interval_units_out    => sample_interval_units,
      sample_interval_fraction_out => sample_interval_fraction
      );

end rtl;
