#!/usr/bin/env python3

# Python implementation of Fast Fourier Transform for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""FFT OCPI Verification script."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from fast_fourier_transform import FastFourierTransform_ComplexShort

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case(input_file_path)

# Initialise fft object
log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
inverse_fft = os.environ["OCPI_TEST_inverse_fft"].upper() == "TRUE"
indicate_start_of_fft_block = os.environ["OCPI_TEST_indicate_start_of_fft_block"].upper(
) == "TRUE"
increment_fft_block_value = os.environ["OCPI_TEST_increment_fft_block_value"].upper(
) == "TRUE"
start_of_fft_block_id = int(os.environ["OCPI_TEST_start_of_fft_block_id"])
start_of_fft_block_value = int(
    os.environ["OCPI_TEST_start_of_fft_block_value"])
fft_length = int(os.environ["OCPI_TEST_fft_length"])
gain_factor = int(os.environ["OCPI_TEST_gain_factor"])
metadata_fifo_size = int(os.environ["OCPI_TEST_metadata_fifo_size"])

if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logfile = os.path.dirname(output_file_path)
logfile = os.path.join(logfile, f"{test_id}.py.log")
logging.basicConfig(level=verify_log_level, filename=logfile, filemode="w")

fast_fourier_transform_implementation = FastFourierTransform_ComplexShort(
    inverse_fft=inverse_fft,
    indicate_start_of_fft_block=indicate_start_of_fft_block,
    increment_fft_block_value=increment_fft_block_value,
    start_of_fft_block_id=start_of_fft_block_id,
    start_of_fft_block_value=start_of_fft_block_value,
    fft_length=fft_length,
    gain_factor=gain_factor,
    metadata_fifo_size=metadata_fifo_size,
    logging_level=verify_log_level)

# Check that the file isn't empty
if (os.stat(output_file_path).st_size == 0):
    print("Error: Empty output file")
    sys.exit(1)

# Check the result
verifier = ocpi_testing.Verifier(fast_fourier_transform_implementation)
verifier.set_port_types(["complex_short_timed_sample"],
                        ["complex_short_timed_sample"],
                        ["bounded"])
verifier.comparison[0].BOUND = 2

if not verifier.verify(test_id, [input_file_path], output_file_path):
    sys.exit(1)
