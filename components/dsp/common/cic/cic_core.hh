// CIC Filter Implementation
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_DSP_COMMON_CIC_CIC_CORE_HH_
#define COMPONENTS_DSP_COMMON_CIC_CIC_CORE_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstddef>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <cstdint>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <type_traits>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <complex>
// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include <vector>

// LINT EXCEPTION: cpp_011: 3: Single class definition, but template uses ...
// LINT EXCEPTION: cpp_011: 5: .. class keyword multiple times
template <class TYPE,
          class WORK_TYPE =
              typename std::conditional<std::is_integral<TYPE>::value, int64_t,
                                        std::complex<int64_t>>::type>
class cic_core {
 public:
  cic_core(uint8_t cic_order, uint8_t cic_differential_delay)
      : delay_buffer(cic_order * cic_differential_delay + 1),
        delay_buffer_read(delay_buffer.begin() + 1),
        delay_buffer_write(delay_buffer.begin()),
        cic_order(cic_order),
        cic_differential_delay(cic_differential_delay),
        previous_input(cic_order) {
    reset();
  }

  // Delete copy and assignment to prevent invalid shallow copy.
  cic_core(const cic_core &) = delete;
  cic_core &operator=(const cic_core &) = delete;

  /// Reset the member's variables and buffers
  void reset() {
    // Fill delay buffer with zero values to add zero comb filtering to
    // the first inputs.
    std::fill(this->delay_buffer.begin(), this->delay_buffer.end(), 0);
    std::fill(this->previous_input.begin(), this->previous_input.end(), 0);

    // Reset counts.
    this->sample_count = 0;
    this->up_sample_factor_count = 0;
    this->comb_sample = 0;
  }

  /// Set the down_sample_factor (from worker parameter).
  /// @param down_sample_factor to use.
  void set_down_sample_factor(uint16_t down_sample_factor) {
    this->down_sample_factor = down_sample_factor;

    this->sample_count = 0;
  }

  /// Set the up_sample_factor (from worker parameter).
  /// @param up_sample_factor to use.
  void set_up_sample_factor(uint16_t up_sample_factor) {
    this->up_sample_factor = up_sample_factor;

    this->sample_count = 0;
  }

  /// Set the output scale factor.
  /// @param scale_output : Number of bits to scale the calculated
  /// value before output.
  void set_scale_output(uint8_t scale_output) {
    this->scale_output = scale_output;
  }

  /// Do cic decimator work. Returns the length of output data.
  /// @tparam T : Input data type.
  /// @tparam anon : T must be class type (i.e. complex).
  /// @param input : Array of input values.
  /// @param samples: Number of samples in input array.
  /// @param output : Array to hold output values.
  /// @return : Number of sample values in output array.
  /// Implementation for complex (class) operands.
  template <class T = TYPE,
            typename std::enable_if<std::is_class<T>::value, bool>::type = true>
  size_t do_decimator_work(const T input[], size_t samples, T output[]) {
    size_t output_length = 0;

    for (size_t index = 0; index < samples; index++) {
      this->modified_input = {input[index].real, input[index].imaginary};
      // Integrate stage
      integrate_input();

      // Downsampling stage
      this->sample_count++;
      if (this->sample_count >= this->down_sample_factor) {
        // Comb filter stage
        comb_filter_input();

        // Scaling stage
        scale_input();

        output[output_length].real = this->modified_input.real();
        output[output_length].imaginary = this->modified_input.imag();
        output_length++;
        this->sample_count = 0;
      }
    }

    return output_length;
  }

  /// Do cic decimator work. Returns the length of output data.
  /// @tparam T : Input data type.
  /// @tparam anon : T must be integral type (i.e. not complex).
  /// @param input : Array of input values.
  /// @param samples: Number of samples in input array.
  /// @param output : Array to hold output values.
  /// @return : Number of sample values in output array.
  /// Implementation for integral (non-complex) operands.
  template <class T = TYPE, typename std::enable_if<std::is_integral<T>::value,
                                                    bool>::type = true>
  size_t do_decimator_work(const T input[], size_t samples, T output[]) {
    size_t output_length = 0;

    for (size_t index = 0; index < samples; index++) {
      this->modified_input = input[index];
      // Integrate stage
      integrate_input();

      // Downsampling stage
      this->sample_count++;
      if (this->sample_count >= this->down_sample_factor) {
        // Comb filter stage
        comb_filter_input();
        // Scaling stage
        scale_input();

        output[output_length] = this->modified_input;
        output_length++;
        this->sample_count = 0;
      }
    }

    return output_length;
  }

  /// Do cic interpolator work.
  /// @param input : Array of input values.
  /// @param samples: Number of samples in input array.
  /// @param output : Array to hold output values.
  /// @param output_length : Returns number of samples in output array.
  /// @return : true if all input samples have been interpolated, false
  /// if output buffer is full and unprocessed samples remain.
  /// Supports class complex (class) operands.
  bool do_interpolator_work(const TYPE input[], size_t samples, TYPE output[],
                            size_t *output_length, size_t max_output_length) {
    for (uint16_t sample_counter = this->sample_count; sample_counter < samples;
         sample_counter++) {
      // Check if comb filter needs to be applied to this sample
      if (this->comb_sample == sample_counter) {
        this->modified_input = {input[sample_counter].real,
                                input[sample_counter].imaginary};
        // Comb filter stage
        comb_filter_input();
        ++this->comb_sample;
      }

      for (uint16_t upsample_counter = this->up_sample_factor_count;
           upsample_counter < this->up_sample_factor; upsample_counter++) {
        // Check if the output will be able to fit in a single stream
        if (*output_length >= max_output_length) {
          // Return early requiring only the output buffer to be advanced.
          return false;
        }
        // Integrate stage
        integrate_input();
        // Scaling stage
        scale_input();

        output[*output_length].real = this->modified_input.real();
        output[*output_length].imaginary = this->modified_input.imag();
        this->modified_input = {0, 0};
        ++*output_length;
        ++this->up_sample_factor_count;
      }
      this->up_sample_factor_count = 0;
      ++this->sample_count;
    }

    // Finished interpolating all samples - reset and request new input.
    this->sample_count = 0;
    this->up_sample_factor_count = 0;
    this->comb_sample = 0;
    return true;
  }

 private:
  using delay_buffer_type = std::vector<WORK_TYPE>;
  delay_buffer_type delay_buffer;
  typename delay_buffer_type::iterator delay_buffer_read;
  typename delay_buffer_type::iterator delay_buffer_write;

  const uint8_t cic_order;
  const uint8_t cic_differential_delay;
  uint16_t down_sample_factor = 0;
  uint16_t up_sample_factor = 0;
  uint16_t comb_sample = 0;
  uint8_t scale_output = 0;

  delay_buffer_type previous_input;
  WORK_TYPE modified_input;

  uint16_t sample_count;
  uint16_t up_sample_factor_count = 0;

  /// Applies an integrator to a given input and returns the modified input.
  /// @param input : Integral input sample values.
  /// @return : Integrated value.
  void integrate_input() {
    for (uint8_t stage = 0; stage < this->cic_order; stage++) {
      this->modified_input = this->previous_input[stage] + this->modified_input;

      this->previous_input[stage] = this->modified_input;
    }
  }

  /// Applies a comb filter to a given input and returns the modified input
  /// @param input : Integral input sample value.
  /// @return : Value after comb filter applied.
  void comb_filter_input() {
    for (uint8_t stage = 0; stage < this->cic_order; stage++) {
      *this->delay_buffer_write = this->modified_input;
      ++this->delay_buffer_write;

      if (this->delay_buffer_write == this->delay_buffer.end()) {
        this->delay_buffer_write = this->delay_buffer.begin();
      }

      this->modified_input -= *this->delay_buffer_read;

      ++this->delay_buffer_read;
      if (this->delay_buffer_read == this->delay_buffer.end()) {
        this->delay_buffer_read = this->delay_buffer.begin();
      }
    }
  }

  /// Scales a given input by the class member's scale factor and returns the
  /// scaled input
  /// @param input : Complex input sample value.
  /// @return : Scaled sample value.
  template <class T = TYPE,
            typename std::enable_if<std::is_class<T>::value, bool>::type = true>
  void scale_input() {
    if (this->scale_output > 0) {
      // Scale output with half up rounding
      // Half the scale output value is added to the input to ensure
      // integer truncation results in half up rounding
      this->modified_input.real(
          (this->modified_input.real() + (1 << (this->scale_output - 1))) >>
          this->scale_output);
      this->modified_input.imag(
          (this->modified_input.imag() + (1 << (this->scale_output - 1))) >>
          this->scale_output);
    }
  }
  template <class T = TYPE, typename std::enable_if<std::is_integral<T>::value,
                                                    bool>::type = true>
  void scale_input(void) {
    if (this->scale_output > 0) {
      // Scale output with half up rounding
      // Half the scale output value is added to the input to ensure
      // integer truncation results in half up rounding
      this->modified_input =
          (this->modified_input + (1 << (this->scale_output - 1))) >>
          this->scale_output;
    }
  }
};

#endif  // COMPONENTS_DSP_COMMON_CIC_CIC_CORE_HH_
