#!/usr/bin/env python3

# Verifier for polyphase interpolator testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verifier script for the Polyphase Interpolator (Complex Short)."""

import os
import sys
import logging

import numpy as np

import opencpi.ocpi_testing as ocpi_testing

from polyphase_interpolator import PolyphaseInterpolatorXS

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case()

# Initialise interpolator object
interpolation_factor = int(os.environ["OCPI_TEST_interpolation_factor"])
number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
taps_per_branch = int(np.ceil(number_of_taps / interpolation_factor))
rounding_type = os.environ["OCPI_TEST_rounding_type"]
group_delay_seconds = int(os.environ["OCPI_TEST_group_delay_seconds"])
group_delay_fractional = int(os.environ["OCPI_TEST_group_delay_fractional"])

taps = [0] * (number_of_taps)
for i, x in enumerate((os.environ["OCPI_TEST_taps"]).split(",")):
    if i >= (number_of_taps):
        if int(x) != 0:
            raise ValueError(
                "Taps contains too many values (expected: {}, got: {})\n"
                .format(number_of_taps,
                        len((os.environ["OCPI_TEST_taps"]).split(","))))
        else:
            continue
    taps[i] = int(x)

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 0))
if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logging.basicConfig(
    level=verify_log_level,
    filename=f"{test_id}.polyphase_interpolator.python.log",
    filemode="w")

# Set the overflow type for scale and round
overflow_type = "saturate"

interpolator_implementation = PolyphaseInterpolatorXS(
    interpolation_factor, taps, rounding_type=rounding_type,
    overflow_type=overflow_type,
    group_delay_seconds=group_delay_seconds,
    group_delay_fractional=group_delay_fractional)

# Check the result
verifier = ocpi_testing.Verifier(interpolator_implementation)
verifier.set_port_types(
    ["complex_short_timed_sample"], ["complex_short_timed_sample"], ["equal"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
