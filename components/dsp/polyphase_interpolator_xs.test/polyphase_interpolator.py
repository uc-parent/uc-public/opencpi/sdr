#!/usr/bin/env python3

# Generates the input binary file for polyphase interpolator testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Complex short version of the Polyphase Interpolator including opcode handling."""

import opencpi.ocpi_testing as ocpi_testing
import opencpi.ocpi_protocols as ocpi_protocols

import decimal

from sdr_dsp.polyphase_interpolator import PolyphaseInterpolator_ComplexInt
from sdr_interface.sample_time import SampleTime


class PolyphaseInterpolatorXS(ocpi_testing.Implementation):
    """Complex short version of the Polyphase Interpolator including opcode handling."""

    def __init__(self, interpolation_factor, taps=[],
                 rounding_type="truncate",
                 overflow_type="saturate",
                 group_delay_seconds=0,
                 group_delay_fractional=0,
                 max_interpolation=8, max_taps_per_branch=3,
                 max_message_samples=ocpi_protocols.PROTOCOLS[
                     "complex_short_timed_sample"].max_sample_length):
        """Initialises a Polyphase Interpolator class.

        Args:
            interpolation_factor (int): The current upsampling factor
            taps (list of float): The coefficient values
            rounding_type (str): Rounding mode to use "half_up","half_even"
                                 or "truncate"
            overflow_type (str): overflow type to use when rounding "wrap" or
                                 "saturate".
            group_delay_seconds (int): The filter group delay in whole seconds.
            group_delay_fractional (int): The filter group delay, fractional
                                          part of a second, multiplied by 2^64.
            max_interpolation (int): The maximum upsampling factor
            max_taps_per_branch: The maximum number of taps per branch
            max_message_samples (int): The maximum number of samples that can
                                       be placed in a single ocpi output buffer
        """
        super().__init__(_interpolation_factor=interpolation_factor,
                         _taps=taps,
                         _rounding_type=rounding_type,
                         _max_interpolation=int(max_interpolation),
                         _max_taps_per_branch=int(max_taps_per_branch))

        self._max_message_samples = max_message_samples

        self._sample_time = SampleTime(
            group_delay_seconds=group_delay_seconds,
            group_delay_fractional=group_delay_fractional,
            interval_up_sampling_factor=interpolation_factor)

        self._polyphase_interpolator = PolyphaseInterpolator_ComplexInt(
            interpolation_factor,
            taps,
            rounding_type=rounding_type,
            overflow_type=overflow_type)

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._polyphase_interpolator.reset()

    def sample(self, input_):
        """Functionality that a sample opcode message triggers.

        Take the incoming samples values and output the resultant interpolated
        samples values in one or more sample opcode messages.

        Args:
            input_ (list of complex): Opcode data for the input port.

        Returns:
            Formatted messages of sample opcodes. Samples are split into multiple
            opcodes if they exceed the maximum message size due to interpolation.
        """
        data = []
        for s in input_:
            data.extend(self._polyphase_interpolator.interpolate(s))

        # As this component increases the data size, break long messages up
        # into messages with no more than _max_message_samples samples.
        messages = [{
            "opcode": "sample",
            "data": data[index: index + self._max_message_samples]}
            for index in range(0, len(data),
                               self._max_message_samples)]
        return self.output_formatter(messages)

    def time(self, value):
        """Functionality that a time opcode message triggers.

        Pass the time opcode, corrected for group delay, to the output port.

        Args:
            value: The time value received on the input port.

        Returns:
            Formatted messages of time opcodes corrected for the group delay.
        """
        # Time values are corrected for group delay
        self._sample_time.time = value
        ret = [[{"opcode": "time", "data": self._sample_time.opcode_time()}]]
        return self.output_formatter(*ret)

    def sample_interval(self, value):
        """Functionality that a sample interval opcode message triggers.

        Calculate the output sample interval and send to the output port.

        Args:
            value: The incoming sample interval value for the input port.

        Returns:
            Formatted messages of sample interval opcodes with the value
            from the sample_time primitive.
        """
        # Store the received sample interval and apply interpolation factor to output
        self._sample_time.interval = value

        ret = [[{"opcode": "sample_interval",
                 "data": self._sample_time.opcode_interval()}]]
        return self.output_formatter(*ret)

    def _flush(self):
        """Process zero sample values to flush out any in-progress data.

        Returns:
            Any sample opcodes produced.
        """
        ret = self.sample([0] * self._polyphase_interpolator.taps_per_branch *
                          self._polyphase_interpolator.upsample)
        self._polyphase_interpolator.reset()
        return ret

    def flush(self, input_):
        """Functionality that a flush opcode message triggers.

        If there is any in-progress data, flush it out by processing taps_per_branch
        zeros, forwarding on any sample opcode messages produced.
        Then, reset the state and forward the Flush opcode.

        Args:
            input_ (various): Opcode data for the input port.

        Returns:
            A formatted flush opcode message that will follow the sample opcodes
            returned by _flush() if any data had been sent.
        """
        if self._polyphase_interpolator.data_sent:
            ret = self._flush()
            ret[0].append({"opcode": "flush", "data": None})
        else:
            ret = [[{"opcode": "flush", "data": None}]]
        # Add a flush to end of output
        return self.output_formatter(*ret)

    def discontinuity(self, input_):
        """Functionality that a discontinuity opcode message triggers.

        Reset the state and forward the discontinuity opcode to the output port.

        Args:
            input_ (various): Opcode data for the input port.

        Returns:
            A formatted discontinuity opcode message.
        """
        self._polyphase_interpolator.reset()
        ret = [[{"opcode": "discontinuity", "data": None}]]
        return self.output_formatter(*ret)
