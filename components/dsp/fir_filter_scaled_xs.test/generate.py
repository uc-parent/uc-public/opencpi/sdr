#!/usr/bin/env python3

# Generates the input binary file for fir_filter_scaled_xs testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for fir_filter_scaled_xs testing."""

import os
import logging

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class FirFilterScaledGenerator(ocpi_testing.generator.ComplexShortGenerator):
    """Fir Filter Scaled test case generator."""

    def __init__(self, **kwds):
        """Initialise."""
        super().__init__()

    def flush(self, seed, subcase):
        """Override of base adds "initial" case then defers to super().

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        if subcase == "initial":
            # Insert an initial flush opcode before data
            messages = [{"opcode": "flush", "data": None},
                        {"opcode": "sample", "data": self._get_sample_values()}]
            return messages

        return super().flush(seed, subcase)


log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

arguments = ocpi_testing.get_generate_arguments()

scale_factor_property = int(os.environ.get("OCPI_TEST_scale_factor"))
rounding_type_property = os.environ.get("OCPI_TEST_rounding_type")
number_of_taps_property = int(os.environ.get("OCPI_TEST_number_of_taps"))
taps_property = [
    int(tap) for tap in os.environ.get("OCPI_TEST_taps").split(",")]
# Default to a single multiplier, to allow for the property to not exist
# when testing an RCC worker
number_of_multipliers_property = int(
    os.environ.get("OCPI_TEST_number_of_multipliers", 1))
group_delay_seconds_property = int(
    os.environ.get("OCPI_TEST_group_delay_seconds"))
group_delay_fractional_property = int(
    os.environ.get("OCPI_TEST_group_delay_fractional"))
subcase = os.environ.get("OCPI_TEST_subcase")

# The number of taps provided can be greater than needed. OCPI will also
# remove multiple trailing zeros and so the number of taps may be less than
# needed - add in extra zeros and trim to length to cover both cases
taps_property += [0] * number_of_taps_property
taps_property = taps_property[:number_of_taps_property]

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase,
    scale_factor_property,
    rounding_type_property,
    number_of_taps_property,
    taps_property,
    number_of_multipliers_property,
    group_delay_seconds_property,
    group_delay_seconds_property)

generator = FirFilterScaledGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
