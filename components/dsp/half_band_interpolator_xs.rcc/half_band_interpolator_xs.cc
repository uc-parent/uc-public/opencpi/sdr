// Implementation of the half_band_interpolator_xs worker in C++.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cstring>
#include "../../math/common/time_utils.hh"
#include "../common/fir/symmetric_fir_core.hh"
#include "half_band_interpolator_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Half_band_interpolator_xsWorkerTypes;

class Half_band_interpolator_xsWorker
    : public Half_band_interpolator_xsWorkerBase {
  static_assert((HALF_BAND_INTERPOLATOR_XS_NUMBER_OF_TAPS + 1) % 4 == 0,
                "number_of_taps must be 4n-1");
  const uint8_t number_of_taps = HALF_BAND_INTERPOLATOR_XS_NUMBER_OF_TAPS;

  // symmetric_fir_core <SAMPLE_TYPE, ACCUMULATOR_TYPE>
  // Note : The ACCUMULATOR_TYPE needs to be sized appropriately to allow the
  // roll over of any internal mathematical calculations. The size of the input
  // data or the taps can require an internal accumulator size 4 times the
  // size of the SAMPLE_TYPE.
  // Use 16 bit sample/output, 64 bit working size
  symmetric_fir_core<int16_t, int64_t> fir{};

  // Flags when data has been received and will need flushing out prior to
  // passing through a flush opcode.
  RCCBoolean received_data = false;
  // Zero filled buffer for flush opcode.
  std::vector<Complex_short_timed_sampleSampleData> zero_input;

  // tap container used to perform tap interpolation
  int16_t taps[UINT8_MAX];

  // Number of samples already processed for this opcode.
  uint32_t samples_processed = 0;

  uint32_t group_delay_time_seconds = 0;   // group delay time seconds part
  uint64_t group_delay_time_fraction = 0;  // group delay time fractional part
  uint32_t output_time_seconds = 0;        // output time seconds part
  uint64_t output_time_fraction = 0;       // output time fractional part

  // Create 16 bit containers to hold input data values
  int16_t data_buffer[2 * HALF_BAND_INTERPOLATOR_XS_OCPI_MAX_BYTES_INPUT /
                      sizeof(Complex_short_timed_sampleSampleData)];

  // Define limit for maximum number of complex short output samples
  // that will fit into a single message
  const size_t max_output_samples =
      HALF_BAND_INTERPOLATOR_XS_OCPI_MAX_BYTES_OUTPUT /
      sizeof(Complex_short_timed_sampleSampleData);

  // Loads taps from properties and sets every other tap to zero
  void interpolate_taps() {
    // Copy taps into a container to perform tap interpolation.
    std::copy(properties().taps, properties().taps + this->number_of_taps,
              this->taps);
    for (size_t i = 0; i < this->number_of_taps; i++) {
      // If not the centre tap:
      if ((i != this->number_of_taps / 2) &&
          // If number of taps is 1,5,9... then the even taps...
          (((i % 2 == 0) && (this->number_of_taps % 4 == 1)) ||
           // If number of taps is 3,7,11... then the odd taps...
           ((i % 2 == 1) && (this->number_of_taps % 4 == 3)))) {
        // ...are set to zero.
        this->taps[i] = 0;
      }
    }
  }

  RCCResult start() {
    // Initialise sample storage buffer
    std::memset(this->data_buffer, 0, sizeof(this->data_buffer));

    this->zero_input.resize(this->number_of_taps,
                            Complex_short_timed_sampleSampleData());
    interpolate_taps();

    // Convert properties().rounding_type into dsp enum types
    // Default is convergent rounding with wrapping.
    sdr_math::rounding_type rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    sdr_math::overflow_type overflow = sdr_math::overflow_type::MATH_WRAP;

    Rounding_type property_value = properties().rounding_type;

    if (property_value == ROUNDING_TYPE_HALF_EVEN) {
      rounding = sdr_math::rounding_type::MATH_HALF_EVEN;
    } else if (property_value == ROUNDING_TYPE_HALF_UP) {
      rounding = sdr_math::rounding_type::MATH_HALF_UP;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
    } else if (property_value == ROUNDING_TYPE_TRUNCATE_WITH_SATURATION) {
      rounding = sdr_math::rounding_type::MATH_TRUNCATE;
      overflow = sdr_math::overflow_type::MATH_SATURATE;
    }

    this->fir.set(this->number_of_taps, this->taps, rounding, overflow, false);
    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() {
    interpolate_taps();
    this->fir.set_taps(this->number_of_taps, this->taps);
    return RCC_OK;
  }

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_time_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_time_fraction = properties().group_delay_fractional;
    return RCC_OK;
  }

  // Process samples and send to the output port
  RCCResult handle_samples(
      const Complex_short_timed_sampleSampleData *input_data, size_t length,
      bool advance_input) {
    // Interpolation doubles number of samples from input to output.
    const size_t samples_to_process =
        std::min(length, this->max_output_samples / 2);

    // Save contents of 16 bit input data into a container
    for (size_t sample = 0; sample < samples_to_process; ++sample) {
      this->data_buffer[sample * 4] =
          input_data[sample + this->samples_processed].real;
      this->data_buffer[(sample * 4) + 1] =
          input_data[sample + this->samples_processed].imaginary;
    }
    this->samples_processed += samples_to_process;

    if (samples_to_process > 0) {
      this->received_data = true;
      Complex_short_timed_sampleSampleData *output_data =
          output.sample().data().data();
      output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
      output.sample().data().resize(samples_to_process * 2);

      this->fir.do_work_complex(this->data_buffer, samples_to_process * 2,
                                &output_data->real);

      output.advance();
    }

    if (this->samples_processed >= length) {
      // Finished this input so can advance input.
      this->samples_processed = 0;
      if (advance_input) {
        input.advance();
      } else {
        this->received_data = false;
      }
    }
    return RCC_OK;
  }

  RCCResult run(bool) {

    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const Complex_short_timed_sampleSampleData *input_data =
          input.sample().data().data();
      return handle_samples(input_data, length, true);

    } else if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
      output_time_seconds = input.time().seconds();
      output_time_fraction = input.time().fraction();

      // Subtract group delay from time.
      time_utils::subtract(&output_time_seconds, &output_time_fraction,
                           this->group_delay_time_seconds,
                           this->group_delay_time_fraction);
      output.time().seconds() = output_time_seconds;
      output.time().fraction() = output_time_fraction;
      return RCC_ADVANCE;

    } else if (input.opCode() ==
               Complex_short_timed_sampleSample_interval_OPERATION) {
      const uint8_t interpolation_factor = 2;

      uint32_t output_interval_seconds = input.sample_interval().seconds();
      uint64_t output_interval_fraction = input.sample_interval().fraction();

      time_utils::divide(&output_interval_seconds, &output_interval_fraction,
                         interpolation_factor);

      output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
      output.sample_interval().seconds() = output_interval_seconds;
      output.sample_interval().fraction() = output_interval_fraction;

      return RCC_ADVANCE;

    } else if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      if (this->received_data) {
        return handle_samples(this->zero_input.data(), this->number_of_taps,
                              false);
      }

      // Pass through flush opcode
      output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;

    } else if (input.opCode() ==
               Complex_short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      this->fir.reset();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    }
    setError("Unknown OpCode Received");
    return RCC_FATAL;
  }
};

HALF_BAND_INTERPOLATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
HALF_BAND_INTERPOLATOR_XS_END_INFO
