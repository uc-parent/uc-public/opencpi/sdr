#!/usr/bin/env python3

# Generate input for the fir_filter_symmetric_xs for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Symmetric FIR filter test data generator."""

import os
import random
import logging

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]

seed = ocpi_testing.get_test_seed(arguments.case, subcase)


class FirFilterSymmetricGenerator(ocpi_testing.generator.ComplexShortGenerator):
    """Symmetric FIR filter test case generator."""

    def __init__(self, **kwds):
        """Initialise."""
        super().__init__()

    def property(self, seed, subcase):
        """Override of property generator for group_delay subcase.

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        random.seed(seed)

        if subcase == "group_delay":
            # Ensure time and interval are included.
            messages = [{"opcode": "sample_interval", "data": 1.0},
                        {"opcode": "time", "data": 10000.0},
                        {"opcode": "sample", "data": self._get_sample_values()},
                        {"opcode": "time", "data": 20000.0},
                        {"opcode": "sample", "data": self._get_sample_values()}]
            return messages

        return super().property(seed, subcase)

    def flush(self, seed, subcase):
        """Override of property generator for flush subcase.

        Adds additional "initial" subcase, then defers to super() for
        other subcases.

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        random.seed(seed)

        if subcase == "initial":
            # Insert an initial flush opcode before data
            messages = [{"opcode": "flush", "data": None},
                        {"opcode": "sample", "data": self._get_sample_values()}]
            return messages

        return super().flush(seed, subcase)


log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

generator = FirFilterSymmetricGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
