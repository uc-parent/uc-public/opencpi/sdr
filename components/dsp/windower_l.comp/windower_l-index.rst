.. windower_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _windower_l:


Windower (``windower_l``)
=========================
Applies a window function to a stream of values.

Design
------
The size of the window will be set by the ``window_length`` property.  The windower component will use the ``coefficients`` array to read in the coefficients of a particular window.

Once initialised, the windower component sets an internal variable ``coefficient_index`` to zero.

When a sample is received on the input port the windower component completes the following steps for each input sample:

#. Set output sample to :math:`\frac{input\_sample \times coefficients[coefficient\_index]}{2^{32}}`

    * Divide by :math:`2^{32}` to reduce FPGA logic (unsigned long window coefficients will have values in the range 0 to 0.999...)

    * The output is then rounded using a :ref:`half up rounder <rounding_halfup-primitive>`.

#. Increment ``coefficient_index``.

#. If ``coefficient_index`` is greater than or equal to ``window_length`` set ``coefficient_index`` to zero.

The ``coefficient_index`` is reset to zero on receipt of a flush or discontinuity operation on the input port.

The :ref:`windower_l_proxy` can be used to generate the window coefficients.


Interface
---------
.. literalinclude:: ../specs/windower_l-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
The coefficients are applied to each value in sample opcode messages.

When a Flush or Discontinuity opcode is received, the coefficient index is reset and the opcode is forwarded on.

All other opcodes are passed through this component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   dummy_property: This is only present to work around a bug that means you cannot set raw properties without a volatile property existing.  It is not intended to be used, and its value has no effect on the operation of the component.

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../windower_l.hdl ../windower_l.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Windower primitive <windower-primitive>`

 * :ref:`Rounding (half-up) primitive <rounding_halfup-primitive>`

 * :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
