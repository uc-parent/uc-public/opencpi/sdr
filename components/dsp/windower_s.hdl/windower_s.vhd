-- HDL implementation of windower_s.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library sdr_dsp;
library util;

architecture rtl of worker is

  -- Component Constants
  constant data_width_c  : integer := 16;  -- width of data in protocol
  constant coeff_width_c : integer := 33;  -- 1 bit longer than long_t to cater for unsigned to signed conversion
  constant delay_c       : integer := 3;  -- 1 clock cycle delay for ram and 2 clock cycle delay for windower

  -- Address width for the DPRAM is log2(length of coefficients array)
  constant window_addr_width : integer := 11;

  -- Sample Data Signals
  signal input_data       : signed(data_width_c-1 downto 0);
  signal input_data_valid : std_logic;
  signal output_data      : signed(data_width_c-1 downto 0);

  -- Concatenated processed data
  signal processed_data : std_logic_vector(data_width_c-1 downto 0);

  -- Coefficient Counter
  signal coeff_write     : std_logic;
  signal coeff_index     : integer range 0 to to_integer(max_window_length)-1;
  signal coeff_index_slv : std_logic_vector(window_addr_width-1 downto 0);

  -- Raw address input into DPRAM
  signal raw_input_addr : std_logic_vector(window_addr_width-1 downto 0);

  -- Coefficient
  signal coeff     : signed(coeff_width_c-1 downto 0);
  signal coeff_slv : std_logic_vector(coeff_width_c-2 downto 0);  -- -2 due to this being before 0 is appended

  -- Flags
  signal sample_data_valid   : bool_t;
  signal flush_discontinuity : bool_t;

begin

  -- props_out set to default
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= '1';
  props_out.raw.error <= bfalse;

  ------------------------------------------------------------------------------
  -- Interface Delay Primitive
  ------------------------------------------------------------------------------
  input_out.take    <= output_in.ready;
  sample_data_valid <= '1' when (input_in.opcode = short_timed_sample_sample_op_e) and
                       (output_in.ready = '1') and (input_in.valid = '1') else bfalse;

  interface_delay_i : entity work.short_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => processed_data,
      output_out          => output_out
      );

  ------------------------------------------------------------------------------
  -- Coefficient RAM Primitive
  ------------------------------------------------------------------------------

  -- coefficient index is used as an address into the coefficient ram.
  coeff_index_slv <= std_logic_vector(to_unsigned(coeff_index, window_addr_width));

  -- raw_input_addr = props_in.raw.address with MSBs removed
  -- right shifted by 2 to align ram word address with raw props byte address
  raw_input_addr <= std_logic_vector(props_in.raw.address(window_addr_width+2-1 downto 2));

  coeff_write <= '1' when (unsigned(raw_input_addr) < max_window_length) and
                 (props_in.raw.is_write = '1') else '0';

  coefficent_ram : util.util.bram2
    generic map (
      pipelined  => 0,
      addr_width => window_addr_width,
      data_width => coeff_width_c-1,
      memsize    => to_integer(max_window_length)
      )
    port map (
      clka  => ctl_in.clk,
      ena   => '1',
      wea   => coeff_write,
      addra => raw_input_addr,
      dia   => props_in.raw.data,
      doa   => open,
      clkb  => ctl_in.clk,
      enb   => sample_data_valid,
      web   => '0',
      addrb => coeff_index_slv,
      dib   => (others => '0'),
      dob   => coeff_slv
      );

  ------------------------------------------------------------------------------
  -- Windower Primitive
  ------------------------------------------------------------------------------
  window_real : sdr_dsp.sdr_dsp.windower
    generic map (
      data_width_g  => data_width_c,
      coeff_width_g => coeff_width_c
      )
    port map (
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => output_in.ready,
      coeff_in           => coeff,
      data_in            => input_data,
      data_in_valid_in   => input_data_valid,
      data_out           => output_data,
      data_out_valid_out => open
      );


  -- extra bit added due to conversion from unsigned to signed
  coeff <= signed('0' & coeff_slv);

  -- concatenate data
  processed_data <= std_logic_vector(output_data);

  ------------------------------------------------------------------------------
  -- coeff_counter Process
  ------------------------------------------------------------------------------
  -- Counts through coefficients whilst data is valid and is reset under any
  -- of these conditions: reset, coeff_length is reached or a
  -- flush/discontinuity opcode occurs
  ------------------------------------------------------------------------------
  flush_discontinuity <= '1' when ((input_in.opcode = short_timed_sample_discontinuity_op_e)
                                   or (input_in.opcode = short_timed_sample_flush_op_e))
                         and (input_in.ready = '1') else bfalse;
  -- input_in.ready is checked as input_in.vaild will be '0' during a
  -- flush/discontinuity ZLM.

  coeff_counter : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' or flush_discontinuity = '1' then
        coeff_index <= 0;
      elsif sample_data_valid = '1' then
        if coeff_index >= props_in.window_length-1 then
          coeff_index <= 0;
        else
          coeff_index <= coeff_index + 1;
        end if;
      end if;
    end if;
  end process coeff_counter;

  ------------------------------------------------------------------------------
  -- delay_data Process
  ------------------------------------------------------------------------------
  -- Delays input data by 1 clock cycle because the coefficients take 1 clock
  -- cycle to read out of BRAM
  ------------------------------------------------------------------------------
  delay_data : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        input_data       <= (others => '0');
        input_data_valid <= bfalse;
      elsif output_in.ready = '1' then
        input_data       <= signed(input_in.data(data_width_c-1 downto 0));
        input_data_valid <= sample_data_valid;
      end if;
    end if;
  end process delay_data;

end rtl;
