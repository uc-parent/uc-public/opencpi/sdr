.. cic_decimator_design

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

CIC decimators are an efficient method of reducing the sample rate of an input signal by an integer factor. CIC decimators do not require any multiplication operations making them particularly suitable for compact FPGA designs.

In a CIC decimator the integrator stage is applied first, then the down sampling is applied before the comb stage is applied last. CIC filters have an inherent gain that typically needs to be removed; in this component this is done via a right shift operation allowing for power-of-2 gain compensation. A half-up rounding unit is used to minimise the amount of DC bias caused by the right shift operation.

A block diagram representation of the implementation is given in :numref:`|component_name|-cic_decimator-diagram`.

.. _|component_name|-cic_decimator-diagram:

.. figure:: |include_dir|cic_decimator.svg
   :alt: Block diagram of CIC decimator implementation.
   :align: center

   Block diagram of CIC decimator implementation.

In :numref:`|component_name|-cic_decimator-diagram`

 * :math:`N` is the order of the CIC filter (``cic_order``).

 * :math:`R` is the down sampling factor (``down_sample_factor``).

 * :math:`M` is the delay factor (``cic_delay``, this must only be set to 1 or 2).

 * :math:`S` is the power of 2 gain compensation.

Frequency Response
~~~~~~~~~~~~~~~~~~
The frequency response of a CIC decimator is given by in :eq:`|component_name|-cic_decimator_frequency_response-equation`.

.. math::
   :label: |component_name|-cic_decimator_frequency_response-equation

   H\left (\omega  \right ) = \left | \frac{sin\left ( \frac{\omega M}{2} \right )}{sin\left ( \frac{\omega }{2R} \right )} \right | ^{N}

In :eq:`|component_name|-cic_decimator_frequency_response-equation`:

 * :math:`H(\omega)` is the narrowband frequency response.

When down sampling a signal it is typically desirable to minimise the amplitude of any signals that will be above the Nyquist frequency of the lower sample rate as these signals will alias into the passband. The order and delay factor of the CIC sets the wideband frequency response of a CIC filter, with higher values giving more suppression of signals that will alias after the down sample operation.

The wideband frequency response (i.e. the response before the down sampling operation is performed) is shown in :numref:`|component_name|-cic_decimator_wide_frequency_response_m1-diagram` and :numref:`|component_name|-cic_decimator_wide_frequency_response_m2-diagram`.

.. _|component_name|-cic_decimator_wide_frequency_response_m1-diagram:

.. figure:: |include_dir|cic_decimator_wide_frequency_response_m1.svg
   :alt: Graph of wideband CIC frequency response
   :align: center

   Wideband CIC frequency response. :math:`M = 1`. :math:`R = 8`.

.. _|component_name|-cic_decimator_wide_frequency_response_m2-diagram:

.. figure:: |include_dir|cic_decimator_wide_frequency_response_m2.svg
   :alt: Graph of wideband CIC frequency response
   :align: center

   Wideband CIC frequency response. :math:`M = 2`. :math:`R = 8`.

Gain
~~~~
Increasing the order and differential delay will result in an increase in the gain of the CIC. A larger gain means the width of internal registers used by the CIC decimator (set by the parameter ``cic_register_size``) must be larger in order to store the calculated values. This in turn means the filter takes up more space on an FPGA.

The gain of a CIC decimator is given by :math:`G` in :eq:`|component_name|-cic_decimator_gain-equation`

.. math::
   :label: |component_name|-cic_decimator_gain-equation

   G = \left ( RM \right )^{N}

The minimum ``cic_register_size`` in bits needed to correctly perform the CIC decimation is given in :eq:`|component_name|-cic_decimator_gain_width-equation`. In this component ``inWidth`` is 16 bits.

.. math::
   :label: |component_name|-cic_decimator_gain_width-equation

   \text{cic_register_size} = \text{inWidth} + \left\lceil N\log_{2}(RM) \right\rceil

Passband Attenuation
~~~~~~~~~~~~~~~~~~~~
CIC decimators have attenuation in the passband. The larger the CIC order and delay factor the more attenuation in the passband. The narrowband frequency response of a CIC decimator is shown in :numref:`|component_name|-cic_decimator_narrow_frequency_response-diagram`.

.. _|component_name|-cic_decimator_narrow_frequency_response-diagram:

.. figure:: |include_dir|cic_decimator_narrow_frequency_response.svg
   :alt: Graph of narrowband CIC frequency response
   :align: center

   Narrowband (after decimation) CIC frequency response. :math:`R = 8`.

The narrowband frequency response of a CIC filter is same regardless of the ``down_sample_factor`` (:math:`R`). This is shown in :numref:`|component_name|-cic_decimator_narrow_freq_r-diagram`.

.. _|component_name|-cic_decimator_narrow_freq_r-diagram:

.. figure:: |include_dir|cic_decimator_narrow_frequency_response_r.svg
   :alt: Graph comparing narrowband CIC frequency response for different values of R
   :align: center

   Narrowband (after decimation) CIC frequency response. :math:`N = 3`. :math:`M = 2`.

Mathematical Representation
~~~~~~~~~~~~~~~~~~~~~~~~~~~

:eq:`|component_name|-cic_decimator_integrate-equation` and :eq:`|component_name|-cic_decimator_down_comb-equation` give a mathematical representation of the completed CIC filter implementation.

.. math::
   g[n] = \sum_{m=0}^{n}\left( \frac{\prod_{l=1}^{N-1} \left( n+N-m-l \right) }{(N-1)!} x[m] \right)
   :label: |component_name|-cic_decimator_integrate-equation

.. math::
   h[n] = \sum_{i=0}^{N} (-1)^{i} \binom{N}{i} g[(n-iM)R]
   :label: |component_name|-cic_decimator_down_comb-equation

In :eq:`|component_name|-cic_decimator_integrate-equation` and :eq:`|component_name|-cic_decimator_down_comb-equation`, :math:`h[n]` is the output values and :math:`x[n]` is the input data stream.

In :eq:`|component_name|-cic_decimator_down_comb-equation` the notation :math:`\binom{n}{x}` is the binomial expansion, or more informally described as ":math:`n` choose :math:`x`".

After the CIC filter implementation using the above, an attenuation stage is applied to allow the option of cancelling out the inherent CIC filter gain, this attenuation is implemented as a right shift, which is equivalent to the expression in :eq:`|component_name|-cic_decimator_atten-equation`.

.. math::
   y[n] = \frac{h[n]}{2^S}
   :label: |component_name|-cic_decimator_atten-equation

In :eq:`|component_name|-cic_decimator_atten-equation`, :math:`y[n]` is the final output of the component and :math:`S` is the scale output property.
