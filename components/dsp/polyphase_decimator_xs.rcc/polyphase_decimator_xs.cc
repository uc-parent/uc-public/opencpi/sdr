// RCC implementation of polyphase_decimator_xs worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../../math/common/time_utils.hh"
#include "../common/polyphase_decimator/polyphase_decimator.hh"
#include "OcpiDebugApi.hh"
#include "polyphase_decimator_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Polyphase_decimator_xsWorkerTypes;

class Polyphase_decimator_xsWorker : public Polyphase_decimator_xsWorkerBase {
  dsp::polyphase_decimator<int16_t, int32_t, int64_t>* decimator_real = nullptr;
  dsp::polyphase_decimator<int16_t, int32_t, int64_t>* decimator_imag = nullptr;
  uint8_t decimation_factor = 1;
  uint8_t taps_per_branch = 1;
  int32_t group_delay_seconds = 0;
  int64_t group_delay_fraction = 0;

  bool received_data = false;  // Records if data received into buffers.
  uint32_t input_interval_seconds = 0;   // Sample Interval received ..
  uint64_t input_interval_fraction = 0;  // .. on input port
  uint32_t output_time_seconds = 0;      // Time of next sample to send ..
  uint64_t output_time_fraction = 0;     // .. on output port
  bool missed_timestamp = false;  // Holding timestamp until next sample output.
  size_t samples_until_time = 0;  // Number of samples until time output.
  size_t sample_offset = 0;  // Number of samples processed for this decimation.

  void reset() {
    this->decimator_real->reset();
    this->decimator_imag->reset();

    this->received_data = false;
    this->output_time_seconds = 0;
    this->output_time_fraction = 0;
    this->missed_timestamp = false;
    this->samples_until_time = 0;
    this->sample_offset = 0;
  }

  RCCResult set_taps() {
    // No limits on range, so inputs not bounds checked
    // But will check we have the right number of taps
    size_t taps_passed_in =
        sizeof(properties().taps) / sizeof(properties().taps[0]);
    size_t taps_required =
        properties().taps_per_branch * properties().decimation_factor;

    if (taps_passed_in < taps_required) {
      setError(
          "Invalid number of taps supplied, expected %lu, "
          "but only given %lu",
          taps_required, taps_passed_in);
      return RCC_FATAL;
    }

    // Check taps after required are all zeros, as buffer is max_size
    for (size_t index = taps_required; index < taps_passed_in; ++index) {
      if (properties().taps[index] != 0) {
        setError(
            "Invalid number of taps supplied, expected %lu, "
            "but non-zero value outside of this range",
            taps_required);
        return RCC_FATAL;
      }
    }

    if ((this->decimator_real != nullptr) &&
        (this->decimator_imag != nullptr)) {
      // Update decimator with new taps
      const int16_t* taps = properties().taps;
      for (size_t index = 0; index < taps_required; ++index) {
        this->decimator_real->set_tap(index, taps[index]);
        this->decimator_imag->set_tap(index, taps[index]);
      }
    }
    return RCC_OK;
  }

  // Convert properties().rounding_type into dsp enum type
  sdr_math::rounding_type get_rounding_type() {
    // Default is convergent rounding
    auto defaultValue = sdr_math::rounding_type::MATH_HALF_EVEN;
    Rounding_type propVal = properties().rounding_type;

    switch (propVal) {
      case ROUNDING_TYPE_HALF_EVEN:
        return sdr_math::rounding_type::MATH_HALF_EVEN;
      case ROUNDING_TYPE_HALF_UP:
        return sdr_math::rounding_type::MATH_HALF_UP;
      case ROUNDING_TYPE_TRUNCATE:
        return sdr_math::rounding_type::MATH_TRUNCATE;
      default:
        log(OCPI_LOG_BAD,
            "Unexpected rounding_type (%d), "
            "using default instead (%d)",
            propVal, defaultValue);
        return defaultValue;
    }
  }

  // Notification that taps property has been written
  RCCResult taps_written() { return set_taps(); }

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_fraction = properties().group_delay_fractional;
    return RCC_OK;
  }

  RCCResult start() {
    // Get decimator class setup
    this->decimation_factor = properties().decimation_factor;
    this->taps_per_branch = properties().taps_per_branch;
    sdr_math::rounding_type rounding = get_rounding_type();
    this->group_delay_seconds = properties().group_delay_seconds;
    this->group_delay_fraction = properties().group_delay_fractional;

    this->decimator_real =
        new dsp::polyphase_decimator<int16_t, int32_t, int64_t>(
            this->decimation_factor, this->taps_per_branch, rounding,
            "real_decimator_xs");
    this->decimator_imag =
        new dsp::polyphase_decimator<int16_t, int32_t, int64_t>(
            this->decimation_factor, this->taps_per_branch, rounding,
            "imag_decimator_xs");

    return set_taps();
  }

  RCCResult run(bool) {
    if (input.eof()) {
      // Received EOF
      if (this->missed_timestamp) {
        return send_time_to_output(false);
      }

      output.setEOF();
      return RCC_ADVANCE_FINISHED;
    }
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      const Complex_short_timed_sampleSampleData* samples =
          input.sample().data().data();
      size_t len = input.sample().data().size();

      return handle_samples(samples, len, false);
    }
    if (input.opCode() == Complex_short_timed_sampleTime_OPERATION) {
      // Calc time of next output sample
      // and forward to output
      return handle_time();
    }
    if (input.opCode() == Complex_short_timed_sampleSample_interval_OPERATION) {
      // Calculate output sample interval as
      // input interval * decimation_factor
      // and forward to output
      return handle_interval();
    }
    if (input.opCode() == Complex_short_timed_sampleFlush_OPERATION) {
      // Flush buffers with zeros, if req'd
      // and forward flush onto ouput
      return handle_flush();
    }
    if (input.opCode() == Complex_short_timed_sampleDiscontinuity_OPERATION) {
      if (this->missed_timestamp) {
        // Output the time, before the discontinuity
        return this->send_time_to_output(false);
      }
      // Reset input buffer
      reset();
      // Pass through discontinuity opcode
      output.setOpCode(Complex_short_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    }
    if (input.opCode() == Complex_short_timed_sampleMetadata_OPERATION) {
      // Forward to output port
      output.setOpCode(Complex_short_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    }

    setError("Unknown OpCode received");
    return RCC_FATAL;
  }

  RCCResult handle_interval() {
    // Calculate output sample interval as
    // input interval * decimation_factor
    this->input_interval_seconds = input.sample_interval().seconds();
    this->input_interval_fraction = input.sample_interval().fraction();

    uint32_t output_interval_seconds = this->input_interval_seconds;
    uint64_t output_interval_fraction = this->input_interval_fraction;
    time_utils::multiply(&output_interval_seconds, &output_interval_fraction,
                         this->decimation_factor);

    output.setOpCode(Complex_short_timed_sampleSample_interval_OPERATION);
    output.sample_interval().seconds() = output_interval_seconds;
    output.sample_interval().fraction() = output_interval_fraction;
    return RCC_ADVANCE;
  }

  RCCResult handle_time() {
    this->output_time_seconds = input.time().seconds();
    this->output_time_fraction = input.time().fraction();

    // We want to output the time of the input sample that is the first in a
    // decimated block, i.e. the one after an output sample.
    this->samples_until_time =
        (this->decimator_real->get_samples_until_next_output() %
         this->decimation_factor);

    // Subtract group delay from time, wrapping around from zero to max time if
    // required
    time_utils::subtract(&this->output_time_seconds,
                         &this->output_time_fraction, this->group_delay_seconds,
                         this->group_delay_fraction);

    if (this->samples_until_time == 0) {
      // Time is time of next sample, so lets output time now
      return send_time_to_output(true);
    }

    // Not sending time now, so advance input past the input time, but
    // send nothing on output
    this->missed_timestamp = true;
    input.advance();
    return RCC_OK;
  }

  RCCResult send_time_to_output(bool advance_input) {
    output.setOpCode(Complex_short_timed_sampleTime_OPERATION);
    output.time().seconds() = this->output_time_seconds;
    output.time().fraction() = this->output_time_fraction;
    // Set output time to zero, so it's not output again later.
    this->output_time_seconds = 0;
    this->output_time_fraction = 0;
    this->missed_timestamp = false;
    this->samples_until_time = 0;

    if (advance_input) {
      return RCC_ADVANCE;
    }
    // We don't want to advance input - advance output only.
    output.advance();
    return RCC_OK;
  }

  RCCResult handle_flush() {
    // Flush zeros if samples have been received.
    if (this->received_data) {
      // Flush buffers by passing in zeros.
      size_t len = this->taps_per_branch * this->decimation_factor;
      Complex_short_timed_sampleSampleData samples[len];
      for (size_t i = 0; i < len; i++) {
        samples[i].real = 0;
        samples[i].imaginary = 0;
      }

      RCCResult result = handle_samples(samples, len, true);

      if ((this->sample_offset > 0) || this->missed_timestamp) {
        // Not flushed all the zeros and/or time through yet.
        return result;
      }

      // All flushed through - complete flush process
      this->received_data = false;
      // Reset input buffer and branch number
      reset();
      return result;
    }

    // Forward the flush opcode to output
    output.setOpCode(Complex_short_timed_sampleFlush_OPERATION);
    return RCC_ADVANCE;
  }

  // Handles the decimation, return the number of samples copied into the
  // output stream
  RCCResult handle_samples(const Complex_short_timed_sampleSampleData* samples,
                           size_t input_length, bool is_flush) {
    // Don't want to advance past input flush opcode, as will still need to
    // send flush opcode after the flushed samples
    bool advance_input = !is_flush;
    // Output held time.
    if (this->missed_timestamp) {
      if (this->samples_until_time == 0) {
        // we have a time to output, and no samples need outputting first,
        // so output time now
        if (this->sample_offset < input_length) {
          // We don't want to advance input - as we haven't finished with
          // all the samples yet - advance output only.
          advance_input = false;
        } else {  // (sample_offset >= input_length)
          // We've processed all the samples in this message
          this->sample_offset = 0;
        }
        return send_time_to_output(advance_input);
      } else if ((this->sample_offset + this->samples_until_time) >
                 input_length) {
        // Need more samples than is in the current message to finish the
        // current decimation run, so process all this message
        this->samples_until_time -= (input_length - this->sample_offset);
        this->sample_offset = 0;
      } else {
        // Only want to process some samples then stop to output time
        input_length = this->samples_until_time;
        this->samples_until_time = 0;
        this->sample_offset = input_length;
        advance_input = false;
      }

      // Advance the time counter by sample interval for each processed
      // sample, if have a sample interval.
      if (this->input_interval_seconds != 0 ||
          this->input_interval_fraction != 0) {
        for (size_t i = 0; i < input_length; i++) {
          time_utils::add(&output_time_seconds, &output_time_fraction,
                          input_interval_seconds, input_interval_fraction);
        }
      }
    } else {
      // Don't need to output time
      if (this->sample_offset > 0) {
        // Need to skip samples we've already processed from this message
        samples += this->sample_offset;
        input_length -= this->sample_offset;
        this->sample_offset = 0;
      }
    }

    size_t output_length = 0;
    if (input_length > 0) {
      // Note that we've received data into our decimator
      this->received_data = true;

      // Create array to hold output and reserve space for it
      std::vector<Complex_short_timed_sampleSampleData> decimated_samples;
      decimated_samples.reserve((input_length / this->decimation_factor) + 1);

      // Run decimation
      Complex_short_timed_sampleSampleData output_sample;
      for (size_t i = 0; i < input_length; i++) {
        bool real_ready = this->decimator_real->process_sample(
            samples->real, &output_sample.real);
        bool imag_ready = this->decimator_imag->process_sample(
            samples->imaginary, &output_sample.imaginary);
        samples++;
        if (real_ready != imag_ready) {
          setError("Real/Imag decimators disagree on when to output!");
          return RCC_FATAL;
        }
        if (real_ready) {
          decimated_samples.push_back(output_sample);
        }
      }

      // Place decimated samples into the output
      output_length = decimated_samples.size();
      if (output_length > 0) {
        output.setOpCode(Complex_short_timed_sampleSample_OPERATION);
        Complex_short_timed_sampleSampleData* output_data =
            output.sample().data().data();
        output.sample().data().resize(output_length);

        for (size_t i = 0; i < output_length; i++) {
          *output_data++ = decimated_samples[i];
        }
      }
    }

    if (output_length > 0) {
      // Samples produced, advance output and input if req'd.
      if (advance_input) {
        return RCC_ADVANCE;
      }
      output.advance();
      return RCC_OK;
    }

    // No samples produced, don't advance output
    if (advance_input) {
      input.advance();
    }
    return RCC_OK;
  }
};

POLYPHASE_DECIMATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
POLYPHASE_DECIMATOR_XS_END_INFO
