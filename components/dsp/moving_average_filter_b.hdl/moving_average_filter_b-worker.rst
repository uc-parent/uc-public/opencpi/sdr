.. moving_average_filter_b HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _moving_average_filter_b-HDL-worker:


``moving_average_filter_b`` HDL Worker
======================================
HDL implementation with settable maximum moving average window size.

Detail
------
.. ocpi_documentation_worker::

``maximum_moving_average_size`` should be set to be :math:`(2^n) - 1`, where :math:`n` is a positive integer. Larger window sizes will use more registers in the FPGA.

Utilisation
-----------
.. ocpi_documentation_utilization::
