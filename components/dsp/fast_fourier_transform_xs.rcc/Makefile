# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

# This is the Makefile for worker fast_fourier_transform_xs.rcc
#
# Checks the build configurations in fast_fourier_transform_xs-build.xml
# to see if the kiss_fft core is to be used. If so, checks the kissfft
# prerequisite is present (attempting to download it if not), and then
# adds the kiss_fft.c source to the "SourceFiles" list.
#
# If the kiss_fft prerequisite is not present, and no build configuration
# requires it, this worker can be built cleanly.  If there is a kiss fft
# build configuration and the prerequisite cannot not be obtained, the
# component will fail to build.

# Get location of prerequisites script
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))
prereq_script := $(mkfile_dir)../../../prerequisites/kiss_fft/install-kiss_fft.sh

include $(OCPI_CDK_DIR)/include/worker.mk

FFT_CORE_IMPLEMENTATION := $(foreach f, $(filter Param_%_fast_fourier_transform_xs_fft_core_implementation, $(.VARIABLES)), $($f))

ifneq (,$(findstring FAST_FOURIER_TRANSFORM_XS_FFT_CORE_IMPLEMENTATION_KISS_FFT,$(FFT_CORE_IMPLEMENTATION)))
    # Add the new source file to both the general, and Worker derived set of
    # files
    SourceFiles += gen/kissfft/kiss_fft.c

    # Manually add the object file to the RCC target configurations (this eval
    # Normally happens on the call to
    # `include $(OCPI_CDK_DIR)/include/worker.mk`)
    $(foreach t, $(RccTargets), \
      $(foreach c, $(ParamConfigurations), \
         $(eval $(call WkrMakeObject,gen/kissfft/kiss_fft.c,$t,$c))))

    # Make GNU make run the shell command now, as the rule name expansion
    # occurs at different times depending upon the worker target compiler
    GET_KISS_FFT := $(shell $(prereq_script) fast_fourier_transform_xs.rcc)

endif
