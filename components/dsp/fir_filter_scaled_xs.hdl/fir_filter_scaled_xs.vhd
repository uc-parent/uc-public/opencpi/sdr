-- HDL Implementation of FIR Filter XS
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
use ocpi.wci.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.fir_filter;
use sdr_dsp.sdr_dsp.rounding_halfup;
use sdr_dsp.sdr_dsp.rounding_halfeven;
use sdr_dsp.sdr_dsp.rounding_truncate;

architecture rtl of worker is

  -- Component constants
  constant tap_width_c          : integer := 16;
  constant num_taps_c           : natural := to_integer(number_of_taps);
  constant num_multipliers_c    : natural := to_integer(number_of_multipliers);
  constant data_in_width_c      : integer := (input_in.data'length)/2;
  constant tap_addr_width_c     : integer := integer(ceil(log2(real(num_taps_c))));
  constant fir_out_width_c      : integer := data_in_width_c + tap_width_c + tap_addr_width_c;
  constant data_out_width_c     : integer := (output_out.data'length)/2;
  -- Delay constants
  constant fir_delay_c             : integer := integer(ceil(real(num_taps_c) / real(num_multipliers_c)));
  constant mac_delay_c             : integer := 3;
  constant num_serial_adders_c     : integer := integer(ceil(real(num_multipliers_c) / real(fir_delay_c)));
  constant serial_adder_delay_c    : integer := integer(ceil(real(num_multipliers_c) / real(num_serial_adders_c)));
  constant adder_tree_delay_c      : integer := integer(ceil(log2(real(num_serial_adders_c))));
  constant rounder_delay_c         : integer := 3;
  constant delay_c                 : integer := fir_delay_c + mac_delay_c + serial_adder_delay_c + adder_tree_delay_c + rounder_delay_c;
  -- Input interface signals
  signal take                   : std_logic;
  signal data_in_real           : signed(data_in_width_c - 1 downto 0);
  signal data_in_imag           : signed(data_in_width_c - 1 downto 0);
  signal fir_ready              : std_logic;
  signal input_valid            : std_logic;
  signal input_interface        : worker_input_in_t;
  signal discontinuity          : std_logic;
  signal flush                  : std_logic;
  signal flush_length           : unsigned(number_of_taps'range);
  signal no_data                : std_logic;

  -- FIR signals
  signal tap_data                  : signed(tap_width_c - 1 downto 0);
  signal tap_valid                 : std_logic;
  signal tap_address               : unsigned(integer(ceil(log2(real(num_taps_c)))) downto 0);
  -- Additional bit added to address width to deal with initialisation of
  -- non-power-of-2 taps/multipliers
  signal fir_data_out_real         : signed(fir_out_width_c - 1 downto 0);
  signal fir_data_out_imag         : signed(fir_out_width_c - 1 downto 0);
  signal rounded_fir_data_out      : std_logic_vector((2*data_out_width_c) - 1 downto 0);
  signal rounded_fir_data_out_real : signed(data_out_width_c - 1 downto 0);
  signal rounded_fir_data_out_imag : signed(data_out_width_c - 1 downto 0);
  -- Scale factor signals
  signal scale_factor              : unsigned(integer(ceil(log2(real(fir_out_width_c - data_out_width_c)))) - 1 downto 0);
  signal scale_factor_int          : integer range 0 to (fir_out_width_c - data_out_width_c);

begin

  -- Hold-off until initialised
  ctl_out.done <= '1';

  -- props_out set to default
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= btrue;
  props_out.raw.error <= bfalse;

  ------------------------------------------------------------------------------
  -- Process to check for flush or discontinuity, when no_data (i.e. no data
  -- samples have been passed to the FIR filters) then the flush length is
  -- overridden to 0.
  ------------------------------------------------------------------------------
  no_data_flag_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        no_data <= '1';
      else
        if fir_ready = '1' and input_valid = '1' then
          no_data <= '0';
        end if;
        if flush = '1' or discontinuity = '1' then
          no_data <= '1';
        end if;
      end if;
    end if;
  end process;

  flush_length <= number_of_taps when no_data = '0' else (others => '0');

  ------------------------------------------------------------------------------
  -- Flush inject
  ------------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when the FIR core and output port are ready.
  flush_insert_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output)/(input_in.data'length/8)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  ------------------------------------------------------------------------------
  -- Interface delay
  ------------------------------------------------------------------------------
  take          <= output_in.ready and fir_ready;
  input_valid   <= '1' when input_interface.valid = '1' and input_interface.opcode = complex_short_timed_sample_sample_op_e else '0';
  discontinuity <= '1' when input_interface.ready = '1' and take = '1'
                   and input_in.opcode = complex_short_timed_sample_discontinuity_op_e
                   else '0';
  flush <= '1' when input_interface.ready = '1' and take = '1'
           and input_interface.opcode = complex_short_timed_sample_flush_op_e
           else '0';
  data_in_real <= signed(input_interface.data(data_in_width_c - 1 downto 0));
  data_in_imag <= signed(input_interface.data((2*data_in_width_c) - 1 downto data_in_width_c));

  interface_delay_i : entity work.complex_short_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                    => ctl_in.clk,
      reset                  => ctl_in.reset,
      group_delay_seconds    => props_in.group_delay_seconds,
      group_delay_fractional => props_in.group_delay_fractional,
      enable                 => output_in.ready,
      take_in                => fir_ready,
      input_in               => input_interface,
      processed_stream_in    => rounded_fir_data_out,
      output_out             => output_out
      );

  ------------------------------------------------------------------------------
  -- FIR Primitive
  --
  -- Tap values, loaded on raw property interface, are setup for 16-bit data
  -- transfer, therefore raw.address(0) is ignored.
  -- tap_array()(31:16) is ignored as it isn't used.
  ------------------------------------------------------------------------------

  tap_load_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (props_in.raw.is_write = '1') and (props_in.raw.address(31 downto 1) < number_of_taps) then
        tap_valid <= '1';
      else
        tap_valid <= '0';
      end if;

      tap_address <= '0' & props_in.raw.address(tap_addr_width_c downto 1);

      -- Byte-enable controls 16-bit data transfer on 32-bit bus.
      -- NOTE: raw.byte_enable(0) and (1) or (2) and (3) actually set
      if (props_in.raw.byte_enable(0) = '1') then
        tap_data <= signed(props_in.raw.data(15 downto 0));
      else
        tap_data <= signed(props_in.raw.data(31 downto 16));
      end if;
    end if;
  end process;

  fir_filter_real_i : fir_filter
    generic map(
      data_in_width_g   => data_in_width_c,
      num_taps_g        => num_taps_c,
      tap_width_g       => tap_width_c,
      num_multipliers_g => num_multipliers_c
      )
    port map(
      clk            => ctl_in.clk,
      rst            => ctl_in.reset,
      data_rst       => discontinuity,
      enable         => output_in.ready,
      data_in_valid  => input_valid,
      take           => fir_ready,
      data_in        => data_in_real,
      tap_data_in    => tap_data,
      tap_valid_in   => tap_valid,
      tap_address_in => tap_address,
      data_out       => fir_data_out_real,
      data_valid_out => open
      );

  fir_filter_imag_i : fir_filter
    generic map(
      data_in_width_g   => data_in_width_c,
      num_taps_g        => num_taps_c,
      tap_width_g       => tap_width_c,
      num_multipliers_g => num_multipliers_c
      )
    port map(
      clk            => ctl_in.clk,
      rst            => ctl_in.reset,
      data_rst       => discontinuity,
      enable         => output_in.ready,
      data_in_valid  => input_valid,
      take           => open,
      data_in        => data_in_imag,
      tap_data_in    => tap_data,
      tap_valid_in   => tap_valid,
      tap_address_in => tap_address,
      data_out       => fir_data_out_imag,
      data_valid_out => open
      );
  ------------------------------------------------------------------------------
  -- Output scaling
  --
  -- data_valid_in/out not used as delay is fixed (dependent on constants that
  -- define the filter). Delay of this entity is 1 clock cycle.
  ------------------------------------------------------------------------------
  scale_factor     <= props_in.scale_factor(scale_factor'length - 1 downto 0);
  scale_factor_int <= to_integer(scale_factor);

  halfup_rounder_gen : if rounding_type = half_up_e generate
    halfup_rounder_real_i : rounding_halfup
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => false,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_real,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_real,
        data_valid_out => open
        );

    halfup_rounder_imag_i : rounding_halfup
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => false,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_imag,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_imag,
        data_valid_out => open
        );
  end generate;

  halfeven_rounder_gen : if rounding_type = half_even_e generate
    halfeven_rounder_real_i : rounding_halfeven
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => false,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_real,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_real,
        data_valid_out => open
        );

    halfeven_rounder_imag_i : rounding_halfeven
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => false,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_imag,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_imag,
        data_valid_out => open
        );
  end generate;

  truncate_rounder_gen : if rounding_type = truncate_e generate
    truncate_rounder_real_i : rounding_truncate
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => false,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_real,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_real,
        data_valid_out => open
        );

    truncate_rounder_imag_i : rounding_truncate
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => false,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_imag,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_imag,
        data_valid_out => open
        );
  end generate;

  truncate_saturated_rounder_gen : if rounding_type = truncate_with_saturation_e generate
    truncate_saturated_rounder_real_i : rounding_truncate
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => true,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_real,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_real,
        data_valid_out => open
        );

    truncate_saturated_rounder_imag_i : rounding_truncate
      generic map (
        input_width_g   => fir_out_width_c,
        output_width_g  => data_out_width_c,
        saturation_en_g => true,
        registers_g     => rounder_delay_c
        )
      port map (
        clk            => ctl_in.clk,
        reset          => ctl_in.reset,
        clk_en         => output_in.ready,
        data_in        => fir_data_out_imag,
        data_valid_in  => '0',
        binary_point   => scale_factor_int,
        data_out       => rounded_fir_data_out_imag,
        data_valid_out => open
        );
  end generate;

  rounded_fir_data_out <= std_logic_vector(rounded_fir_data_out_imag) & std_logic_vector(rounded_fir_data_out_real);

end rtl;
