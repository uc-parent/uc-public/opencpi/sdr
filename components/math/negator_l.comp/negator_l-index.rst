.. negator_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: inverse


.. _negator_l:


Negator (``negator_l``)
=======================
Multiplies input values by :math:`-1`.

Design
------
Multiplies input values by :math:`-1`, to change the sign of the values.

The mathematical representation of the implementation is given in :eq:`negator_l-equation`.

.. math::
   :label: negator_l-equation

   y[n] = -x[n]

In :eq:`negator_l-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`negator_l-diagram`.

.. _negator_l-diagram:

.. figure:: negator_l.svg
   :alt: Block diagram outlining negator implementation.
   :align: center

   Negator implementation.

Interface
---------
.. literalinclude:: ../specs/negator_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Only values in a sample opcode message are multiplied by :math:`-1`. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../negator_l.hdl ../negator_l.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``negator_l`` are:

 * The negation does not handle overflows caused by a full scale negative values being negated

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
