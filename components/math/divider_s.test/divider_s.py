#!/usr/bin/env python3

# Python implementation of divider for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of divider for testing."""

from argparse import ArgumentError
import numpy as np
import logging

import opencpi.ocpi_testing as ocpi_testing


def to_twos(value, bits):
    """Helper function to convert to 2's complement format.

    Args:
        value: (signed integer) input value to convert
        bits: (unsigned integer) number of bits in output
    Returns:
        Sign extended value as an unsigned integer
    """
    value &= ((2**bits)-1)
    if (value & (1 << (bits - 1))) != 0:
        value = value - (1 << bits)
    return value


class Divider(ocpi_testing.Implementation):
    """Python implementation of divider for testing."""

    def __init__(self,
                 division_method="floor",
                 bit_length=16,
                 case="",
                 logging_level="WARNING"):
        """Constructor for the class.

        Args:
            division_method: Type of division to use (floor or truncated)
            bit_length:    Size of the input data
            case:          Current case string to use for the logfile name
            logging_level: logging granularity to print
        """
        super().__init__()

        logging.basicConfig(level=logging_level,
                            filename=case+".divider_s.py.log",
                            filemode="w")

        self.input_ports = ["input", "denominator"]
        self.output_ports = ["output", "remainder"]
        self.non_sample_opcode_port_select = 0

        self._division_method = division_method

        self._bit_length = bit_length
        self._bit_mask = ((2**bit_length)-1)

        logging.info(
            f"Running {case} with division method: {division_method}, bit depth of {self._bit_length}")
        self.reset()

    def reset(self):
        """Reset the input and output buffers."""
        self._input_data = []
        self._denominator_data = []
        self._output_message_lengths = []

    def select_input(self, input_1, input_2):
        """Selects which input should be advanced.

        Args:
            input_*: Input message stream

        Returns:
            Which port should be advanced.
            If neither port has data, return None
        """
        # Pull through message if not a stream, so that both ports get a stream
        # message being the next on each port.
        if input_1 != "sample" and input_1 is not None:
            return 0
        if input_2 != "sample" and input_2 is not None:
            return 1

        # Otherwise only stream messages or no messages on port.
        if input_1 is not None:
            return 0
        if input_2 is not None:
            return 1

    def sample(self, input_1, input_2):
        """Sample Opcode function.

        Args:
            input_*: Input message stream

        Returns:
            Tuple of the output ports (each an array of dictionaries)
        """
        logging.info("Sample message received")

        if input_1 is not None:
            self._input_data = self._input_data + list(input_1)
            self._output_message_lengths.append(len(input_1))
        if input_2 is not None:
            self._denominator_data = self._denominator_data + list(input_2)

        quotient_messages = []
        remainder_messages = []
        while len(self._output_message_lengths) > 0:
            # Check there is enough data on both inputs for output
            if len(self._input_data) >= self._output_message_lengths[0] and \
                    len(self._denominator_data) >= self._output_message_lengths[0]:
                output_length = self._output_message_lengths.pop(0)
                value = self._get_stream_data(output_length)
                quotient_messages.append(
                    {"opcode": "sample", "data": value[0]})
                remainder_messages.append(
                    {"opcode": "sample", "data": value[1]})

                # Remove the data from the input port buffers that has been
                # processed
                self._input_data = self._input_data[output_length:]
                self._denominator_data = self._denominator_data[output_length:]
            else:
                break
        return self.output_formatter(quotient_messages, remainder_messages)

    def _step(self, numerator, denominator):
        """Non restoring divider calculation.

        Args:
            numerator: (int) Numerator to use in divider
            denominator: (int) Denominator to use in divider

        Returns:
            Tuple of quotient and remainder
        """
        double_bitmask = ((2**(2*self._bit_length))-1)

        denominator_is_negative = False
        if denominator < 0:
            # If negative denominator, then flip the signs of both numerator
            # and denominator.
            numerator = -numerator
            denominator = -denominator
            denominator_is_negative = True

        r = numerator & double_bitmask  # Modulo of divider
        d = (denominator << (self._bit_length)) & double_bitmask
        q = 0  # Integer divider value
        logging.debug(
            f"i = {self._bit_length}\tr = {numerator}:{r:+032b}\td = {denominator:4}:{d:+032b}")
        for i in range(self._bit_length-1, -1, -1):
            if r & (1 << ((2*self._bit_length)-1)) == 0:
                q += 1
                r = (r << 1) - d
                r = r & double_bitmask
                logging.debug(
                    f"- i = {i}\tr = {r}:{r:x}:{r:+033b}\tq = {q:4}:{q:+017b}")
            else:
                q += 0
                r = (r << 1) + d
                r = r & double_bitmask
                logging.debug(
                    f"+ i = {i}\tr = {r}:{r:x}:{r:+033b}\tq = {q:4}:{q:+017b}")
            q <<= 1
        logging.debug(f"{q}:{q:+017b}")

        q >>= 1

        r_negative = (r & (1 << ((2*self._bit_length)-1))) > 0
        q_negative = (q & (1 << ((self._bit_length)-1))) > 0

        logging.debug(
            ": i = {0}\tr = {1}:{1:+033b}\tq = {2:4}:{2:+017b}, q_neg: {3}, r_neg: {4}, neg{5}".format(
                i, r, q, q_negative, r_negative, denominator_is_negative
            ))

        if r == 0:
            logging.debug("0")
            q = ((q - (q ^ self._bit_mask)) & self._bit_mask)

        elif r_negative:
            if self._division_method.lower() == "floor":
                q = ((q - (q ^ self._bit_mask)) & self._bit_mask) - 1
                if denominator_is_negative:
                    r = -(r + d)
                else:
                    r = (r + d)
            else:  # Truncate
                if denominator_is_negative:
                    if q_negative:
                        logging.debug("a")
                        r = -(r + d)
                        q = ((q - ((q ^ self._bit_mask) + 1)) & self._bit_mask)
                    else:
                        logging.debug("b")
                        r = -(r)
                        q = ((q - ((q ^ self._bit_mask))) & self._bit_mask)
                else:
                    if q_negative:
                        logging.debug("c1")
                        r = (r + d)
                        q = ((q - ((q ^ self._bit_mask) + 1)) & self._bit_mask)
                    else:
                        logging.debug("c2")
                        r = r
                        q = ((q - ((q ^ self._bit_mask))) & self._bit_mask)

        else:
            if self._division_method.lower() == "floor":
                q = ((q - (q ^ self._bit_mask)) & self._bit_mask)
                if denominator_is_negative:
                    r = -r
                else:
                    r = r
            else:
                if denominator_is_negative:
                    if q_negative:
                        logging.debug("d")
                        r = -(r)
                        q = ((q - (q ^ self._bit_mask)) & self._bit_mask)
                    else:
                        logging.debug("e")
                        r = -(r - d)
                        q = ((q - ((q ^ self._bit_mask) - 1)) & self._bit_mask)
                else:
                    if q_negative:
                        logging.debug("f")
                        r = r
                        q = ((q - (q ^ self._bit_mask)) & self._bit_mask)
                    else:
                        r = r - d
                        logging.debug("g")
                        q = ((q - ((q ^ self._bit_mask) - 1)) & self._bit_mask)

        r >>= self._bit_length

        logging.debug(
            f"; i = {i}\tr = {r}:{r:+033b}\tq = {q:4}:{q:+017b}, {(q & (1 << (self._bit_length-1)))}")

        q = to_twos(q, self._bit_length)
        r = to_twos(r, self._bit_length)

        if r == -denominator:
            q -= 1
            r = 0

        logging.debug(
            f"i = {i}\tr = {r}:{r:+033b}\tq = {q:4}:{int(q):+017b}")
        return (q, r)

    def _get_stream_data(self, output_length):
        """Calculate the output values, using the input data array.

        Args:
            output_length (int): Number of samples to include in the
                                 output message.

        Returns:
            Tuple of arrays: (quotient, remainder)
        """
        quotient = [0] * output_length
        remainder = [0] * output_length

        for index, (numerator, denominator) in enumerate(zip(
                self._input_data[0:output_length],
                self._denominator_data[0:output_length])):
            # This is the actual value to use.
            if self._division_method.lower() == "floor":
                if denominator == 0:
                    quotient[index] = 0 if numerator < 0 else -1
                    remainder[index] = numerator
                else:
                    quotient[index] = int(
                        int(numerator) // int(denominator))
                    remainder[index] = numerator % denominator
            elif self._division_method.lower() == "truncate":
                if denominator == 0:
                    quotient[index] = 1 if numerator < 0 else -1
                    remainder[index] = numerator
                else:
                    quotient[index] = int(numerator / denominator)
                    remainder[index] = np.fmod(numerator, denominator)
            else:
                logging.error(
                    f"Unrecognised division method: {self._division_method}")
                raise ArgumentError("Unrecognised division method")

            # The following generates an compares a python model of
            # non restoring division, as opposed to using the inbuilt
            # integer division operator
            nr_division = self._step(numerator, denominator)
            logging.debug(
                f"in1:{numerator}, in2:{denominator}, out:{quotient[index]}, mod:{remainder[index]}, div:{nr_division}")
            if denominator != 0 and (quotient[index] != nr_division[0] or remainder[index] != nr_division[1]):
                logging.warning(
                    f"Non-restoring division didn't match expected {quotient[index]}:{remainder[index]}=={nr_division}")
                # raise ValueError(
                #     "Non-restoring division didn't match C division")

            # Assert that they follow the rules of division and remainders,
            # equalling the original product.
            if denominator != 0 and numerator != denominator * quotient[index] + remainder[index]:
                logging.warning(
                    "divisor = dividend * quotient + remainder: {} = {} [{} * {} + {}]".format(
                        numerator, denominator *
                        quotient[index] + remainder[index],
                        denominator, quotient[index], remainder[index]))
                raise ValueError(
                    "Division and remainder seem to be following different methods"
                )

        return (quotient, remainder)

    def time(self, *inputs):
        """Get messages resulting from a time message.

        Set with default behaviour here, however if a different behaviour is
        needed this should be overridden by the specific implementation. A
        specific implementation can either have code to implement the expected
        behaviour or call a different time case (e.g. self._time_fixed_delay).

        Args:
            inputs* (floats): Time value for the input ports. There should be
                as many inputs arguments as there are input ports. Set to None
                if no time message on a port (used when a time message is
                present on another port).

        Returns:
            A named tuple where each item in the tuple relates to each output
                port. Each tuple element (port) is a list of dictionaries which
                are the output messages for the port. The order of lists in the
                tuple, so order of port's outputs, matches that as defined in
                self.output_ports; the names used for the named tuple are that
                as defined in self.output_ports. The message dictionaries
                within the lists within the named tuple, have the keys "opcode"
                and "data".
        """
        logging.info("Time message received")

        # Get the input value from the port that is being used for non-sample
        # input messages.
        logging.debug(
            f"Looking for Time Message on port {self.input_ports[self.non_sample_opcode_port_select]}")
        time = inputs[self.non_sample_opcode_port_select]
        logging.debug(f"Time value is: {time}")

        if time is not None:
            return self.output_formatter(*[[{"opcode": "time", "data": time}] for _ in self.output_ports])
        else:
            other_outputs = [[]] * (len(self.output_ports))
            return self.output_formatter(*other_outputs)

    def sample_interval(self, *inputs):
        """Get messages resulting from a sample interval message.

        Set with default behaviour here, however if a different behaviour is
        needed this should be overridden by the specific implementation. A
        specific implementation can either have code to implement the expected
        behaviour or call a different sample interval behaviour defined in this
        class.

        Args:
            inputs* (floats): Sample interval value for the input ports. There
                should be as many inputs arguments as there are input ports.
                Set to None if no sample interval message on a port (used when
                a sample interval message is present on another port).

        Returns:
            A named tuple where each item in the tuple relates to each output
                port. Each tuple element (port) is a list of dictionaries which
                are the output messages for the port. The order of lists in the
                tuple, so order of port's outputs, matches that as defined in
                self.output_ports; the names used for the named tuple are that
                as defined in self.output_ports. The message dictionaries
                within the lists within the named tuple, have the keys "opcode"
                and "data".
        """
        logging.info("Sample Interval message received")

        # Get the input value from the port that is being used for non-sample
        # input messages.
        logging.debug(
            f"Looking for Sample Interval Message on port {self.input_ports[self.non_sample_opcode_port_select]}")
        sample_interval = inputs[self.non_sample_opcode_port_select]
        logging.debug(f"Sample Interval value is: {sample_interval}")

        if sample_interval is not None:
            return self.output_formatter(*[[{"opcode": "sample_interval", "data": sample_interval}] for _ in self.output_ports])
        else:
            outputs = [[]] * len(self.output_ports)
            return self.output_formatter(*outputs)

    def flush(self, *inputs):
        """Get messages resulting from a flush message.

        Set with default behaviour here, however if a different behaviour is
        needed this should be overridden by the specific implementation. A
        specific implementation can either have code to implement the expected
        behaviour or call a different flush behaviour defined in this class.

        Args:
            inputs* (bools): Indicate if a flush message has been received on
                an input port (True) or not received (False). There must be as
                many inputs arguments as there are input ports. The order of
                ports and inputs arguments matches that defined in
                self.input_ports.

        Returns:
            A named tuple where each item in the tuple relates to each output
                port. Each tuple element (port) is a list of dictionaries which
                are the output messages for the port. The order of lists in the
                tuple, so order of port's outputs, matches that as defined in
                self.output_ports; the names used for the named tuple are that
                as defined in self.output_ports. The message dictionaries
                within the lists within the named tuple, have the keys "opcode"
                and "data".
        """
        logging.info("Flush message received")

        # Get the input value from the port that is being used for non-sample
        # input messages.
        logging.debug(
            f"Looking for Flush Message on port {self.input_ports[self.non_sample_opcode_port_select]}")
        flush = inputs[self.non_sample_opcode_port_select]
        logging.debug(f"Flush value is: {flush}")

        if flush is True:
            return self.output_formatter(*[[{"opcode": "flush", "data": None}] for _ in self.output_ports])
        else:
            outputs = [[]] * len(self.output_ports)
            return self.output_formatter(*outputs)

    def discontinuity(self, *inputs):
        """Get messages resulting from a discontinuity message.

        Set with default behaviour here, however if a different behaviour is
        needed this should be overridden by the specific implementation. A
        specific implementation can either have code to implement the expected
        behaviour or call a different discontinuity behaviour defined in this
        class.

        Args:
            inputs* (bools): Indicate if a discontinuity message has been
                received on an input port (True) or not received (False). There
                must be as many inputs arguments as there are input ports. The
                order of ports and inputs arguments matches that defined in
                self.input_ports.

        Returns:
            A named tuple where each item in the tuple relates to each output
                port. Each tuple element (port) is a list of dictionaries which
                are the output messages for the port. The order of lists in the
                tuple, so order of port's outputs, matches that as defined in
                self.output_ports; the names used for the named tuple are that
                as defined in self.output_ports. The message dictionaries
                within the lists within the named tuple, have the keys "opcode"
                and "data".
        """
        logging.info("Discontinuity message received")

        # Get the input value from the port that is being used for non-sample
        # input messages.
        logging.debug(
            f"Looking for Discontinuity Message on port {self.input_ports[self.non_sample_opcode_port_select]}")
        discontinuity = inputs[self.non_sample_opcode_port_select]
        logging.debug(f"Discontinuity value is: {discontinuity}")

        if discontinuity is True:
            return self.output_formatter(*[[{"opcode": "discontinuity", "data": None}] for _ in self.output_ports])
        # Drop discontinuity on any port other than the selected input.
        else:
            outputs = [[]] * len(self.output_ports)
            return self.output_formatter(*outputs)

    def metadata(self, *inputs):
        """Get messages resulting from a metadata message.

        Set with default behaviour here, however if a different behaviour is
        needed this should be overridden by the specific implementation. A
        specific implementation can either have code to implement the expected
        behaviour or call a different metadata behaviour defined in this class.

        Args:
            inputs* (dict): Input metadata for input ports. Metadata must be
                given as dictionaries with the keys "id" and "value". There
                must be as many inputs arguments as there are input ports. The
                order of ports and inputs arguments matches that defined in
                ``self.input_ports``.

        Returns:
            A named tuple where each item in the tuple relates to each output
                port. Each tuple element (port) is a list of dictionaries which
                are the output messages for the port. The order of lists in the
                tuple, so order of port's outputs, matches that as defined in
                self.output_ports; the names used for the named tuple are that
                as defined in self.output_ports. The message dictionaries
                within the lists within the named tuple, have the keys "opcode"
                and "data".
        """
        logging.info("Metadata message received")

        # Get the input value from the port that is being used for non-sample
        # input messages.
        logging.debug(
            f"Looking for Metadata Message on port {self.input_ports[self.non_sample_opcode_port_select]}")
        metadata = inputs[self.non_sample_opcode_port_select]
        logging.debug(f"Metadata value is: {metadata}")

        if metadata is not None:
            return self.output_formatter(*[[{"opcode": "metadata", "data": metadata}] for _ in self.output_ports])
        # Drop metadata on any port other than the selected input.
        else:
            outputs = [[]] * len(self.output_ports)
            return self.output_formatter(*outputs)
