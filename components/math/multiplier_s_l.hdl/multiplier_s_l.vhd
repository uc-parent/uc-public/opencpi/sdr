-- multiplier_s_l HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  constant delay_c : integer := 0;

  signal input_1_take   : std_logic;
  signal input_2_take   : std_logic;
  signal multiplier_out : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  input_1_out.take <= input_1_take;
  input_2_out.take <= input_2_take;

  -- Always take non-sample opcodes from both inputs.
  -- Only take sample opcodes if both ports have a sample opcode.
  input_take_logic_p : process (output_in.ready, input_1_in, input_2_in)
  begin
    -- INPUT 1
    -- If input and output are ready
    input_1_take <= '0';
    if output_in.ready = '1' then
      -- Always take when a non-sample opcode or not valid
      if input_1_in.opcode /= short_timed_sample_sample_op_e or input_1_in.valid = '0' then
        input_1_take <= '1';
      -- Only take a sample opcode if input 2 is valid,
      -- and also a sample opcode
      elsif input_2_in.valid = '1' and
        input_2_in.opcode = short_timed_sample_sample_op_e then
        input_1_take <= '1';
      end if;
    end if;
    -- INPUT 2
    -- If input and output are ready
    input_2_take <= '0';
    if output_in.ready = '1' then
      -- Always take when a non-sample opcode or not valid
      if input_2_in.opcode /= short_timed_sample_sample_op_e or input_2_in.valid = '0' then
        input_2_take <= '1';
      -- Only take a sample opcode if input 1 is valid, and also a
      -- sample opcode.
      elsif input_1_in.valid = '1' and
        input_1_in.opcode = short_timed_sample_sample_op_e then
        input_2_take <= '1';
      end if;
    end if;
  end process;

  -- This handles the translation of non-sample message data from
  -- the narrow input to wider output.
  -- It has zero delay to match the calculation below.
  interface_delay_i : entity work.short_to_long_protocol_delay
    generic map (
      delay_g          => delay_c,
      data_in_width_g  => input_1_in.data'length,
      data_out_width_g => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => input_1_take,
      input_in            => input_1_in,
      processed_stream_in => multiplier_out,
      output_out          => output_out
      );

  -- Performs multiplication operation
  multiplier_out <= std_logic_vector(signed(input_1_in.data) * signed(input_2_in.data));

end rtl;
