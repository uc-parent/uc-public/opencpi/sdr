// Unit tests for the time_utils common functions
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <tuple>

#include "../time_utils.hh"
#include "gtest/gtest.h"

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11
// time_utils_gtest.cc -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -o time_utils_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/
// ./time_utils_gtest

// ***********************************************************************
// Define parameterized tests for the 32 bit addition function
// i.e. add(uint32_t*, uint64_t*, uint32_t, uint64_t)

struct Addition32BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint32_t, uint64_t, uint32_t, uint64_t>> {};

TEST_P(Addition32BitTestFixture, Add32BitTime) {
  uint32_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint32_t seconds_to_add = std::get<2>(GetParam());
  uint64_t fraction_to_add = std::get<3>(GetParam());

  __uint128_t expected_time =
      (static_cast<__uint128_t>(current_seconds) << 64) + current_fraction;
  expected_time +=
      (static_cast<__uint128_t>(seconds_to_add) << 64) + fraction_to_add;

  uint32_t expected_seconds = static_cast<uint32_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::add(&current_seconds, &current_fraction, seconds_to_add,
                  fraction_to_add);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Addition of 32 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Subtraction of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Addition32BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 0xFF, 0xFF000000),
        std::make_tuple(0, 0x8000000000000000, 0, 0x8000000000000000),
        std::make_tuple(0xFFFFFFFF, 0, 1, 0),
        std::make_tuple(
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))))));

// ***********************************************************************
// Define parameterized tests for the 64 bit addition function
// i.e. add(uint64_t*, uint64_t*, uint64_t, uint64_t)

struct Addition64BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint64_t, uint64_t, uint64_t, uint64_t>> {};

TEST_P(Addition64BitTestFixture, Add64BitTime) {
  uint64_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint64_t seconds_to_add = std::get<2>(GetParam());
  uint64_t fraction_to_add = std::get<3>(GetParam());

  __uint128_t expected_time =
      (static_cast<__uint128_t>(current_seconds) << 64) + current_fraction;
  expected_time +=
      (static_cast<__uint128_t>(seconds_to_add) << 64) + fraction_to_add;

  uint64_t expected_seconds = static_cast<uint64_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::add(&current_seconds, &current_fraction, seconds_to_add,
                  fraction_to_add);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Addition of 64 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Subtraction of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Addition64BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF00000000, 0xFFFFFF, 0xFFFF0000, 0xFF000000),
        std::make_tuple(0, 0x8000000000000000, 0, 0x8000000000000000),
        std::make_tuple(0xFFFFFFFFFFFFFFFF, 0, 1, 0),
        std::make_tuple(
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))))));

// ***********************************************************************
// Define parameterized tests for the 64 bit subtraction function
// i.e. subtract(uint32_t*, uint64_t*, uint32_t, uint64_t)

struct Subtract32BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint32_t, uint64_t, uint32_t, uint64_t>> {};

TEST_P(Subtract32BitTestFixture, Subtract32BitTime) {
  uint32_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint32_t seconds_to_subtract = std::get<2>(GetParam());
  uint64_t fraction_to_subtract = std::get<3>(GetParam());

  __uint128_t expected_time =
      (static_cast<__uint128_t>(current_seconds) << 64) + current_fraction;
  expected_time -= ((static_cast<__uint128_t>(seconds_to_subtract) << 64) +
                    fraction_to_subtract);

  uint32_t expected_seconds = static_cast<uint32_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::subtract(&current_seconds, &current_fraction, seconds_to_subtract,
                       fraction_to_subtract);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Subtraction of 32 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Subtraction of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Subtract32BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 0xFF, 0xFF),
        std::make_tuple(0, 0, 1, 0), std::make_tuple(0, 0, 0, 1),
        std::make_tuple(
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))))));

// ***********************************************************************
// Define parameterized tests for the 32 bit multiplication function
// with 8 bit multiplier
// i.e. multiply(uint32_t*, uint64_t*, uint8_t)

struct Multiply32BitBy8BitTestFixture
    : public ::testing::TestWithParam<std::tuple<uint32_t, uint64_t, uint8_t>> {
};

TEST_P(Multiply32BitBy8BitTestFixture, Multiply32BitTimeBy8Bit) {
  uint32_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint8_t multiplication_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time *= multiplication_factor;

  uint32_t expected_seconds = static_cast<uint32_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::multiply(&current_seconds, &current_fraction,
                       multiplication_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Multiplication of 32 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Multiplication of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Multiply32BitBy8BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x10),
        std::make_tuple(0x10000000, 0, 0x10),
        std::make_tuple(0, 0x1000000000000000, 0x10),
        std::make_tuple(
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint8_t>(std::rand() %
                                 static_cast<int>(std::pow(2, 8))))));

// ***********************************************************************
// Define parameterized tests for the 32 bit multiplication function
// i.e. multiply(uint32_t*, uint64_t*, uint16_t)

struct Multiply32BitBy16BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint32_t, uint64_t, uint16_t>> {};

TEST_P(Multiply32BitBy16BitTestFixture, Multiply32BitTimeBy16Bit) {
  uint32_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint16_t multiplication_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time *= multiplication_factor;

  uint32_t expected_seconds = static_cast<uint32_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::multiply(&current_seconds, &current_fraction,
                       multiplication_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Multiplication of 32 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Multiplication of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Multiply32BitBy16BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x10),
        std::make_tuple(0x10000000, 0, 0x100),
        std::make_tuple(0, 0x1000000000000000, 0x100),
        std::make_tuple(
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint8_t>(std::rand() %
                                 static_cast<int>(std::pow(2, 16))))));

// ***********************************************************************
// Define parameterized tests for the 64 bit multiplication function
// i.e. multiply(uint64_t*, uint64_t*, uint8_t)

struct Multiply64BitBy8BitTestFixture
    : public ::testing::TestWithParam<std::tuple<uint64_t, uint64_t, uint8_t>> {
};

TEST_P(Multiply64BitBy8BitTestFixture, Multiply64BitTimeBy8Bit) {
  uint64_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint8_t multiplication_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time *= multiplication_factor;

  uint64_t expected_seconds = static_cast<uint64_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::multiply(&current_seconds, &current_fraction,
                       multiplication_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Multiplication of 64 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Multiplication of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Multiply64BitBy8BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x10),
        std::make_tuple(0x1000000000000000, 0, 0x10),
        std::make_tuple(0, 0x1000000000000000, 0x10),
        std::make_tuple(
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint8_t>(std::rand() %
                                 static_cast<int>(std::pow(2, 8))))));

// ***********************************************************************
// Define parameterized tests for the 64 bit multiplication function
// i.e. multiply(uint64_t*, uint64_t*, uint16_t)

struct Multiply64BitBy16BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint64_t, uint64_t, uint16_t>> {};

TEST_P(Multiply64BitBy16BitTestFixture, Multiply64BitTimeBy16Bit) {
  uint64_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint8_t multiplication_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time *= multiplication_factor;

  uint64_t expected_seconds = static_cast<uint64_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::multiply(&current_seconds, &current_fraction,
                       multiplication_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Multiplication of 64 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Multiplication of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Multiply64BitBy16BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x10),
        std::make_tuple(0x1000000000000000, 0, 0x100),
        std::make_tuple(0, 0x1000000000000000, 0x100),
        std::make_tuple(
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint8_t>(std::rand() %
                                 static_cast<int>(std::pow(2, 16))))));

// ***********************************************************************
// Define parameterized tests for the 32 bit division function
// i.e. divide(uint32_t*, uint64_t*, uint8_t)

struct Divide32BitBy8BitTestFixture
    : public ::testing::TestWithParam<std::tuple<uint32_t, uint64_t, uint8_t>> {
};

TEST_P(Divide32BitBy8BitTestFixture, Divide32BitTimeBy8Bit) {
  uint32_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint8_t division_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time /= division_factor;

  uint32_t expected_seconds = static_cast<uint32_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::divide(&current_seconds, &current_fraction, division_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Division of 32 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Division of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Divide32BitBy8BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x10), std::make_tuple(1, 0, 0x10),
        std::make_tuple(0, 0x1000000000000000, 0x10),
        std::make_tuple(
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint8_t>(std::rand() %
                                 static_cast<int>(std::pow(2, 8))))));

// ***********************************************************************
// Define parameterized tests for the 32 bit division function
// i.e. divide(uint32_t*, uint64_t*, uint16_t)

struct Divide32BitBy16BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint32_t, uint64_t, uint16_t>> {};

TEST_P(Divide32BitBy16BitTestFixture, Divide32BitTimeBy16Bit) {
  uint32_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint16_t division_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time /= division_factor;

  uint32_t expected_seconds = static_cast<uint32_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::divide(&current_seconds, &current_fraction, division_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Division of 32 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Division of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Divide32BitBy16BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x100), std::make_tuple(1, 0, 0x100),
        std::make_tuple(0, 0x1000000000000000, 0x100),
        std::make_tuple(
            static_cast<uint32_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 32))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint16_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 16))))));

// ***********************************************************************
// Define parameterized tests for the 64 bit division function
// i.e. divide(uint64_t*, uint64_t*, uint8_t)

struct Divide64BitBy8BitTestFixture
    : public ::testing::TestWithParam<std::tuple<uint64_t, uint64_t, uint8_t>> {
};

TEST_P(Divide64BitBy8BitTestFixture, Divide64BitTimeBy8Bit) {
  uint64_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint8_t division_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time /= division_factor;

  uint64_t expected_seconds = static_cast<uint64_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::divide(&current_seconds, &current_fraction, division_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Division of 64 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Division of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Divide64BitBy8BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x10), std::make_tuple(1, 0, 0x10),
        std::make_tuple(0, 0x1000000000000000, 0x10),
        std::make_tuple(
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint8_t>(std::rand() %
                                 static_cast<int>(std::pow(2, 8))))));

// ***********************************************************************
// Define parameterized tests for the 64 bit division function
// i.e. divide(uint64_t*, uint64_t*, uint16_t)

struct Divide64BitBy16BitTestFixture
    : public ::testing::TestWithParam<
          std::tuple<uint64_t, uint64_t, uint16_t>> {};

TEST_P(Divide64BitBy16BitTestFixture, Divide64BitTimeBy16Bit) {
  uint64_t current_seconds = std::get<0>(GetParam());
  uint64_t current_fraction = std::get<1>(GetParam());
  uint16_t division_factor = std::get<2>(GetParam());

  __uint128_t expected_time =
      ((static_cast<__uint128_t>(current_seconds) << 64) + current_fraction);
  expected_time /= division_factor;

  uint64_t expected_seconds = static_cast<uint64_t>(expected_time >> 64);
  uint64_t expected_fraction =
      static_cast<uint64_t>(expected_time & UINT64_MAX);

  time_utils::divide(&current_seconds, &current_fraction, division_factor);

  EXPECT_EQ(current_seconds, expected_seconds)
      << "Division of 64 bit seconds do not equal expected";
  EXPECT_EQ(current_fraction, expected_fraction)
      << "Division of 64 bit fractions do not equal expected";
}

INSTANTIATE_TEST_CASE_P(
    testIntegerTimeUtils, Divide64BitBy16BitTestFixture,
    ::testing::Values(
        std::make_tuple(0xFFFF, 0xFFFFFF, 1),
        std::make_tuple(0xFFFF, 0xFFFFFF, 0x100), std::make_tuple(1, 0, 0x100),
        std::make_tuple(0, 0x1000000000000000, 0x100),
        std::make_tuple(
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint64_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 64))),
            static_cast<uint16_t>(std::rand() %
                                  static_cast<int>(std::pow(2, 16))))));

// ***********************************************************************
// Define main

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  std::cout << std::hex;
  return RUN_ALL_TESTS();
}
