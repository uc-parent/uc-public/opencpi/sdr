// Gain implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "gain_s_l-worker.hh"

using namespace OCPI::RCC;
using namespace Gain_s_lWorkerTypes;

class Gain_s_lWorker : public Gain_s_lWorkerBase {
  int32_t gain = 0;
  size_t sent_samples = 0;

  RCCResult start() {
    sent_samples = 0;
    return RCC_OK;
  }

  RCCResult gain_written() {
    gain = static_cast<int32_t>(properties().gain);
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Short_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const int16_t *inData = input.sample().data().data();
      int32_t *outData = output.sample().data().data();

      output.setOpCode(Long_timed_sampleSample_OPERATION);
      size_t max_length = GAIN_S_L_OCPI_MAX_BYTES_OUTPUT / sizeof(*outData);
      inData = &inData[sent_samples];
      if (length - sent_samples <= max_length) {
        size_t unsent_samples = length - sent_samples;
        output.sample().data().resize(unsent_samples);
        for (size_t i = 0; i < unsent_samples; i++) {
          *outData = *inData * gain;
          inData++;
          outData++;
        }
        // All input data has been processed prepare for next input message
        sent_samples = 0;
        return RCC_ADVANCE;
      } else {
        output.sample().data().resize(max_length);
        for (size_t i = 0; i < max_length; i++) {
          *outData = *inData * gain;
          inData++;
          outData++;
        }
        sent_samples = sent_samples + max_length;
        output.advance(max_length);
        return RCC_OK;
      }

    } else if (input.opCode() == Short_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Long_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Long_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Short_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Long_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

GAIN_S_L_START_INFO

GAIN_S_L_END_INFO
