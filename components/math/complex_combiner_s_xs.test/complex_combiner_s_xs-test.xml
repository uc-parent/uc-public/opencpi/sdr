<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <case>
    <input port="input_real" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case typical --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" value="true"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case property --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="true, false"/>
    <property name="subcase" value="input_real_opcode_passthrough"/>
  </case>
  <!-- No variation of input_real_opcase_passthrough as a parameter so cannot be
       changed at runtime. -->
  <case>
    <input port="input_real" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case sample_other_port --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" value="true"/>
    <property name="subcase" values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case sample_other_port" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case sample --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" value="true"/>
    <property name="subcase" values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384"
      stressormode="full"/>
    <input port="input_imaginary" script="generate.py --case input_stressing_other_port --seed 347" messagesinfile="true"
      messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" value="true"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case input_stressing_other_port" messagesinfile="true" messagesize="16384"
      stressormode="full"/>
    <input port="input_imaginary" script="generate.py --case input_stressing --seed 347" messagesinfile="true" messagesize="16384"
      stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" value="true"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case message_size --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- For non-sample opcode testing run with instep_opcode_passthrough set to
       true and false to ensure different opcode handling behaviour is
       covered. -->
  <case>
    <input port="input_real" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case time_other_port --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case time_other_port" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case time --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case sample_interval_other_port --seed 347" messagesinfile="true"
      messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case sample_interval_other_port" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case sample_interval --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case flush_other_port --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case flush_other_port" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case flush --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case discontinuity_other_port --seed 347" messagesinfile="true"
      messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case discontinuity_other_port" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case discontinuity --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case metadata_other_port --seed 347" messagesinfile="true"
      messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case metadata_other_port" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case metadata --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input_real" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <input port="input_imaginary" script="generate.py --case soak --seed 347" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="input_real_opcode_passthrough" values="false,true"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
