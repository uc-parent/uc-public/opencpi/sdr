#!/usr/bin/env python3

# Runs checks for fifo testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Fifo Message OCPI Verification script."""

import os
import sys

from fifo_message import FifoMessage

import opencpi.ocpi_testing as ocpi_testing

data_width = int(os.environ["OCPI_TEST_data_width"])
input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

case = str(os.environ.get("OCPI_TESTCASE"))[-2:]
subcase = str(os.environ.get("OCPI_TESTSUBCASE"))

fifo_message_implementation = FifoMessage()

test_id = ocpi_testing.get_test_case()

if data_width == 8:
    protocol = "char_timed_sample"
elif data_width == 16:
    protocol = "short_timed_sample"
elif data_width == 32:
    protocol = "long_timed_sample"
elif data_width == 64:
    protocol = "longlong_timed_sample"

verifier = ocpi_testing.Verifier(fifo_message_implementation)
verifier.set_port_types([protocol], [protocol], ["equal"])

if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)
