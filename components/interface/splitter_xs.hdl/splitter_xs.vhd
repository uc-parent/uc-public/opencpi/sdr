-- HDL Implementation of splitter_xs.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  signal output_ready_1 : std_logic;
  signal output_ready_2 : std_logic;
  signal output_ready_3 : std_logic;
  signal output_ready_4 : std_logic;
  signal take           : std_logic;

begin

  -- Enable outputs when either they are ready or not connected
  output_ready_1 <= output_1_in.ready;
  output_ready_2 <= output_2_in.ready or output_2_in.reset;
  output_ready_3 <= output_3_in.ready or output_3_in.reset;
  output_ready_4 <= output_4_in.ready or output_4_in.reset;

  -- Take input data whenever all outputs are ready
  take           <= output_ready_1 and output_ready_2 and output_ready_3 and output_ready_4;
  input_out.take <= take;

  -- Output streaming interface
  output_1_out.give        <= input_in.ready and take;
  output_1_out.valid       <= input_in.valid and take;
  output_1_out.eom         <= input_in.eom;
  output_1_out.data        <= input_in.data;
  output_1_out.opcode      <= input_in.opcode;
  output_1_out.byte_enable <= input_in.byte_enable;

  output_2_out.give        <= input_in.ready and take;
  output_2_out.valid       <= input_in.valid and take;
  output_2_out.eom         <= input_in.eom;
  output_2_out.data        <= input_in.data;
  output_2_out.opcode      <= input_in.opcode;
  output_2_out.byte_enable <= input_in.byte_enable;

  output_3_out.give        <= input_in.ready and take;
  output_3_out.valid       <= input_in.valid and take;
  output_3_out.eom         <= input_in.eom;
  output_3_out.data        <= input_in.data;
  output_3_out.opcode      <= input_in.opcode;
  output_3_out.byte_enable <= input_in.byte_enable;

  output_4_out.give        <= input_in.ready and take;
  output_4_out.valid       <= input_in.valid and take;
  output_4_out.eom         <= input_in.eom;
  output_4_out.data        <= input_in.data;
  output_4_out.opcode      <= input_in.opcode;
  output_4_out.byte_enable <= input_in.byte_enable;

end rtl;
