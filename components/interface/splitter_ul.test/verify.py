#!/usr/bin/env python3

# Runs checks for splitter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Runs checks for splitter testing."""

import sys

import opencpi.ocpi_testing as ocpi_testing

from splitter import Splitter

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

splitter_implementation = Splitter()

test_id = ocpi_testing.get_test_case()

# As the file name includes the port number this can be used to recover the
# port number
port_index = int(output_file_path[-5])

verifier = ocpi_testing.Verifier(splitter_implementation)
verifier.set_port_types(
    ["ulong_timed_sample"],
    ["ulong_timed_sample", "ulong_timed_sample", "ulong_timed_sample",
     "ulong_timed_sample"],
    ["equal", "equal", "equal", "equal"])
if verifier.verify(test_id, [input_file_path], output_file_path,
                   port_select=port_index) is True:
    sys.exit(0)
else:
    sys.exit(1)
