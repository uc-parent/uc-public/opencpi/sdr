-- HDL Implementation of FIFO Message.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library sdr_util;
use sdr_util.sdr_util.fifo_instant_read;

architecture rtl of worker is

  function data_buffer_width_f(store_all_controls : std_logic; data_width : natural; byte_enable_width : natural) return natural is
    variable result : natural;
  begin
    if its(store_all_controls) then
      result := data_width + byte_enable_width;
    else
      result := data_width;
    end if;
    return result;
  end function;

  constant ushort_max_c         : natural   := 65535;
  constant data_width_c         : natural   := to_integer(data_width);  -- Input and output data width in bits
  constant store_all_controls_c : std_logic := store_all_controls;
  constant data_buffer_width_c  : natural   := data_buffer_width_f(store_all_controls_c, data_width_c, input_in.byte_enable'length);
  -- Maximum number of data_width samples the FIFO can hold
  -- (maximum positive VHDL integer)
  constant data_depth_c         : natural   := to_integer(data_depth(data_depth'high-1 downto 0));
  constant data_length_width_c  : natural   := integer(ceil(log2(real(data_depth_c+1))));
  constant message_depth_c      : natural   := to_integer(message_depth);  -- Maximum number of messages the FIFO can hold
  -- Register FIFO input, output, mux of multiple BRAMs and final register for
  -- component output primitive output
  constant fifo_pipeline_c      : natural   := 4;

  -- FIFO Write
  signal length_write                       : unsigned(data_length_width_c-1 downto 0);
  signal length_write_prev                  : unsigned(length_write'range);
  constant fifo_index_byte_enable_high_c    : natural := 2+length_write'high+input_in.opcode'high+input_in.byte_enable'high;
  constant fifo_index_byte_enable_low_c     : natural := 2+length_write'high+input_in.opcode'high;
  constant fifo_index_opcode_high_c         : natural := 1+length_write'high+input_in.opcode'high;
  constant fifo_index_opcode_low_c          : natural := 1+length_write'high;
  constant fifo_index_message_length_high_c : natural := 0+length_write'high;
  constant fifo_index_message_length_low_c  : natural := 0;
  signal data_buffer_full                   : std_logic;
  signal zero_length_message                : std_logic;
  signal data_buffer_write                  : std_logic;
  signal data_buffer_write_data             : std_logic_vector(data_buffer_width_c-1 downto 0);
  signal message_buffer_write               : std_logic;
  signal message_buffer_write_data          : std_logic_vector(fifo_index_byte_enable_high_c downto fifo_index_message_length_low_c);

  -- FIFO Read
  signal busy                            : std_logic;
  signal message_buffer_full             : std_logic;
  signal message_buffer_empty            : std_logic;
  signal message_buffer_read             : std_logic;
  signal message_buffer_read_byte_enable : std_logic_vector(input_in.byte_enable'range);
  signal message_buffer_read_opcode      : std_logic_vector(input_in.opcode'range);
  signal message_buffer_read_length      : unsigned(data_length_width_c-1 downto 0);
  signal message_buffer_read_data        : std_logic_vector(message_buffer_write_data'range);
  signal data_buffer_empty               : std_logic;
  signal data_buffer_read                : std_logic;
  signal data_buffer_read_data           : std_logic_vector(data_buffer_width_c-1 downto 0);
  signal length_read                     : unsigned(data_length_width_c-1 downto 0);
  signal cache_read_byte_enable          : std_logic_vector(input_in.byte_enable'range);
  signal cache_read_opcode               : std_logic_vector(input_in.opcode'range);
  signal cache_read_length               : unsigned(data_length_width_c-1 downto 0);

  -- FIFO Status
  signal current_data_fill        : natural;
  signal full_data_count          : natural;
  signal full_data_count_clear    : std_logic;
  signal current_message_fill     : natural;
  signal full_message_count       : natural;
  signal full_message_count_clear : std_logic;
  signal empty_count              : natural;
  signal empty_count_clear        : std_logic;
  signal both_buffer_empty        : std_logic;
  signal both_buffer_empty_r      : std_logic;

begin
  -----------------------------------------------------------------------------
  -- Input / FIFO write control
  -----------------------------------------------------------------------------
  input_out.take <= data_buffer_write or message_buffer_write;

  zero_length_message  <= input_in.ready and input_in.som and not(input_in.valid) and input_in.eom;
  data_buffer_write    <= input_in.valid and ((input_in.eom and not(message_buffer_full)) or not(input_in.eom)) and not(data_buffer_full);
  message_buffer_write <= input_in.ready and ((input_in.eom and not(data_buffer_full)) or zero_length_message) and not(message_buffer_full);

  -- Concatenate signals into the Message FIFO
  message_buffer_write_data(fifo_index_byte_enable_high_c downto fifo_index_byte_enable_low_c)       <= input_in.byte_enable;
  message_buffer_write_data(fifo_index_opcode_high_c downto fifo_index_opcode_low_c)                 <= input_in.opcode;
  message_buffer_write_data(fifo_index_message_length_high_c downto fifo_index_message_length_low_c) <= std_logic_vector(length_write);
  -----------------------------------------------------------------------------
  -- Count length of message written to Data FIFO
  -----------------------------------------------------------------------------
  length_write <=
    to_unsigned(0, length_write'length) when ((input_in.som = '1') and (input_in.valid = '0')) else  -- ZLM
    to_unsigned(1, length_write'length) when ((input_in.som = '1') and (input_in.valid = '1')) else  -- Start of valid data
    length_write_prev + 1               when (input_in.valid = '1')                            else  -- End of valid data
    length_write_prev;

  length_write_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        length_write_prev <= (others => '0');
      else
        if (data_buffer_write = '1') then
          length_write_prev <= length_write;
        end if;
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- Data buffer
  -----------------------------------------------------------------------------
  full_data_count_clear       <= props_in.full_data_count_written;
  props_out.current_data_fill <= '0' & to_unsigned(current_data_fill, props_out.current_data_fill'length-1);  -- As close to ulong that VHDL integer supports
  props_out.full_data_count   <= to_unsigned(full_data_count, props_out.full_data_count'length);

  data_buffer_all_gen : if (store_all_controls_c = '1') generate
    data_buffer_write_data <= input_in.byte_enable & input_in.data;
  end generate;

  data_buffer_not_all_gen : if (store_all_controls_c = '0') generate
    data_buffer_write_data <= input_in.data;
  end generate;

  data_buffer_i : fifo_instant_read
    generic map (
      data_width_g    => data_buffer_width_c,
      buffer_depth_g  => data_depth_c,
      read_pipeline_g => fifo_pipeline_c
      )
    port map (
      clk               => ctl_in.clk,
      reset             => ctl_in.reset,
      -- Read/Write
      write_enable      => data_buffer_write,
      write_data        => data_buffer_write_data,
      read_enable       => data_buffer_read,
      read_valid        => open,
      read_data         => data_buffer_read_data,
      -- Status
      fill_level        => current_data_fill,
      full_count_clear  => full_data_count_clear,
      full_count        => full_data_count,
      full              => data_buffer_full,
      empty_count_clear => '0',
      empty_count       => open,
      empty             => data_buffer_empty
      );
  -----------------------------------------------------------------------------
  -- Message buffer
  -----------------------------------------------------------------------------
  full_message_count_clear       <= props_in.full_message_count_written;
  empty_count_clear              <= props_in.empty_count_written;
  props_out.current_message_fill <= to_unsigned(current_message_fill, props_out.current_message_fill'length);
  props_out.full_message_count   <= to_unsigned(full_message_count, props_out.full_message_count'length);
  props_out.empty_count          <= to_unsigned(empty_count, props_out.empty_count'length);

  message_buffer_i : fifo_instant_read
    generic map (
      data_width_g    => message_buffer_write_data'length,
      buffer_depth_g  => message_depth_c,
      read_pipeline_g => fifo_pipeline_c
      )
    port map (
      clk               => ctl_in.clk,
      reset             => ctl_in.reset,
      -- Read/Write
      write_enable      => message_buffer_write,
      write_data        => message_buffer_write_data,
      read_enable       => message_buffer_read,
      read_valid        => open,
      read_data         => message_buffer_read_data,
      -- Status
      fill_level        => current_message_fill,
      full_count_clear  => full_message_count_clear,
      full_count        => full_message_count,
      full              => message_buffer_full,
      empty_count_clear => empty_count_clear,
      empty_count       => open,
      empty             => message_buffer_empty
      );
  -----------------------------------------------------------------------------
  -- Count times both buffers are empty at the same time
  -----------------------------------------------------------------------------
  both_buffer_empty <= message_buffer_empty and data_buffer_empty;
  empty_count_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        both_buffer_empty_r <= '1';
        empty_count         <= 0;
      else
        both_buffer_empty_r <= both_buffer_empty;
        if (empty_count_clear = '1') then
          empty_count <= 0;
        elsif ((both_buffer_empty_r = '0') and (both_buffer_empty = '1')) then
          -- Enter empty
          if (empty_count < ushort_max_c) then
            empty_count <= empty_count + 1;
          end if;
        end if;
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- Count length of message read from Data FIFO
  -----------------------------------------------------------------------------
  length_read_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        length_read <= (others => '0');
      else
        if ((message_buffer_read = '1') and (message_buffer_read_length > 1)) then
          length_read <= to_unsigned(2, length_read'length);
        elsif (data_buffer_read = '1') then
          length_read <= length_read + 1;
        end if;
      end if;
    end if;
  end process;
  -----------------------------------------------------------------------------
  -- Indicate busy when reading from Data FIFO
  -- Cache information read from Message FIFO for use once all data has been
  -- read
  -----------------------------------------------------------------------------
  busy_read_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        busy                   <= '0';
        cache_read_byte_enable <= (others => '0');
        cache_read_opcode      <= (others => '0');
        cache_read_length      <= (others => '0');
      else
        if ((message_buffer_read = '1') and (message_buffer_read_length > 1)) then
          busy                   <= '1';
          cache_read_byte_enable <= message_buffer_read_byte_enable;
          cache_read_opcode      <= message_buffer_read_opcode;
          cache_read_length      <= message_buffer_read_length;
        elsif ((output_in.ready = '1') and (length_read = cache_read_length)) then
          busy <= '0';
        end if;
      end if;
    end if;
  end process;

  data_buffer_read <= '1' when
                      ((message_buffer_read = '1') and (busy = '0') and (message_buffer_read_length > 0)) or  -- Message indicates data available (read 1st word)
                      ((output_in.ready = '1') and (busy = '1')) else '0';  -- Read remaining words of the data

  message_buffer_read <= output_in.ready and not(message_buffer_empty) and not(busy);

  -- Slice signals out of the Message FIFO
  message_buffer_read_byte_enable <= message_buffer_read_data(fifo_index_byte_enable_high_c downto fifo_index_byte_enable_low_c);
  message_buffer_read_opcode      <= message_buffer_read_data(fifo_index_opcode_high_c downto fifo_index_opcode_low_c);
  message_buffer_read_length      <= unsigned(message_buffer_read_data(fifo_index_message_length_high_c downto fifo_index_message_length_low_c));

  -----------------------------------------------------------------------------
  -- Output
  -----------------------------------------------------------------------------
  output_out.som <= message_buffer_read;
  output_out.eom <= '1' when ((message_buffer_read = '1') and (message_buffer_read_length <= 1)) else
                    '1' when ((data_buffer_read = '1') and (busy = '1') and (length_read = cache_read_length)) else
                    '0';
  output_out.eof   <= input_in.eof when ((current_message_fill = 0) and (current_data_fill = 0))  else '0';
  output_out.valid <= data_buffer_read;
  output_out.give  <= '1'          when ((message_buffer_read = '1') or (data_buffer_read = '1')) else '0';

  byte_enable_all_gen : if (store_all_controls_c = '1') generate
    output_out.byte_enable <=
      data_buffer_read_data(data_buffer_read_data'high downto data_width_c) when (data_buffer_read = '1') else
      (others => '0');
  end generate;

  byte_enable_not_all_gen : if (store_all_controls_c = '0') generate
    output_out.byte_enable <=
      message_buffer_read_byte_enable                    when ((message_buffer_read = '1') and (message_buffer_read_length <= 1)) else
      cache_read_byte_enable                             when ((data_buffer_read = '1') and (busy = '1') and (length_read = cache_read_length)) else
      (output_out.byte_enable'range => data_buffer_read) when ((message_buffer_read = '1') or (busy = '1')) else
      (others                       => '0');
  end generate;

  output_out.opcode <=
    message_buffer_read_opcode when (message_buffer_read = '1') else
    cache_read_opcode          when ((data_buffer_read = '1') and (busy = '1')) else
    (others => '0');
  output_out.data <= data_buffer_read_data(data_width_c-1 downto 0) when (data_buffer_read = '1') else (others => '0');
-----------------------------------------------------------------------------
end rtl;
