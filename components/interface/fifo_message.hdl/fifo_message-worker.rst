.. fifo_message HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _fifo_message-hdl-worker:


``fifo_message`` HDL Worker
===========================

Detail
------
The typical operation is described below for the ``store_all_controls`` property set to ``false``:

The implementation of the HDL worker consists of a data buffer FIFO and a message buffer FIFO. Providing that the buffers are not full, any valid data for the message is written into the data buffer before writing a metadata word into the message buffer. If the incoming message is a zero length message then the metadata is written immediately. The metadata word consists of the message opcode, byte enables of the last data word and the data length. If either of the buffers are full, back pressure will be applied to pause incoming messages until messages have been extracted out of the FIFO and the buffers are no longer full.

When the message buffer becomes not empty and no back pressure is applied on the output port, a single metadata word is read from it, and if the metadata length indicates it's a zero length message it is sent straight to the output. If the metadata length indicates data is also present then, providing back pressure is not applied on the output port, it is read from the data buffer and fed to the output. All data bytes are fully data enabled until the last sample where the metadata byte_enable is used to validate the output data. The output opcode will be provided by the metadata opcode and the ``SOM``, ``EOM``, ``Valid`` and ``Give`` outputs will be generated. The ``EOF`` output will be de-asserted until both buffers are empty and will then follow the state of the ``EOF`` input. The process is then repeated until the message buffer and data buffer are both empty.

The fill levels and counts together with their clear inputs are connected directly to their respective data or message buffers. The empty signals from each of the buffers are monitored and the empty count is incremented each time both buffers are simultaneously empty, with a write to the ``empty_count`` property resetting the count.

If the ``store_all_controls`` property is set to ``true``:

The operation is as described above but each ``byte_enable`` input word is stored together with each data word in the data buffer and, once extracted from the data buffer, is output directly rather than asserting all byte enables until the last one.

.. ocpi_documentation_worker::

   store_all_controls: When set to `true`, the component will store all input control signals. This will require increased resources.

Limitations
-----------
Although the worker can simultaneously consume and produce samples on its input and output ports, respectively, a full message (an EOM) has to have been received before any samples can be produced on the output. Therefore, ``data_depth`` must be set large enough to accept a maximum size input message.

Utilisation
-----------
.. ocpi_documentation_utilization::
