-- HDL implementation of message_length_adjuster
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_util;
use sdr_util.sdr_util.opcode_t;
use sdr_util.sdr_util.opcode_to_slv;
library sdr_interface;
use sdr_interface.sdr_interface.timeout_monitor;

architecture rtl of worker is
  constant data_width_c   : integer := input_in.data'length;
  constant bytes_c        : integer := data_width_c/8;
  constant byte_shift_c   : integer := integer(ceil(log2(real(bytes_c))));
  constant opcode_width_c : integer := input_in.opcode'length;

  signal timeout                  : std_logic;
  signal timeout_r                : std_logic;
  signal take                     : std_logic;
  signal backpressure             : std_logic;
  signal backpressure_r           : std_logic;
  signal backpressure_eom         : std_logic                                                      := '0';
  signal backpressure_eom_r       : std_logic                                                      := '0';
  signal passthrough_eom          : std_logic                                                      := '0';
  signal sample_data_valid        : std_logic;
  signal non_sample_data_valid    : std_logic;
  signal sample_opcode_transition : std_logic;
  signal insert_eom               : std_logic;
  signal message_size             : unsigned(props_in.message_size'length+byte_shift_c-1 downto 0);
  signal message_size_count       : unsigned(props_in.message_size'length+byte_shift_c-1 downto 0) := (others => '0');
  signal message_size_bytes       : unsigned(props_in.message_size'length+byte_shift_c-1 downto 0);

  signal input_som   : std_logic;
  signal input_eom   : std_logic;
  signal input_ready : std_logic;
  signal input_valid : std_logic;
begin

  message_size_bytes <= resize(shift_left(props_in.message_size, byte_shift_c), message_size_bytes'length);

  -- handles the setting of the message size property
  message_size_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if message_size_bytes > props_in.ocpi_buffer_size_output then
        message_size <= resize(props_in.ocpi_buffer_size_output, message_size'length);
      else
        message_size <= message_size_bytes;
      end if;
    end if;
  end process;

  take <= input_in.ready and output_in.ready and not backpressure;

  input_out.take <= take;

  sample_data_valid <= '1' when input_in.valid = '1' and input_in.opcode = opcode_to_slv(sample_op_e, opcode_width_c) else '0';

  non_sample_data_valid <= '1' when input_in.ready = '1' and input_in.opcode /= opcode_to_slv(sample_op_e, opcode_width_c) else '0';

  backpressure <= sample_opcode_transition;


  flag_register_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if backpressure = '1' then
        backpressure_r <= '1';
      end if;
      if timeout = '1' or ctl_in.reset = '1' then
        timeout_r <= '1';
      end if;
      if output_in.ready = '1' then
        backpressure_r <= '0';
        timeout_r      <= '0';
      end if;
    end if;
  end process;

  -- When a sample is taken increment a counter to track the number of samples
  -- in a message. If a non-sample is received or a timeout occurs, the
  -- counter is cleared.
  count_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if output_in.ready = '1' then
        if backpressure = '1' or backpressure_r = '1' or timeout = '1' or timeout_r = '1' then
          message_size_count <= (others => '0');
        elsif sample_data_valid = '1' then
          if message_size_count >= message_size - bytes_c then
            message_size_count <= (others => '0');
          else
            message_size_count <= message_size_count + bytes_c;
          end if;
        end if;
      end if;
    end if;
  end process;

  -- Detect sample to non-sample opcode transition
  sample_opcode_transition <= '1' when message_size_count > 0 and non_sample_data_valid = '1' else '0';

  -- When the maximum message size is reached, insert an eom
  insert_eom <= '1' when message_size_count >= message_size - bytes_c and sample_data_valid = '1' else
                '0';

  input_som <= '0';

  input_eom   <= input_in.eom   when non_sample_data_valid = '1' else insert_eom;
  input_ready <= input_in.ready when backpressure = '0' else '1';
  input_valid <= input_in.valid when backpressure = '0'          else '0';

  -- check timeout between sample messages
  timeout_i : timeout_monitor
    generic map (
      sample_opcode_g     => opcode_to_slv(sample_op_e, opcode_width_c),
      data_width_g        => data_width_c,
      time_width_g        => time_in.fraction'length,
      opcode_width_g      => opcode_width_c,
      byte_enable_width_g => input_in.byte_enable'length
      )
    port map (
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => output_in.ready,
      take_in            => take,
      timeout_in         => props_in.timeout,
      time_valid         => time_in.valid,
      time_fraction      => time_in.fraction,
      input_som          => input_som,
      input_eom          => input_eom,
      input_eof          => input_in.eof,
      input_valid        => input_valid,
      input_ready        => input_ready,
      input_byte_enable  => input_in.byte_enable,
      input_opcode       => input_in.opcode,
      input_data         => input_in.data,
      timeout_out        => timeout,
      -- Output interface
      output_som         => output_out.som,
      output_eom         => passthrough_eom,
      output_eof         => output_out.eof,
      output_valid       => output_out.valid,
      output_give        => output_out.give,
      output_byte_enable => output_out.byte_enable,
      output_opcode      => output_out.opcode,
      output_data        => output_out.data
      );

  -- insert EOM on final sample after opcode transition
  backpressure_eom_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      -- double registered to keep in line with timeout_monitor latency
      if backpressure = '1' then
        backpressure_eom <= '1';
      end if;
      if take = '1' then
        backpressure_eom   <= '0';
        backpressure_eom_r <= backpressure_eom;
      end if;
    end if;
  end process;

  output_out.eom <= backpressure_eom_r or passthrough_eom;

end rtl;
