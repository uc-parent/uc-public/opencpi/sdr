#!/usr/bin/env python3

# Runs checks for register_l testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Runs checks for register_l testing."""

import sys

import opencpi.ocpi_testing as ocpi_testing
from opencpi.ocpi_testing.ocpi_testing.verifier import BaseComparison


class InSetComparison(BaseComparison):
    """Custom comparison, which will just check the opcodes."""

    def variable_summary(self):
        """Returns summary of the variables that control the comparison method.

        Returns:
            An empty dictionary.
        """
        return {}

    def same(self, reference, implementation):
        """Checks that the dataset is the right opcode.

        Args:
            reference (list): List of reference messages. A message is a
                dictionary with an "opcode" and "data" key word, which have
                values of the opcode name and the data value for that message.
                Reference messages are typically from running a (Python)
                reference implementation against the input data.
            implementation (list): List of the messages to be checked. A
                message is a dictionary with an "opcode" and "data" key word,
                which have values of the opcode name and the data value for
                that message. Typically from the implementation-under-test
                being provided with the input test data.

        Returns:
             Bool, str. A boolean to indicate if the test passed (True) or
                failed (False). String that is the reason for any failure, in
                the case of tests which pass returns an empty string.
        """
        for reference_message in reference:
            if reference_message["opcode"] != "sample":
                continue
            for index, implementation_message in enumerate(implementation):
                if implementation_message["opcode"] != "sample":
                    return False, (f"Message {index} (zero indexed) is of an"
                                   + " unexpected opcode")
                # We cannot check the actual data, as the position within
                # the stream is unknown.
        # Checks pass, no failure message
        return True, ""


class Register(ocpi_testing.Implementation):
    """Reference register implementation."""

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Handle incoming Sample opcode.

        Args:
            values (list): Incoming sample values.

        Returns:
            Formatted output opcode messages.
        """
        return self.output_formatter(
            [{"opcode": "sample", "data": values}])


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

register_implementation = Register()

test_id = ocpi_testing.get_test_case()

# Use a custom verifier, which will just check the opcodes.
ocpi_testing.ocpi_testing.verifier.COMPARISON_METHODS["set"] = InSetComparison
verifier = ocpi_testing.Verifier(register_implementation)
verifier.set_port_types(
    ["long_timed_sample"], ["long_timed_sample"], ["set"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
