// RCC implementation of fifo_message worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <queue>
#include <utility>

#include "../../util/common/opcodes.hh"
#include "fifo_message-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fifo_messageWorkerTypes;

class Fifo_messageWorker : public Fifo_messageWorkerBase {
  // Fifo/Queue holding opcode,data pairs.
  using fifo_data_t = std::vector<uint8_t>;
  using fifo_element_t = std::pair<uint8_t, fifo_data_t>;
  std::queue<fifo_element_t> fifo;
  // Fifo statistics.
  uint32_t fifo_data_size{0};
  uint32_t empty_count{0};
  uint32_t full_data_count{0};
  uint32_t full_message_count{0};

  // Number of bytes needed to unblock input.
  uint32_t fifo_data_size_needed{0};

  // Data depth converted to bytes.
  uint32_t data_depth_bytes{0};

  // Normal FIFO running: either input or output to run.
  const RunCondition run_condition_normal{
      1 << FIFO_MESSAGE_INPUT, 1 << FIFO_MESSAGE_OUTPUT, RCC_NO_PORTS};
  // FIFO empty: only run if input available.
  const RunCondition run_condition_empty{1 << FIFO_MESSAGE_INPUT, RCC_NO_PORTS};
  // FIFO full: only run if output available.
  const RunCondition run_condition_full{1 << FIFO_MESSAGE_OUTPUT, RCC_NO_PORTS};

  RCCResult current_data_fill_read() {
    properties().current_data_fill = this->fifo_data_size;
    return RCC_OK;
  }

  RCCResult current_message_fill_read() {
    properties().current_message_fill = this->fifo.size();
    return RCC_OK;
  }

  RCCResult empty_count_read() {
    properties().empty_count = this->empty_count;
    return RCC_OK;
  }

  RCCResult full_data_count_read() {
    properties().full_data_count = this->full_data_count;
    return RCC_OK;
  }

  RCCResult full_message_count_read() {
    properties().full_message_count = this->full_message_count;
    return RCC_OK;
  }

  RCCResult empty_count_written() {
    this->empty_count = 0;
    return RCC_OK;
  }

  RCCResult full_data_count_written() {
    this->full_data_count = 0;
    return RCC_OK;
  }

  RCCResult full_message_count_written() {
    this->full_message_count = 0;
    return RCC_OK;
  }

  RCCResult start() {
    // Start with empty FIFO: run when input available.
    setRunCondition(&this->run_condition_empty);
    // Property is in samples - convert to bytes for internal use.
    this->data_depth_bytes =
        properties().data_depth * properties().data_width / 8;
    // There is no ocpi_buffer_size_input, but as this requires
    // input size == output size to maintain message boundaries
    // the ocpi_buffer_size_output will do.
    if (this->data_depth_bytes < properties().ocpi_buffer_size_output) {
      setError("data_depth must be at least input buffer size");
      return RCC_FATAL;
    }
    return RCC_OK;
  }

  RCCResult run(bool) {
    bool have_input_buffer{input.hasBuffer()};
    bool have_output_buffer{output.hasBuffer()};

    if (input.eof()) {
      if (this->fifo.empty()) {
        output.setEOF();
        return RCC_ADVANCE_FINISHED;
      }
      this->pop_fifo_to_output();
    } else if (have_input_buffer && have_output_buffer) {
      if (this->fifo.empty()) {
        return forward_opcode(input, output);
      }
      // Pop before push in case FIFO is nearing full.
      this->pop_fifo_to_output();
      this->push_input_to_fifo();
    } else if (have_input_buffer) {
      // Input only, store in FIFO.
      this->push_input_to_fifo();
    } else if (!this->fifo.empty()) {
      // The RunConditions mean if run() is called and there is no input
      // buffer, there must be an output buffer
      // Output only, take from FIFO.
      this->pop_fifo_to_output();
      if (this->fifo.empty()) {
        setRunCondition(&this->run_condition_empty);
        ++this->empty_count;
      }
    }

    return RCC_OK;
  }

  void push_input_to_fifo() {
    // Check for data room first. The data full can only be triggered
    // after receipt of a non-zero-length buffer that would overflow
    // the fifo data length.
    if (this->fifo_data_size + input.length() > this->data_depth_bytes) {
      // No data room in fifo, cannot accept this message, only run
      // again when output buffer available to free up room.
      setRunCondition(&this->run_condition_full);
      this->fifo_data_size_needed = input.length();
      ++this->full_data_count;
      return;
    }

    this->fifo.push(fifo_element_t(
        input.opCode(),
        fifo_data_t(input.data(), input.data() + input.length())));
    this->fifo_data_size += input.length();
    input.advance();

    // Check for message full. This can be done after push
    // as no further messages can be stored.
    if (this->fifo.size() >= properties().message_depth) {
      // No message room in fifo: Only run if output buffer available.
      ++this->full_message_count;
      setRunCondition(&this->run_condition_full);
    }
  }

  void pop_fifo_to_output() {
    uint32_t data_size = this->fifo.front().second.size();
    output.setLength(data_size);
    output.setOpCode(this->fifo.front().first);
    if (data_size > 0) {
      std::move(this->fifo.front().second.begin(),
                this->fifo.front().second.end(), output.data());
      this->fifo_data_size -= data_size;
      if (this->fifo_data_size_needed > 0 &&
          data_size >= this->fifo_data_size_needed) {
        // Space for next input message.
        setRunCondition(&this->run_condition_normal);
        this->fifo_data_size_needed = 0;
      } else {
        this->fifo_data_size_needed -= data_size;
      }
    }
    this->fifo.pop();
    output.advance();
  }
};

FIFO_MESSAGE_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in
// any case
FIFO_MESSAGE_END_INFO
