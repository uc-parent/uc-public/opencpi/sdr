-- register_b HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator_v2;

architecture rtl of worker is

  constant delay_c        : integer := 1;
  constant opcode_width_c : integer := 3;

  signal take        : std_logic;
  signal valid_data  : std_logic;
  signal data_reg    : std_logic_vector(input_in.data'range);
  signal output_data : std_logic_vector(output_out.data'range);

begin

  -- Take input data whenever it is available
  input_out.take <= '1';

  valid_data <= '1' when input_in.valid = '1' and
                input_in.opcode = bool_timed_sample_sample_op_e else '0';

  -- Register sample data as it becomes available
  register_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        data_reg <= (others => '0');
      elsif valid_data = '1' then
        data_reg <= input_in.data;
      end if;
    end if;
  end process;

  -- instantiate generator
  interface_generator_i : protocol_interface_generator_v2
    generic map (
      delay_g                 => delay_c,
      data_width_g            => output_out.data'length,
      opcode_width_g          => opcode_width_c,
      byte_enable_width_g     => output_out.byte_enable'length,
      processed_data_opcode_g => "000",
      discontinuity_opcode_g  => "100"
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_in.ready,
      -- High when discontinuity event occurs
      discontinuity_trigger => '0',
      generator_enable      => '1',
      generator_reset       => '0',
      -- Connect output from data processing module
      processed_stream_in   => data_reg,
      -- Specifies the length that the output message should be
      message_length        => props_in.message_length,
      -- High when processing module should be disabled.
      output_hold           => open,
      -- Output interface signals
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => open,
      output_data           => output_data
      );

  output_out.opcode <= bool_timed_sample_sample_op_e;
  output_out.data   <= output_data;

end rtl;
