// RCC implementation of message_length_adjuster worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include <cstdint>

#include "message_length_adjuster-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Message_length_adjusterWorkerTypes;

class Message_length_adjusterWorker : public Message_length_adjusterWorkerBase {
  static constexpr size_t SAMPLE_OPCODE = 0;

  size_t max_bytes = MESSAGE_LENGTH_ADJUSTER_OCPI_MAX_BYTES_OUTPUT;
  uint32_t timeout = 0;
  uint8_t width_in_bytes = 1;

  RunCondition run_condition;
  size_t input_index = 0;   // Number of bytes processed from input buffer
  size_t output_bytes = 0;  // Number of bytes placed into output buffer

  RCCResult start() {
    // Check the width property
    if (properties().width == 0) {
      setError("width must be greater than 0");
      return RCC_FATAL;
    }
    if (properties().width % 8 != 0) {
      setError("width must be a multiple of 8.");
      return RCC_FATAL;
    }
    this->width_in_bytes = properties().width / 8;
    // Update message size as required
    return this->message_size_written();
  }

  RCCResult message_size_written() {
    this->max_bytes = properties().message_size * this->width_in_bytes;
    if (this->max_bytes == 0) {
      setError("message_size must be greater than 0");
      return RCC_FATAL;
    }
    if (this->max_bytes > properties().ocpi_buffer_size_output) {
      this->max_bytes = properties().ocpi_buffer_size_output;
    }
    output.setDefaultLength(this->max_bytes);
    return RCC_OK;
  }

  RCCResult timeout_written() {
    this->timeout = properties().timeout;
    if (this->timeout == 0) {
      this->run_condition.disableTimeout();
    } else {
      // Convert 32-bit binary fraction of a second to microseconds
      uint64_t timeout_microseconds =
          ((static_cast<uint64_t>(this->timeout) * 1000000) >> 32);
      // Ensure that timeout is not set to zero. Very small values may
      // result in the application looping on timeout processing.
      if (timeout_microseconds < 1) {
        this->run_condition.setTimeout(1);
      } else {
        this->run_condition.setTimeout(timeout_microseconds);
      }
      this->run_condition.enableTimeout();
    }
    setRunCondition(&run_condition);
    return RCC_OK;
  }

  RCCResult run(bool timedout) {
    if (input.eof()) {
      // First send out any buffered output samples
      if (this->output_bytes > 0) {
        this->output_bytes = 0;
        output.advance();
        return RCC_OK;
      }
      output.setEOF();
      return RCC_ADVANCE_FINISHED;
    }

    if (timedout) {
      // Send any buffered output samples after a timeout
      if (this->output_bytes > 0) {
        this->output_bytes = 0;
        output.advance();
      }
      return RCC_OK;
    }

    // Deal with sample opcodes
    if (input.opCode() == SAMPLE_OPCODE) {
      output.setOpCode(input.opCode());
      size_t free_output_bytes = this->max_bytes - this->output_bytes;
      size_t input_bytes = input.length() - this->input_index;
      if (free_output_bytes >= input_bytes) {
        size_t output_buffer_size = this->output_bytes + input_bytes;
        output.setLength(output_buffer_size);
        std::copy_n(input.data() + this->input_index, input_bytes,
                    output.data() + this->output_bytes);
        this->input_index = 0;
        if (output_buffer_size == this->max_bytes) {
          this->output_bytes = 0;
          return RCC_ADVANCE;
        } else {
          this->output_bytes = output_buffer_size;
          input.advance();
          return RCC_OK;
        }
      } else {
        output.setLength(this->max_bytes);
        std::copy_n(input.data() + this->input_index, free_output_bytes,
                    output.data() + this->output_bytes);
        // Not enough space in output buffer so advance it now
        this->input_index += free_output_bytes;
        this->output_bytes = 0;
        output.advance();
        return RCC_OK;
      }
    }

    // Advance any buffered output data before non-sample opcodes
    if (this->output_bytes > 0) {
      output.setLength(this->output_bytes);
      this->output_bytes = 0;
      output.advance();
      return RCC_OK;
    }

    // Deal with non-sample opcodes
    output.setLength(input.length());
    output.setOpCode(input.opCode());
    // Note this last call is a move, and so will invalidate the input buffer,
    // This is ok as long as no additional actions are then taken.
    std::move(input.data(), input.data() + input.length(), output.data());
    return RCC_ADVANCE;
  }
};

MESSAGE_LENGTH_ADJUSTER_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
MESSAGE_LENGTH_ADJUSTER_END_INFO
