#!/usr/bin/env python3

# Runs checks for timestamp inserter testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Timestamp Inserter OCPI Verification script."""

import os
import sys
import logging
import decimal
from xml.dom import minidom

import opencpi.ocpi_testing as ocpi_testing

from timestamp_inserter import TimestampInserter

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
continuous_mode = os.environ["OCPI_TEST_continuous_mode"].upper() == "TRUE"
leap_seconds = int(os.environ["OCPI_TEST_leap_seconds"])
port_width = int(os.environ["OCPI_TEST_port_width"])

if port_width == 8:
    protocol = "uchar_timed_sample"
elif port_width == 16:
    protocol = "ushort_timed_sample"
elif port_width == 32:
    protocol = "ulong_timed_sample"
elif port_width == 64:
    protocol = "ulonglong_timed_sample"
else:
    raise ValueError(f"Unsupported port length: {port_width}")

if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

test_id = ocpi_testing.get_test_case()

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

case_text = str(os.environ.get("OCPI_TESTCASE"))
case = int(case_text[-2:])

subcase_text = str(os.environ.get("OCPI_TESTSUBCASE"))
subcase = int(subcase_text)

logfile = os.path.dirname(output_file_path)
logfile = os.path.join(logfile, f"{test_id}.timestamp_inserter.py.log")
logging.basicConfig(level=verify_log_level, filename=logfile, filemode="w")

# Get the application xml, and use this to find the initial value for
# timestamp_pending.
# Note: this is done as the OCPI_TEST_xxx is modified by the UUT, and the value
# present at the end of the application run is passed to this verify script.
app = minidom.parse(f"../../gen/applications/case{case:02}.{subcase:02}.xml")
timestamp_pending = None
for inst in app.getElementsByTagName("instance"):
    if inst.getAttribute("name") == "timestamp_inserter":
        component = inst
        for prop in inst.getElementsByTagName("property"):
            if prop.getAttribute("name") == "timestamp_pending":
                timestamp_pending = prop.getAttribute(
                    "value").upper() == "TRUE"
# Check value was found
if component is None or timestamp_pending is None:
    logging.error("Failed to find initial application settings")
    sys.exit(1)

timestamp_inserter_implementation = TimestampInserter(continuous_mode,
                                                      timestamp_pending, leap_seconds)


class InsertedTimeComparison(ocpi_testing.ocpi_testing._comparison_methods.Equal):
    """Check inserted times increase in value, and other messages match."""

    def __init__(self, complex_, sample_data_type):
        """Initialise the comparison class.

        Args:
            complex (bool): Indicates if the data type on the input and
            output is complex. True for it is, False for it's not.
            sample_data_type (type): The Python type that a single sample data
                value is most like. E.g. for character protocol would be an
                int, for a complex short protocol would be int.

        Returns:
            Initialised class.
        """
        super().__init__(complex_, sample_data_type)
        self._time_opcode_count = 0
        self._last_inserted_time = 0

    def _check_time_message(self, reference, implementation):
        """Check two time or sample interval messages are the same.

        For time opcodes, checks if it is an inserted opcode, and if so if the
        time value is monotonically increasing. Otherwise defers to base class
        to check values match to required level of precision.

        Args:
            reference (dict): The reference message to check the implementation
                message against. A dictionary with the keys "opcode" and "data".
            implementation (dict): Message to check against reference. A
                dictionary with the keys "opcode" and "data".

        Returns:
            A boolean to indicate if the test passed (True) or failed (False).
                Also returns a string that is the reason for any failure, in
                the case of tests which pass returns an empty string.
        """
        if reference["opcode"] == "time":
            count = self._time_opcode_count
            self._time_opcode_count += 1

            if count in timestamp_inserter_implementation.inserted_time_opcodes:
                # This is an inserted time opcode
                last_value = self._last_inserted_time
                this_value = decimal.Decimal(implementation["data"])
                self._last_inserted_time = this_value

                if this_value > last_value:
                    return True, ""
                else:
                    # Inserted time is not later than the last inserted time
                    last_str = str(last_value)
                    this_str = str(this_value)
                    width = max(len(last_str), len(this_str))

                    last_str = (f"{last_str:{width}} ==> " +
                                self._time_to_hex_string(last_value))
                    this_str = (f"{this_str:{width}} ==> " +
                                self._time_to_hex_string(this_value))
                    return False, (
                        f"{reference['opcode'].capitalize()} data inserted a "
                        + "timestamp that was not later than the prior value.\n"
                        + f"This insertion    : {this_str}\n"
                        + f"Previous insertion: {last_str}\n"
                    )

        # For sample intervals, or passed through time values, defer to the
        # base classes time comparison
        return super()._check_time_message(reference, implementation)


comparison = InsertedTimeComparison(complex_=False, sample_data_type=int)

verifier = ocpi_testing.Verifier(timestamp_inserter_implementation)
verifier.set_port_types([protocol], [protocol], [comparison])

if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)
