// Metadata Inserter RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cstring>  // for std::memcpy

#include "../../util/common/opcodes.hh"
#include "OcpiDebugApi.hh"
#include "metadata_inserter-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Metadata_inserterWorkerTypes;

class Metadata_inserterWorker : public Metadata_inserterWorkerBase {
  // Properties
  uint32_t metadata_id{0};
  uint64_t metadata_value{0};
  RCCBoolean increment_value{true};
  RCCBoolean insert_metadata_message{false};

  // Internal state
  size_t input_byte_offset{0};    // Input sample currently being processing.
  size_t trigger_port_offset{0};  // Trigger sample currently being processed.
  RCCBoolean send_metadata_flag{false};  // Indicates a metadata message
                                         // needs outputting.
  size_t samples_to_send{0};             // Internal count for the number of
                                         // samples that are ready to be output.

  RCCResult metadata_id_written() {
    this->metadata_id = properties().metadata_id;
    return RCC_OK;
  }

  RCCResult metadata_value_written() {
    this->metadata_value = properties().metadata_value;
    return RCC_OK;
  }

  RCCResult increment_value_written() {
    this->increment_value = properties().increment_value;
    return RCC_OK;
  }

  RCCResult insert_metadata_message_written() {
    this->insert_metadata_message = properties().insert_metadata_message;
    return RCC_OK;
  }

  RCCResult insert_metadata_message_read() {
    properties().insert_metadata_message = this->insert_metadata_message;
    return RCC_OK;
  }

  void insert_metadata() {
    // Although metadata_id is 32 bits, this field is padded to a 64 bit value.
    output.setInfo(Timed_sampleMetadata_OPERATION,
                   sizeof(uint64_t) + sizeof(uint64_t));
    uint8_t* output_stream = output.data();
    *reinterpret_cast<uint32_t*>(output_stream) = this->metadata_id;
    output_stream += sizeof(uint64_t);
    *reinterpret_cast<uint64_t*>(output_stream) = this->metadata_value;

    if (this->increment_value) {
      ++this->metadata_value;
    }
    output.advance();
  }

  RCCResult run(bool) {
    // User has triggered a metadata message to be output.
    if (this->insert_metadata_message) {
      insert_metadata();
      this->insert_metadata_message = false;
      return RCC_OK;
    }

    // Trigger port has triggered a metadata message to be output.
    if (this->send_metadata_flag) {
      insert_metadata();
      this->send_metadata_flag = false;
      // Samples to send must be set to 1 here to include the sample associated
      // with the trigger that triggered the output in the previous cycle.
      this->samples_to_send = 1;
      return RCC_OK;
    }

    if (input.getEOF()) {
      log(OCPI_LOG_DEBUG, "Input port EOF detected.\n");
      output.setEOF();
      return RCC_ADVANCE_FINISHED;
    }

    if (input.opCode() == Timed_sampleSample_OPERATION) {
      const uint8_t* input_stream = input.data();
      size_t input_length_bytes = input.length();
      return handle_samples(input_stream, input_length_bytes);
    } else {
      // Pass through all other opcodes and data
      return forward_opcode(input, output);
    }
  }

  // handle_samples
  //
  // Function to use the triggers received on the trigger port to determine
  // how many samples to send to the output port.
  //
  // Args:
  //  input_stream : Stream of bytes received from on the input port.
  //  input_length_bytes : Length of the incoming stream of bytes.
  RCCResult handle_samples(const uint8_t* input_stream,
                           const size_t input_length_bytes) {
    size_t input_length = input_length_bytes;

    if (this->input_byte_offset > input_length) {
      // Sample offset should not be more than the input length
      setError("Internal error, input_byte_offset > length of bytes");
      return RCC_FATAL;
    }

    if (this->input_byte_offset > 0) {
      // Already processed some samples, move stream
      // pointer past the samples already processed
      // and reduce the input length.
      input_stream += this->input_byte_offset;
      input_length -= this->input_byte_offset;
    }

    // If the trigger has reached the end of file
    // continue to send input port data to the output.
    if (!trigger.isConnected() || trigger.getEOF()) {
      log(OCPI_LOG_DEBUG, "Trigger port EOF detected.\n");
      send_bytes_to_output(input_stream, input_length);
      input.advance();
      return RCC_OK;
    }

    if (trigger.opCode() != Bool_timed_sampleSample_OPERATION) {
      // Advance past non-sample trigger opcodes
      trigger.advance();
    } else {
      RCCBoolean const* trigger_samples = trigger.sample().data().data();
      size_t trigger_samples_length = trigger.sample().data().size();

      if (this->trigger_port_offset > 0) {
        // Already processed some triggers, move trigger
        // pointer past the triggers already processed
        // and reduce the trigger port length.
        trigger_samples += this->trigger_port_offset;
        trigger_samples_length -= this->trigger_port_offset;
      }

      // Convert input_length_bytes into the number of samples and
      // calculate the minimum number of triggers to search through.
      const size_t input_length_samples =
          (input_length / (METADATA_INSERTER_PORT_WIDTH / 8));
      size_t triggers_to_search =
          std::min((input_length_samples - this->samples_to_send),
                   trigger_samples_length);

      for (size_t i = 0; i < triggers_to_search; i++) {
        if (trigger_samples[i] == 0) {
          this->samples_to_send++;
          this->trigger_port_offset++;
        } else {
          this->send_metadata_flag = true;
          // Move past the trigger that triggered the metadata
          // insertion, ready for the next software cycle.
          this->trigger_port_offset++;
          break;
        }
      }

      if (this->samples_to_send > 0) {
        if (this->send_metadata_flag) {
          // A trigger has been found send the sample(s) to the output.
          this->input_byte_offset += send_bytes_to_output(
              input_stream,
              (this->samples_to_send * (METADATA_INSERTER_PORT_WIDTH / 8)));
          this->samples_to_send = 0;
        } else {
          // No triggers have been found during this processing cycle determine
          // if the end of the input stream has been reached, if so send the
          // remaining data to the output port.
          if ((this->input_byte_offset +
               (this->samples_to_send * (METADATA_INSERTER_PORT_WIDTH / 8))) >=
              input_length_bytes) {
            this->input_byte_offset += send_bytes_to_output(
                input_stream,
                this->samples_to_send * (METADATA_INSERTER_PORT_WIDTH / 8));
            this->samples_to_send = 0;
          }
        }
      }

      if (this->input_byte_offset >= input_length_bytes) {
        // All input port samples have been processed
        input.advance();
        this->input_byte_offset = 0;
      }

      if (this->trigger_port_offset >= trigger.sample().data().size()) {
        // All trigger port samples have been processed
        trigger.advance();
        this->trigger_port_offset = 0;
      }
    }
    return RCC_OK;
  }

  inline size_t send_bytes_to_output(const uint8_t* input_stream,
                                     const size_t bytes_to_send) {
    output.setInfo(Timed_sampleSample_OPERATION, bytes_to_send);
    uint8_t* output_data_stream = output.data();
    std::copy(input_stream, input_stream + bytes_to_send, output_data_stream);
    output.advance();
    return bytes_to_send;
  }
};

METADATA_INSERTER_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
METADATA_INSERTER_END_INFO
