#!/usr/bin/env python3

# Generates the input binary file for Golay Decoder testing.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generate script.

Generate args:
1. Test case number
2. Output filename
"""
import struct
import numpy as np
import sys
import os.path
import golay_utils

if len(sys.argv) != 3:
    print("Don't run this script manually, it is called by 'ocpidev test' or 'make test'")
    sys.exit(1)

simulation = os.environ.get("OCPI_TEST_simulation")
checkbits_mask = int(os.environ.get("OCPI_TEST_checkbits_mask"))

num_codewords = 4096  # There are 2^12 codewords
num_error_patterns = 2048  # There are 2^11 3 bit error patterns

ofilename = sys.argv[2]
test_case = int(sys.argv[1])
codewords = golay_utils.gen_codewords()
error_patterns = golay_utils.gen_error_patterns()

# Apply mask to codeword to emulate toggling of checkbits done by golay_encoder
for i, codeword in enumerate(codewords):
    codewords[i] = codeword ^ (checkbits_mask << 12)

# Test sending codewords with no errors
if test_case == 1:
    with open(ofilename, "wb") as f:
        for i in range(0, num_codewords):
            f.write(struct.pack("<I", codewords[i]))
# Test all possible 0 bit error, 1 bit error, 2 bit error, and 3 bit error patterns for codewords
# The error patterns are are 23 bits wide
elif test_case == 2:
    with open(ofilename, "wb") as f:
        # For simulation test all possible 3 bit errors for just 5 codewords
        # It takes too long to test every single codeword
        if simulation == "true":
            for i in range(0, 5):
                for j in range(0, num_error_patterns):
                    f.write(struct.pack(
                        "<I", codewords[i] ^ error_patterns[j]))
        # For hardware test all possible 3 bit errors for every possible codeword
        else:
            for i in range(0, num_codewords):
                for j in range(0, num_error_patterns):
                    f.write(struct.pack(
                        "<I", codewords[i] ^ error_patterns[j]))
else:
    print("Invalid test scenario: valid test scenarios are 1 and 2")
    sys.exit(1)
