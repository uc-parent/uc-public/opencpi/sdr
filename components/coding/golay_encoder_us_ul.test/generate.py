#!/usr/bin/env python3

# Generates the input binary file for Golay Encoder testing.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generate script."""

import struct
import numpy as np
import sys
import os.path

if len(sys.argv) != 2:
    print("Don't run this script manually, it is called by 'ocpidev test' or 'make test'")
    sys.exit(1)

num_samples = 4096  # There are 2^12 codewords

ofilename = sys.argv[1]
with open(ofilename, "wb") as f:
    # Protocol uses a signed shorts (2 bytes) and the input port is 16 bits wide
    # So need to file_read to send 2 bytes of data so that the worker doesn't have
    # to chunk up the data
    f.write(struct.pack("<I", num_samples*2))
    f.write(struct.pack("<I", 0))
    for i in range(0, num_samples):
        f.write(struct.pack("<H", i))
