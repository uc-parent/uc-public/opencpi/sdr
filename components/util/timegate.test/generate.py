#!/usr/bin/env python3

# Generates the input binary file for timegate testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Timegate: Generate test input data."""

import os
import random
import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing

arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
bypass = os.environ["OCPI_TEST_bypass"].upper() == "TRUE"
gate_all_opcodes = os.environ["OCPI_TEST_gate_all_opcodes"].upper() == "TRUE"
leap_seconds = int(os.environ["OCPI_TEST_leap_seconds"])
time_correction = int(os.environ["OCPI_TEST_time_correction"])
late_time_sticky = os.environ["OCPI_TEST_late_time_sticky"].upper() == "TRUE"
port_width = int(os.environ["OCPI_TEST_port_width"])

seed = ocpi_testing.get_test_seed(arguments.case, subcase, port_width,
                                  bypass, gate_all_opcodes, leap_seconds,
                                  time_correction, late_time_sticky)

if port_width == 8:
    protocol = "uchar_timed_sample"
    generator = ocpi_testing.generator.UnsignedCharacterGenerator
elif port_width == 16:
    protocol = "ushort_timed_sample"
    generator = ocpi_testing.generator.UnsignedShortGenerator
elif port_width == 32:
    protocol = "ulong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongGenerator
elif port_width == 64:
    protocol = "ulonglong_timed_sample"
    generator = ocpi_testing.generator.UnsignedLongLongGenerator
else:
    raise ValueError(f"Unsupported port length: {port_width}")


class TimegateGenerator(generator):
    """Customised test data generator.

    Adds custom tests, with subcases for different combinations of opcodes and
    timegate properties.
    """

    def custom(self, seed, subcase):
        """Custom test to insert a specific known time.

        Generates an input opcode sequence that is
        samples (that should be forwarded without delay),
        a time opcode with a fixed time value, and then more
        samples that should be delayed if the fixed time is
        not in the past. This tests the hardware functionality of
        the timegate component, but will need the fixed time
        altering to suit when the tests are run.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        messages = []
        messages += super().typical(seed, subcase)
        # Set GPS time to the starting epoch of UTC Jan 06, 1980 00:00:00 UTC.
        # Note : If using the time_correction property this time will need to
        #        be increased otherwise underflow can occur.
        # Pass number around as strings rather than floating point.
        messages.append({"opcode": "time", "data": "0.0"})
        messages += super().typical(seed, subcase)
        return messages


generator = TimegateGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, protocol) as file_id:
    file_id.write_dict_messages(messages)
