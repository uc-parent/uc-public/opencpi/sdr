-- HDL implementation of triggered_sampler_c.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;

library ocpi;
use ocpi.types.all;

library sdr_util;
use sdr_util.sdr_util.all;

architecture rtl of worker is

  --
  -- type conversion routines
  --

  -- Convert protocol specific op-code into primitive op-code
  function convert_opcode(
    protocol_opcode : char_timed_sample_opcode_t
    ) return opcode_t is
  begin
    return opcode_t'val(char_timed_sample_opcode_t'pos(protocol_opcode));
  end function;

  -- Convert primitive op-code into protocol specific op-code
  function convert_opcode(
    primitive_opcode : opcode_t
    ) return char_timed_sample_opcode_t is
  begin
    return char_timed_sample_opcode_t'val(opcode_t'pos(primitive_opcode));
  end function;

  signal input_in_opcode   : opcode_t;
  signal output_out_opcode : opcode_t;

begin

  --
  -- Instantiate the protocol independent primitive to do the work
  --
  triggered_sampler_i : triggered_sampler
    generic map (
      data_width_g           => input_in.data'length,
      capture_length_width_g => props_in.capture_length'length,
      little_endian_g        => ocpi_endian = little_e
      )
    port map (
      clk                     => ctl_in.clk,
      rst                     => ctl_in.reset,
      bypass                  => props_in.bypass,
      capture_single_in       => props_in.capture_single,
      capture_single_written  => props_in.capture_single_written,
      capture_single_out      => props_out.capture_single,
      capture_continuous      => props_in.capture_continuous,
      capture_on_meta         => props_in.capture_on_meta,
      capture_length          => props_in.capture_length,
      send_flush              => props_in.send_flush,
      capture_in_progress_out => props_out.capture_in_progress,
      trigger_som             => trigger_in.som,
      trigger_ready           => trigger_in.ready,
      trigger_take            => trigger_out.take,
      input_data              => input_in.data,
      input_opcode            => input_in_opcode,
      input_som               => input_in.som,
      input_eom               => input_in.eom,
      input_eof               => input_in.eof,
      input_valid             => input_in.valid,
      input_ready             => input_in.ready,
      input_take              => input_out.take,
      output_data             => output_out.data,
      output_opcode           => output_out_opcode,
      output_eom              => output_out.eom,
      output_eof              => output_out.eof,
      output_valid            => output_out.valid,
      output_give             => output_out.give,
      output_ready            => output_in.ready
      );

  -- type conversion
  input_in_opcode   <= convert_opcode(input_in.opcode);
  output_out.opcode <= convert_opcode(output_out_opcode);

end rtl;
