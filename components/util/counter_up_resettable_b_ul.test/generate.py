#!/usr/bin/env python3

# Generates the input binary file for counter_up_resettable_b_ul testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI generator for counter_up_resettable_b_ul tests."""

import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


# Get arguments used in the ocpi testing style
arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
enable = bool(os.environ.get("OCPI_TEST_enable"))
auto_reset = bool(os.environ.get("OCPI_TEST_auto_reset"))
period = int(os.environ.get("OCPI_TEST_period"))
reset_count = int(os.environ.get("OCPI_TEST_reset_count"))
nonsample_output_select = bool(
    os.environ.get("OCPI_TEST_nonsample_output_select"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase, enable,
                                  auto_reset, period, reset_count,
                                  nonsample_output_select)

generator = ocpi_testing.generator.BooleanGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
