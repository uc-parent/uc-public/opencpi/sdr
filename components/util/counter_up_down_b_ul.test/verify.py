#!/usr/bin/env python3

# Runs checks for counter_up_down_b_ul testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI verification script for counter_up_down_b_ul tests."""

import os
import pathlib
import sys
from xml.dom import minidom

import opencpi.ocpi_testing as ocpi_testing

from counter_up_down_b_ul import counter_up_down_b_ul

enable = (os.environ.get("OCPI_TEST_enable").lower() == "true")
direction = (os.environ.get("OCPI_TEST_direction").lower() == "true")

case_text = str(os.environ.get("OCPI_TESTCASE"))
case = int(case_text[-2:])

subcase_text = str(os.environ.get("OCPI_TESTSUBCASE"))
subcase = int(subcase_text)

# Get the application xml, and use this to find the initial values
app = minidom.parse(f"../../gen/applications/case{case:02}.{subcase:02}.xml")
component = None
counter_value = None
for inst in app.getElementsByTagName("instance"):
    if inst.getAttribute("name") == "counter_up_down_b_ul":
        component = inst
        for prop in inst.getElementsByTagName("property"):
            if prop.getAttribute("name") == "counter_value":
                counter_value = int(prop.getAttribute("value"))
        break

if component is None or counter_value is None:
    print("Failed to find initial application settings")
    sys.exit(1)

size = int(os.environ.get("OCPI_TEST_size"))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

counter_up_down_b_ul_implementation = counter_up_down_b_ul(
    enable, direction, counter_value, size)

test_id = ocpi_testing.get_test_case()
test_case, test_subcase = ocpi_testing.id_to_case(test_id)

verifier = ocpi_testing.Verifier(counter_up_down_b_ul_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["ulong_timed_sample"], ["equal"])

if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    if enable:
        counter_value_property = int(os.environ.get("OCPI_TEST_counter_value"))
        if counter_value_property != counter_up_down_b_ul_implementation.counter_value:
            fail_message = (
                "counter_value_property does not match expected value\n" +
                "  Python count                   : " +
                f"{counter_up_down_b_ul_implementation.counter_value}\n" +
                f"  Implementation-under-test count: {counter_value_property}")

            # No runtime variables has the worker under test so use the output
            # data file name
            case_worker_port = pathlib.Path(output_file_path).stem.split(".")
            worker = f"{case_worker_port[-3]}.{case_worker_port[-2]}"

            verifier.test_failed(worker, "counter_value", test_case,
                                 test_subcase, fail_message)
            sys.exit(1)
    sys.exit(0)
else:
    sys.exit(1)
