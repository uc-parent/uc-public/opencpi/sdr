// Common opcode enum used in protocol-less components.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

// LINT EXCEPTION: cpp_005: 2: Define use allowed in include guard
#ifndef COMPONENTS_UTIL_COMMON_OPCODES_HH_
#define COMPONENTS_UTIL_COMMON_OPCODES_HH_

// LINT EXCEPTION: cpp_008: 1: Include allowed after include guard
#include "RCC_Worker.hh"

// Enumeration constants for the protocol-less operation codes.
enum Timed_sampleOpCodes {
  Timed_sampleSample_OPERATION,
  Timed_sampleTime_OPERATION,
  Timed_sampleSample_interval_OPERATION,
  Timed_sampleFlush_OPERATION,
  Timed_sampleDiscontinuity_OPERATION,
  Timed_sampleMetadata_OPERATION
};

using PORT = OCPI::RCC::RCCUserPort&;
inline OCPI::RCC::RCCResult forward_opcode(PORT input, PORT output) {
  output.setInfo(input.opCode(), input.length());
  if (input.length()) {
    std::copy(input.data(), input.data() + input.length(), output.data());
  }
  input.advance();
  output.advance();
  return OCPI::RCC::RCC_OK;
}

#endif  // COMPONENTS_UTIL_COMMON_OPCODES_HH_
