// RCC implementation of triggered_sampler_b worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../../math/common/time_utils.hh"
#include "OcpiDebugApi.hh"
#include "triggered_sampler_f-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Triggered_sampler_fWorkerTypes;

class Triggered_sampler_fWorker : public Triggered_sampler_fWorkerBase {
  // ***************************************************
  // Component Properties
  bool bypass = false;
  uint32_t capture_length = 1;
  bool send_flush = false;
  bool capture_on_meta = false;
  bool capture_single = false;
  bool capture_continuous = false;

  // Internal clock vars
  uint32_t time_seconds = 0;
  uint64_t time_fraction = 0;
  uint32_t interval_seconds = 0;
  uint64_t interval_fraction = 0;
  bool time_valid = false;
  bool interval_valid = false;
  bool time_sent = false;
  bool interval_sent = false;

  // Used when waiting for trigger or (input&output) ports
  const RunCondition m_input_output_run_condition;
  // Used when needing to output with no input/trigger
  const RunCondition m_output_only_run_condition;

  // Initialise internal variables
  uint32_t samples_left_to_send = 0;  // capture_length - samples sent
  bool capture_queued = false;
  bool capture_pending = false;
  bool flush_sent = false;
  bool discontinuity_sent = false;
  bool capture_finished = false;
  bool trigger_activated = false;
  bool pending_caused_by_capture_single = false;
  bool queued_caused_by_capture_single = false;
  uint32_t capture_single_count = 0;
  bool send_time_si = false;
  bool precursor_sent = false;

  // ***************************************************
  // Property updated callbacks
  RCCResult capture_length_written() {
    this->capture_length = properties().capture_length;
    if (this->capture_length == 0) {
      setError("Capture length must not be zero.");
      return RCC_FATAL;
    }
    return RCC_OK;
  }
  RCCResult send_flush_written() {
    this->send_flush = properties().send_flush;
    return RCC_OK;
  }
  RCCResult capture_on_meta_written() {
    this->capture_on_meta = properties().capture_on_meta;
    return RCC_OK;
  }
  RCCResult capture_single_written() {
    this->capture_single = properties().capture_single;
    // on receiving a write to this property, check if we
    // need to add an additional capture to the queue
    if (this->bypass) {
      // ignore single capture write
      properties().capture_single = this->capture_single = false;
    } else {
      this->capture_single = properties().capture_single;
      if (this->capture_single) {
        // add a capture to the queue
        // can have only two capture queued at most
        if (this->capture_pending) {
          this->queued_caused_by_capture_single = this->capture_queued = true;
        } else {
          this->pending_caused_by_capture_single = this->capture_pending = true;
        }
      } else {
        // remove most recent capture from the queue
        // can't remove a capture unless one is queued
        if (this->capture_queued) {
          this->queued_caused_by_capture_single = this->capture_queued = false;
        } else {
          this->pending_caused_by_capture_single = this->capture_pending =
              false;
        }
      }
    }
    return RCC_OK;
  }
  RCCResult capture_continuous_written() {
    this->capture_continuous = properties().capture_continuous;
    return RCC_OK;
  }
  RCCResult bypass_written() {
    this->bypass = properties().bypass;
    if (this->bypass) {
      // mark that capture is no longer in progress
      properties().capture_in_progress = false;
      // capture_single reset
      properties().capture_single = this->capture_single = false;
      this->capture_single_count = 0;
    }
    return RCC_OK;
  }

  // ***************************************************
  // ControlOperations Functions

  RCCResult run(bool) {
    // check the trigger input.
    // If a trigger is present, end this run and start again.
    // If no triggers are present on the trigger input, then
    // continue with execution. This continually reads from
    // the trigger input until all initial triggers have been read,
    // before reading from the input port.
    handle_trigger();
    if (this->trigger_activated) {
      this->trigger_activated = false;
      return RCC_OK;
    }

    if (input.hasBuffer() && output.hasBuffer()) {
      // check to see if we need to send a flush to output
      if (this->send_flush && !this->flush_sent && this->capture_finished) {
        // send the flush
        // mark component as output only until discontinuity sent
        setRunCondition(&this->m_output_only_run_condition);
        output.setOpCode(Float_timed_sampleFlush_OPERATION);
        output.advance();
        this->flush_sent = true;
        return RCC_OK;
      }
      // check to see if we need to send the discontinuity to output
      if (!this->discontinuity_sent && this->capture_finished) {
        setRunCondition(&this->m_input_output_run_condition);
        output.setOpCode(Float_timed_sampleDiscontinuity_OPERATION);
        output.advance();
        this->discontinuity_sent = true;
        return RCC_OK;
      }
    }
    if (input.hasBuffer() && input.getEOF()) {
      // Received EOF
      output.setEOF();
      return RCC_ADVANCE_DONE;
    }

    // check to see if need to send time or sample interval
    if (this->send_time_si) {
      if (this->time_valid && !this->time_sent) {
        // if time is valid and hasn't already been sent, send it
        output_time();
        this->time_sent = true;
        // Output only while potentially sending time and sample interval
        setRunCondition(&this->m_output_only_run_condition);
        return RCC_OK;
      }
      if (this->interval_valid && !this->interval_sent) {
        // if interval is valid and hasn't been sent, send it
        output_interval();
        this->interval_sent = true;
        return RCC_OK;
      }
      this->precursor_sent = true;
    }

    if (input.opCode() == Float_timed_sampleSample_OPERATION) {
      // read size of input and increment counter
      size_t input_length = input.sample().data().size();
      if (input.hasBuffer() && output.hasBuffer()) {
        // Due to start a capture, and have received samples
        if (!this->bypass && this->samples_left_to_send == 0 &&
            (this->capture_pending || this->capture_continuous)) {
          // check if we've already sent all precursor information
          if (!this->precursor_sent) {
            // mark that we need to send the time/sample interval
            this->send_time_si = true;
            // switch to output only mode while sending info
            setRunCondition(&this->m_output_only_run_condition);
            return RCC_OK;
          } else {
            // reset bool flags for next capture
            this->send_time_si = false;
            this->precursor_sent = false;
          }
          // switch back to input and output mode
          setRunCondition(&this->m_input_output_run_condition);
          // as this is a new capture, the number of samples we need to capture
          // is equal to the capture_length property
          this->samples_left_to_send = this->capture_length;

          // Check to see whether the input is larger or smaller than the number
          // of samples needed
          increment_timestamp(input_length);
          if (input_length > this->samples_left_to_send) {
            capture_samples(this->samples_left_to_send);
          } else {
            capture_samples(input_length);
          }
          return RCC_OK;
        }
        // ensure that the internal time remains accurate
        increment_timestamp(input_length);
        // Currently in a capture, send through the remaining samples
        if (this->samples_left_to_send > 0) {
          // Check whether the input is larger or smaller than the number
          // of samples required to complete the capture
          if (input_length > this->samples_left_to_send) {
            capture_samples(this->samples_left_to_send);
          } else {
            capture_samples(input_length);
          }
          return RCC_OK;
        } else if (this->bypass) {
          // Bypass mode, ignore all flags and simply forward
          // all received samples to output
          this->samples_left_to_send = 0;
          output.setOpCode(Float_timed_sampleSample_OPERATION);
          output.sample().data().resize(input_length);
          const RCCFloat* in_data_pointer = input.sample().data().data();
          RCCFloat* out_data_pointer = output.sample().data().data();
          size_t length_of_bytes = input_length * sizeof(in_data_pointer[0]);
          memcpy(out_data_pointer, in_data_pointer, length_of_bytes);
          input.advance();
          output.advance();
        } else {
          // Otherwise, we're not currently a capture, and not due to start a
          // capture so ignore incoming samples
          input.advance();
          return RCC_OK;
        }
      }
      return RCC_OK;
    } else if (input.opCode() == Float_timed_sampleTime_OPERATION) {
      if (input.hasBuffer() && output.hasBuffer()) {
        // update the internal time from the received input
        update_time_from_input();
        // forward time to output in bypass mode or when capture is active
        if (this->bypass || this->samples_left_to_send > 0) {
          output_time();
          input.advance();
          return RCC_OK;
        }
        input.advance();
        return RCC_OK;
      }
      return RCC_OK;
    } else if (input.opCode() == Float_timed_sampleSample_interval_OPERATION) {
      if (input.hasBuffer() && output.hasBuffer()) {
        // update the internal sample interval value
        update_interval_from_input();
        if (this->bypass || this->samples_left_to_send > 0) {
          // forward sample_interval to output in bypass mode or during capture
          output_interval();
          input.advance();
          return RCC_OK;
        }
        input.advance();
        return RCC_OK;
      }
      return RCC_OK;
    } else if (input.opCode() == Float_timed_sampleFlush_OPERATION) {
      if (input.hasBuffer() && output.hasBuffer()) {
        // Always forward flush to output
        output.setOpCode(Float_timed_sampleFlush_OPERATION);
        output.advance();
        input.advance();
        return RCC_OK;
      }
      return RCC_OK;
    } else if (input.opCode() == Float_timed_sampleDiscontinuity_OPERATION) {
      if (input.hasBuffer() && output.hasBuffer()) {
        // invalidate the internal time counter
        this->time_valid = false;
        // Forward discontinuity in bypass mode or when capture is active
        if (this->samples_left_to_send > 0 || this->bypass) {
          output.setOpCode(Float_timed_sampleDiscontinuity_OPERATION);
          output.advance();
          input.advance();
          return RCC_OK;
        }
        input.advance();
        return RCC_OK;
      }
      return RCC_OK;
    } else if (input.opCode() == Float_timed_sampleMetadata_OPERATION) {
      if (input.hasBuffer() && output.hasBuffer()) {
        // Always forward metadata to output
        output.setOpCode(Float_timed_sampleMetadata_OPERATION);
        output.metadata().id() = input.metadata().id();
        output.metadata().value() = input.metadata().value();
        // check if this metadata ought to cause a trigger
        if (!this->bypass && this->capture_on_meta) {
          if (this->samples_left_to_send > 0 || this->capture_pending) {
            // if we're in a capture or there's already one pending, queue
            // another
            this->capture_queued = true;
            this->queued_caused_by_capture_single = false;
          } else {
            // No capture in progress, mark capture pending
            this->capture_pending = true;
            this->pending_caused_by_capture_single = false;
          }
        }
        input.advance();
        output.advance();
        return RCC_OK;
      }
      return RCC_OK;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }

  // ***************************************************
  // Functions
  // send internal time to output
  void output_time() {
    output.setOpCode(Float_timed_sampleTime_OPERATION);
    output.time().fraction() = time_fraction;
    output.time().seconds() = time_seconds;
    output.advance();
  }

  // send internal sample interval to output
  void output_interval() {
    output.setOpCode(Float_timed_sampleSample_interval_OPERATION);
    output.sample_interval().fraction() = interval_fraction;
    output.sample_interval().seconds() = interval_seconds;
    output.advance();
  }

  // update internal sample interval from input
  void update_interval_from_input() {
    this->interval_seconds = input.sample_interval().seconds();
    this->interval_fraction = input.sample_interval().fraction();
    this->interval_valid = true;
  }

  // update internal time from input
  void update_time_from_input() {
    this->time_seconds = input.time().seconds();
    this->time_fraction = input.time().fraction();
    this->time_valid = true;
  }

  // Capture number_of_samples worth of input and send to output
  void capture_samples(size_t number_of_samples) {
    // started a capture, so it's no longer pending
    this->capture_pending = false;
    // ensure that capture_single is reset if we're starting
    // the last capture caused by a capture_single trigger
    if (this->pending_caused_by_capture_single &&
        !this->queued_caused_by_capture_single) {
      properties().capture_single = this->capture_single = false;
    }

    // copy a given amount of samples from input to output
    output.setOpCode(Float_timed_sampleSample_OPERATION);
    output.sample().data().resize(number_of_samples);
    const RCCFloat* in_data_pointer = input.sample().data().data();
    RCCFloat* out_data_pointer = output.sample().data().data();
    size_t length_of_bytes = number_of_samples * sizeof(in_data_pointer[0]);
    memcpy(out_data_pointer, in_data_pointer, length_of_bytes);

    // Advance both ports
    output.advance();
    input.advance();

    // update the internal time based on number of samples
    // seen in input. Decrease the number of samples left to send
    if (number_of_samples > this->samples_left_to_send) {
      this->samples_left_to_send = 0;
    } else {
      this->samples_left_to_send -= number_of_samples;
    }

    // check to see if we've completed the capture
    if (this->samples_left_to_send == 0) {
      // set to output only as need to send flush/discontinuity
      setRunCondition(&m_output_only_run_condition);
      // reset variables to ensure trailing messages are sent
      this->capture_finished = true;
      this->time_sent = false;
      this->interval_sent = false;
      this->flush_sent = false;
      this->discontinuity_sent = false;
      // check to see if we have another capture queued
      if (this->capture_queued) {
        // if there is, mark a capture as pending and reset the queue
        this->capture_queued = false;
        this->capture_pending = true;
        // shift down the bools tracking what was caused by capture_single
        this->pending_caused_by_capture_single =
            this->queued_caused_by_capture_single;
        this->queued_caused_by_capture_single = false;

      } else {
        // otherwise, mark capture as completed
        this->capture_pending = this->pending_caused_by_capture_single = false;
      }
    }
  }

  // increase the internal time using the number of samples seen and the sample
  // interval
  void increment_timestamp(size_t number_of_samples) {
    if (number_of_samples == 0) {
      // No need to update samples
      return;
    }
    if (!this->interval_valid) {
      // if just the interval is invalid, then the time is invalid
      this->time_valid = false;
      return;
    }
    if (!this->time_valid || !this->interval_valid) {
      // if either the time or the interval aren't valid, then can't compute the
      // new time
      return;
    }
    for (size_t counter = 0; counter < number_of_samples; counter++) {
      time_utils::add(&this->time_seconds, &this->time_fraction,
                      this->interval_seconds, this->interval_fraction);
    }
  }

  // read from the trigger, and handle queueing / starting a capture
  void handle_trigger() {
    if (trigger.isConnected() && trigger.hasBuffer()) {
      if (trigger.getEOF()) {
        // Nothing on the trigger!
        return;
      }
      // mark that there was a trigger present
      this->trigger_activated = true;
      trigger.advance();

      if (!this->bypass) {
        if (this->samples_left_to_send > 0 || this->capture_pending) {
          // in a capture, or capture already pending, so queue another
          this->capture_queued = true;
          this->queued_caused_by_capture_single = false;
        } else {
          // otherwise, mark that a capture is pending
          this->capture_pending = true;
          this->pending_caused_by_capture_single = false;
        }
      }
    }
  }

 public:
  // Constructor used to initialise run conditions for various states,
  // but done here to allow them to be const.
  Triggered_sampler_fWorker()  // End of list.
  {
    // OCPI::OS::logSetLevel(OCPI_LOG_INFO);
    log(OCPI_LOG_INFO, "Constructor called debug");
  }
};

TRIGGERED_SAMPLER_F_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
TRIGGERED_SAMPLER_F_END_INFO
