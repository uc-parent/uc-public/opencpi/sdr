.. triggered_sampler_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _triggered_sampler_us:


Triggered Sampler (``triggered_sampler_us``)
============================================
Upon trigger (either property change, data on optional input port, or meta data on data port) gate ``capture_length`` samples followed by an optional flush, and a discontinuity.

Design
------
When triggered, ``capture_length`` samples are transferred from ``input`` to ``output``. These samples are preceded by time and sample_interval operations if the corresponding data is available, and are followed by an optional flush, and a discontinuity.

Single captures are triggered through a write to a property, optional receipt of input metadata or input data on a second optional ``trigger`` port. The component also supports a continuous capture mode (controlled through a property), in which captures are repeatedly performed; although it should be noted that a discontinuity will still exist between captures in this mode.

More specifically, captures are triggered when:

    * true is written to the ``capture_single`` property, or

    * the ``capture_continuous`` property is true and there is no capture currently in progress, or

    * the ``capture_on_meta`` property is true and any metadata message is received on ``input`` port, or

    * any message is received on the optional ``trigger`` port.

Triggers that occur simultaneously (i.e. within the same cycle in HDL or on the same call to run() in RCC) will result in a single capture.

If additional triggers occur during a capture, another capture is performed once the current capture is complete.

Output sample messages will always be aligned to input sample messages. Once a capture is triggered, the sample output will begin with the start of the next input sample message. Output sample messages will be the same size as the input sample messages, except possibly the final message in a capture which may be shorter to achieve the correct capture size.

The following diagrams depict typical message flows for the four trigger cases. In these examples it is assumed that time and sample interval are known, and are therefore output prior to each capture.

Capture Single
~~~~~~~~~~~~~~

.. triggered_sampler_b-capture-single-diagram:

.. figure:: ../triggered_sampler_b.comp/triggered_sampler_capture_single.svg
   :alt: Message flow for single capture.
   :align: center

   Capture single

Capture Continuous
~~~~~~~~~~~~~~~~~~

.. triggered_sampler_b-capture-continuous-diagram:

.. figure:: ../triggered_sampler_b.comp/triggered_sampler_capture_continuous.svg
   :alt: Message flow for continuous capture.
   :align: center

   Capture continuous

Trigger Port Operation
~~~~~~~~~~~~~~~~~~~~~~

.. triggered_sampler_b-trigger-port-diagram:

.. figure:: ../triggered_sampler_b.comp/triggered_sampler_trigger_port_operation.svg
   :alt: Message flow for trigger port operation.
   :align: center

   Trigger port operation

Metadata Operation
~~~~~~~~~~~~~~~~~~

.. triggered_sampler_b-metadata-diagram:

.. figure:: ../triggered_sampler_b.comp/triggered_sampler_metadata_operation.svg
   :alt: Message flow for metadata operation.
   :align: center

   Metadata operation

Bypass Mode
~~~~~~~~~~~

The component also supports a bypass mode which disables the capture triggers and simply routes all input messages to output.

While in bypass mode, the behaviour of the capture triggers is:

    * ``capture_single`` - writing true does nothing - no capture is queued.

    * ``capture_continuous`` - writing true will cause captures to start once bypass mode completes.

    * trigger port - no captures are queued.

    * metadata message - no captures are queued.

When entering bypass mode:

    * Any pending captures are cancelled.

    * Any current capture is aborted - no flush or discontinuity are generated.

Interface
---------
.. literalinclude:: ../specs/triggered_sampler_us-spec.xml
   :language: xml
   :lines: 1,25-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   trigger: Optional trigger port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
**On input port:**

   * Time

      * Re-synchronises an internal sample time counter that is used to generate correct output time messages.

      * During a capture, message is forwarded to output.

      * In bypass mode, message is forwarded to output.

   * Sample_Interval

      * Value is stored and used for maintaining the internal sample time counter.

      * During a capture, message is forwarded to output.

      * In bypass mode, message is forwarded to output.

   * Flush

      * Outside of a capture, flush is forwarded to output.

      * During a capture:

         * The current output samples message is completed.

         * The received flush is forwarded to output.

         * The capture continues and any remaining samples are forwarded when they arrive.

         * This results in a single capture of ``capture_length`` samples, but with an extra flush in the middle.

      * In bypass mode, message is forwarded to output.

   * Discontinuity

      * Invalidates the internal sample time counter.

      * In non-bypass mode and outside of a capture, only the internal sample counter is invalidated, no data is forwarded to output.

      * During a capture:

         * The current output samples message is completed.

         * The received discontinuity is forwarded to output.

         * The capture continues and any remaining samples are forwarded when they arrive.

         * This results in a single capture of ``capture_length`` samples, but with an extra discontinuity in the middle.

      * In bypass mode, message is forwarded to output.

   * Metadata

      * In non-bypass mode and outside of a capture, this optionally triggers a capture.

      * During a capture, this optionally queues another capture to start when the current one completes.

      * Received metadata messages are always passed to output.

   * Samples

      * In all modes, the internal sample time counter is incremented and the samples are dropped.

      * During a capture, the samples are passed to the output.

      * In bypass mode, the message is forwarded to output.


**On trigger port**

Any message received on this port while in non-bypass mode will trigger a single capture. The trigger port is read from continuously until there are no more messages present on the trigger port. After checking the trigger port, the input port is checked to handle any incoming opcodes. A maximum of two captures can be queued at any one time (regardless of how the capture was initially triggered). If a trigger is received while two captures are already queued (i.e. one capture is pending and one capture is queued), then the trigger port is advanced and the trigger ignored. Otherwise, if a capture is pending, but no capture is queued, receipt of input on the trigger port will cause a capture to be queued. If no captures are pending or queued, then an incoming trigger will cause a capture to start, pending arrival of Sample opcodes.

All received messages are otherwise discarded; nothing is forwarded to the output port.

**On output port:**

   * Time

      * If known, time is sent at the start of every capture (ahead of interval and samples)

      * It won't be known if a time has not been received since start or since most recent discontinuity.

      * Time messages received on input during a capture or bypass mode are forwarded to output.

   * Sample_Interval

      * If known, sample_interval is sent at the start of every capture (just after time)

      * It won't be known if an interval has not been received since start.

      * Sample_interval messages received on input during a capture or bypass mode are forwarded to output.

   * Flush

      * Optionally sent (controlled by "send_flush" property) after the last sample in a capture.

      * Flush messages received on input at any time are forwarded to output.

   * Discontinuity

      * Sent at the end of every capture (after the optional flush).

      * Discontinuity messages received on input during a capture or bypass mode are forwarded to output.

   * Metadata

      * Metadata messages received on input at any time are forwarded to output.

   * Samples

      * Sample messages received on input during a capture or bypass mode are forwarded to output.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

  bypass: When ``false``, the component is in normal mode and the capture functionality is enabled.

  capture_length: Changes only affect captures which have yet to start (even if they have been triggered, but are pending completion of a previous capture). Minimum = 1, Maximum = :math:`2^{32}-1`

  capture_in_progress: Returns ``false`` when no captures are pending. In bypass mode, this returns ``false``.

  capture_single: Writing ``true`` triggers a single capture. It will return ``true`` until the capture actually starts (this is almost immediate if there is no capture in progress). Writing ``true`` again during a capture causes another capture to be queued to start when the current one finishes. It will return ``true`` until that second capture actually starts. Writing ``true`` while it is already ``true`` (i.e. when another capture is queued) does nothing.  Writing ``false`` while it is ``true`` (i.e. when another capture is queued) cancels that queued capture. Note that the return value indicates whether a capture has started - not completed. It does not return ``true`` when captures are triggered by metadata or trigger port messages. To check if all the requested captures have completed, read ``capture_in_progress``. If ``capture_single`` is ``true`` on entry to bypass mode, it will immediately become ``false``, thus cancelling any queued capture. In bypass mode, any value written to this is ignored, and it will return ``false``.

  capture_continuous:  Note that when one capture finishes, the next capture will start at the beginning of the next sample message - there may be a gap in samples between the captures, depending on the message and capture lengths. Write ``false`` to cease capturing at the end of the current capture. In bypass mode, any value written will be stored and returned, although no captures will be performed. If ``true`` is written during bypass mode, captures will begin when the component returns to normal mode.

Implementations
---------------
.. ocpi_documentation_implementations:: ../triggered_sampler_us.hdl ../triggered_sampler_us.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Triggered Sampler primitive <triggered_sampler-primitive>`


There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``triggered_sampler_us`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
