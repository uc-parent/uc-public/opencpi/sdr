#!/usr/bin/env python3

# Python implementation of serial crc block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""The CrcGenerator Engine."""


class CrcGenerator():
    """The CrcGenerator Engine."""

    def __init__(self, width, polynomial_taps, input_reflect, output_reflect, initial_seed, final_xor):
        """Initialise the CrcGenerator Engine.

        Args:
            width (int): Width of input data in bits
            polynomial_taps (string): Comma separated list of (bit_size, taps[0]...taps[n]).
            input_reflect (bool): Reverse input bits
            output_reflect (bool): Reverse calculated crc bits
            initial_seed (int): Initial seed value for crc.
            final_xor: Apply XOR to final calculated value.
        """
        self._polynomial_taps = polynomial_taps.split(",")

        # Get polynomial and size from taps
        self._polynomial_size = int(self._polynomial_taps[0])
        self._polynomial_mask = 2**self._polynomial_size - 1
        self._polynomial = 0
        for val in self._polynomial_taps[1:]:
            self._polynomial += 1 << int(val)
        self._width = width
        self._initial_seed = initial_seed
        self._input_reflect = input_reflect
        self._output_reflect = output_reflect
        self._final_xor = final_xor

    def calculate_crc(self, data):
        """Perform calculation.

        Args:
            data (list): Data for which to calculate crc.

        Returns:
            Calculated CRC.
        """
        crc = self._initial_seed & self._polynomial_mask

        for value in data:
            if self._input_reflect:
                value_string = format(value, f"0{self._width}b")
                value = int(value_string[::-1], 2)

            crc ^= (value << (self._polynomial_size - self._width)
                    ) & self._polynomial_mask

            for _ in range(self._width):
                # Test for MSB
                if crc & (2**(self._polynomial_size - 1)):
                    crc = ((crc << 1) ^ self._polynomial) & self._polynomial_mask
                else:
                    crc = crc << 1

        # Reflect output CRC
        if self._output_reflect:
            crc_string = format(crc, f"0{self._polynomial_size}b")
            crc = int(crc_string[::-1], 2)
        else:
            crc = int(crc)

        # Final XOR
        crc ^= (self._final_xor & self._polynomial_mask)

        return crc
