#!/usr/bin/env python3

# Tests of the python implementation of Symmetric FIR Filter block
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Symmetric FIR filter pytest."""

import pytest
import numpy as np
import scipy.signal as sig

from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric
from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric_Int
from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric_ComplexInt


@pytest.fixture(params=[FirFilterSymmetric,
                        FirFilterSymmetric_Int,
                        FirFilterSymmetric_ComplexInt])
def fixture_fir(request):
    """Get fir."""
    return request.param


@pytest.fixture(params=[0.000125, 8.0125, (2**32)-1])
def fixture_group_delay(request):
    """Get group_delay."""
    return request.param


@pytest.fixture(params=[1.5, 0, (2**32)-1])
def fixture_time(request):
    """Get time."""
    return request.param


@pytest.fixture(params=[0, 4, 2**16-1])
def fixture_flush_length(request):
    """Get flush_length."""
    return request.param


@pytest.fixture()
def input_signal():
    """Create some samples, Interpolate, then decimate with this."""
    sample_length = 1024
    vals = [0, 1, 0, 0, 0,
            complex(4, 1), 0, 0, 0, 0,
            complex(0, -1), 0, 0, complex(0, 2), 0]
    signal = np.fft.fft(vals, sample_length, axis=0)
    return signal / max(abs(signal))


@pytest.fixture(params=[8, 9, 25, 100, 255])
def fixture_taps(request):
    """Get taps."""
    number_of_taps = request.param

    taps = sig.firwin(number_of_taps, cutoff=1200, nyq=24000)
    assert len(taps) == number_of_taps
    return taps
