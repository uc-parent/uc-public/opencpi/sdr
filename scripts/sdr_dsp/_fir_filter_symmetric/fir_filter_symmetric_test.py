#!/usr/bin/env python3

# Tests of the python implementation of Symmetric FIR Filter block
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import pytest
import numpy as np
import scipy.signal as sig

from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric
from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric_Int
from sdr_dsp.fir_filter_symmetric import FirFilterSymmetric_ComplexInt

from fixtures import *


@pytest.fixture()
def fixture_example_input():
    """Provide example input to tests.

    Returns:
        example input
    """
    return [0x7FFF, 0x2A95, 0x44B3, 0x29E9, 0xE44A, 0x2894, 0xE2A3, 0x269B,
            0x3FC2, 0x2406, 0x77D9, 0x20E0, 0x3978, 0x1D35, 0xD629, 0x1914,
            0xD1D4, 0x148E, 0x2C89, 0x0FB5, 0x6284, 0x0A9C, 0x225D, 0x0559,
            0xBDA5, 0x0000, 0xB849, 0xFAA7, 0x1260, 0xF564, 0x4825, 0xF04B,
            0x0834, 0xEB72, 0xA41B, 0xE6EC, 0x9FC5, 0xE2CB, 0xFB44, 0xDF20,
            0x32D0, 0xDBFA, 0xF4FB, 0xD965, 0x934C, 0xD76C, 0x91A4, 0xD617,
            0xF00A, 0xD56B, 0x2AAA, 0xD56B, 0xF00A, 0xD617, 0x91A4, 0xD76C,
            0x934C, 0xD965, 0xF4FB, 0xDBFA, 0x32D0, 0xDF20, 0xFB44, 0xE2CB]


@pytest.fixture(params=["odd", "even"])
def fixture_non_symmetric_taps(request):
    """Provide odd/even coefficient taps and expected results to tests.

    Args:
        request : odd or even

    Returns:
        coefficients and expected result
    """
    if request.param == "odd":
        # Odd number of taps
        taps = [0x0000, 0x0100, 0x0200, 0x0300, 0x0400, 0x0500, 0x0600, 0x0700,
                0x0800,
                0x0900, 0x0a00, 0x0b00, 0x0c00, 0x0d00, 0x0e00, 0x0f00, 0x1000]
        expected = [0x1000, 0x1453, 0x1B94, 0x1EF2, 0x1949, 0x1C61, 0x1669, 0x192B,
                    0x1EC5, 0x2267, 0x30E7, 0x34AA, 0x3BE6, 0x3EB8, 0x3910, 0x3BA4,
                    0x35B9, 0x2880, 0x2905, 0x23A9, 0x2C11, 0x3287, 0x337B, 0x38DC,
                    0x2CD3, 0x253D, 0x1832, 0x094A, 0x0840, 0x0177, 0x0892, 0x0DE1,
                    0x0DD3, 0x1256, 0x0598, 0xFD74, 0xF003, 0xE0DE, 0xDFC0, 0xD90A,
                    0xE063, 0xE618, 0xE698, 0xEBD1, 0xDFEF, 0xD8CE, 0xCC85, 0xBEAB,
                    0xBEFB, 0xB9D5, 0xC2DD, 0xCA5E, 0xCCC6, 0xD402, 0xCA3B, 0xC54B,
                    0xBB46, 0xAFC2, 0xB277, 0xAFC2, 0xBB46, 0xC54B, 0xCA3B, 0xD402]
    else:
        # Even number of taps
        taps = [0x0000, 0x0100, 0x0200, 0x0300, 0x0400, 0x0500, 0x0600, 0x0700]
        expected = [0x0700, 0x0854, 0x0AC1, 0x0B2D, 0x0877, 0x0B09, 0x0A55, 0x0E71,
                    0x0BBD, 0x0B75, 0x0D9A, 0x0B43, 0x0ED6, 0x0D24, 0x0C9B, 0x0D3B,
                    0x0899, 0x09E2, 0x06B4, 0x05F8, 0x07AE, 0x04EE, 0x081E, 0x0611,
                    0x0533, 0x0586, 0x009E, 0x01AB, 0xFE47, 0xFD5E, 0xFEF1, 0xFC16,
                    0xFF34, 0xFD1D, 0xFC40, 0xFC9C, 0xF7C6, 0xF8EE, 0xF5AE, 0xF4F2,
                    0xF6B9, 0xF41B, 0xF77F, 0xF5B6, 0xF52D, 0xF5E4, 0xF171, 0xF302,
                    0xF031, 0xEFE9, 0xF22A, 0xF00B, 0xF3F0, 0xF2AD, 0xF2AD, 0xF3F0,
                    0xF00B, 0xF22A, 0xEFE9, 0xF031, 0xF302, 0xF171, 0xF5E4, 0xF52D]
    return (taps, expected)


def hex_to_s16int(hex_value):
    """Convert unsigned 16-bit to signed 16-bit.

    Args:
        hex_value : number to convert

    Returns:
        signed 16-bit result
    """
    val = int(hex_value)
    if val >= 0x8000:
        val -= 0x10000
    return val


def test_fir_init(fixture_fir):
    """Test that the fixtures accept the base parameters.

    Args:
        fixture_fir : FIR under test
    """
    fir = fixture_fir(number_of_taps=4, taps=[0, 1, 2, 3])
    assert (fir._number_of_taps == 4)
    assert (fir._taps[0] == 3)
    assert (fir._taps[1] == 2)
    assert (fir._taps[2] == 2)
    assert (fir._taps[3] == 3)


def test_fir_samples(fixture_fir, fixture_taps, input_signal):
    """Test samples work as expected.

    Args:
        fixture_fir : FIR under test
        fixture_taps : The coefficient values
        input_signal: Opcode data for the input port.
    """
    if (fixture_fir == FirFilterSymmetric_Int or
            fixture_fir == FirFilterSymmetric_ComplexInt):
        scale = 2.0**15
        round = True
        fixture_taps = [int(scale*i) for i in fixture_taps]
        if fixture_fir == FirFilterSymmetric_ComplexInt:
            input_signal = [complex(int(scale*i.real),
                                    int(scale*i.imag)) for i in input_signal]
        else:
            input_signal = [int(scale*i.real) for i in input_signal]
    else:
        scale = 1.0
        round = False

    fir = fixture_fir(number_of_taps=len(fixture_taps), taps=fixture_taps)
    actual = fir.process_samples(input_signal)
    assert (len(actual) > 0)

    expected = sig.lfilter(fixture_taps, scale, input_signal)
    # This contains the full group delay ramp up and ramp down, but "actual"
    # doesn't have the ramp down, as it is still waiting on more samples, so
    # we'll trim to length
    expected = expected[:len(actual)]
    # int versions need rounding
    if round:
        expected = np.around(expected)

    assert (len(actual) == len(expected))
    diff = np.abs(actual-expected)
    assert (max(diff) <= 1e-10)


def test_fir_example_even_taps(fixture_example_input):
    """Test samples work as expected.

    Args:
        fixture_example_input: Opcode data for the input port.
    """
    taps = [0x013a, 0x0589, 0x012c, 0xf5f1, 0xf0ee, 0x05e2, 0x3313, 0x59e3,
            0x59e3, 0x3313, 0x05e2, 0xf0ee, 0xf5f1, 0x012c, 0x0589, 0x013a]

    expected = [0x013A, 0x05F1, 0x03AC, 0xF9B4, 0xEFC2, 0xFB0B, 0x2ADE, 0x6ABF,
                0x9736, 0x91A7, 0x5F12, 0x2362, 0xFCA2, 0xFC39, 0x1CCA, 0x4BE4,
                0x7DD1, 0x9932, 0x9603, 0x7445, 0x3C06, 0x0712, 0xE26D, 0xDE0B,
                0xF9F0, 0x2631, 0x55D3, 0x6F11, 0x69EA, 0x4661, 0x0C85, 0xD624,
                0xB043, 0xAAD7, 0xC5E7, 0xF187, 0x20BE, 0x39C6, 0x349F, 0x114C,
                0xD7DB, 0xA21B, 0x7D10, 0x78AD, 0x94F8, 0xC205, 0xF2D8, 0x0DAD,
                0x0A7E, 0xE94E, 0xB228, 0x7ED9, 0x5C62, 0x5AB5, 0x79D4, 0xA9D0,
                0xDDAC, 0xFB9D, 0xFB9D, 0xDDAC, 0xA9D0, 0x79D4, 0x5AB5, 0x5C62]

    taps = [hex_to_s16int(t) for t in taps]
    expected = [hex_to_s16int(e) for e in expected]
    fixture_example_input = [hex_to_s16int(i) for i in fixture_example_input]

    fir = FirFilterSymmetric_Int(number_of_taps=len(taps), taps=taps)
    actual = fir.process_samples(fixture_example_input)
    assert (len(actual) > 0)

    assert (len(actual) == len(expected))
    assert (actual == expected)


def test_fir_example_oddtaps(fixture_example_input):
    """Test samples work as expected.

    Args:
        fixture_example_input: Opcode data for the input port.
    """
    taps = [0x0000, 0xff86, 0x0000, 0x02a2, 0x0000, 0xf6be, 0x0000, 0x2716,
            0x4000,
            0x2716, 0x0000, 0xf6be, 0x0000, 0x02a2, 0x0000, 0xff86, 0x0000]

    expected = [0x0000, 0xff86, 0xffD7, 0x0261, 0x00B8, 0xF842, 0xFDA2, 0x21A8,
                0x4AA9, 0x5284, 0x39E0, 0x1B34, 0x0614, 0xFE2A, 0x054E, 0x196A,
                0x3291, 0x4688, 0x4D2D, 0x4362, 0x2C48, 0x1004, 0xF8AC, 0xEE6A,
                0xF457, 0x075E, 0x1F59, 0x3238, 0x37D9, 0x2D1F, 0x152D, 0xF82A,
                0xE029, 0xD557, 0xDACE, 0xED78, 0x0531, 0x17E8, 0x1D7C, 0x12CF,
                0xFB06, 0xDE44, 0xC6A0, 0xBC44, 0xC24B, 0xD59E, 0xEE16, 0x01A5,
                0x0827, 0xFE7F, 0xE7CE, 0xCC38, 0xB5D2, 0xACC5, 0xB42A, 0xC8EA,
                0xE2DD, 0xF7F0, 0x0002, 0xF7F0, 0xE2DD, 0xC8EA, 0xB42A, 0xACC5]

    taps = [hex_to_s16int(t) for t in taps]
    expected = [hex_to_s16int(e) for e in expected]
    fixture_example_input = [hex_to_s16int(i) for i in fixture_example_input]

    fir = FirFilterSymmetric_Int(number_of_taps=len(taps), taps=taps)
    actual = fir.process_samples(fixture_example_input)
    assert (len(actual) > 0)

    assert (len(actual) == len(expected))
    assert (actual == expected)


def test_fir_non_symmetric_taps(fixture_example_input, fixture_non_symmetric_taps):
    """Test that non-symmetric taps are handled consistently.

    (by ignoring the first half of the taps array, and only using the 2nd half)

    Args:
        fixture_example_input : Opcode data for the input port.
        fixture_non_symmetric_taps : The coefficient values
    """
    taps, expected = fixture_non_symmetric_taps

    taps = [hex_to_s16int(t) for t in taps]
    expected = [hex_to_s16int(e) for e in expected]
    fixture_example_input = [hex_to_s16int(i) for i in fixture_example_input]

    fir = FirFilterSymmetric_Int(number_of_taps=len(taps), taps=taps)
    actual = fir.process_samples(fixture_example_input)

    assert (len(actual) == len(expected))
    assert (actual == expected)
