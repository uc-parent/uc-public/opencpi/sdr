#!/usr/bin/env python3

# Testing of code in unsigned long_long_generator.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Pytest for Unsigned Long Long Block Generator."""

import numpy
import random
import statistics
import unittest

from ocpi_block_testing.generator.unsigned_long_long_block_generator import UnsignedLongLongBlockGenerator


class TestUnsignedLongLongBlockGenerator(unittest.TestCase):
    def setUp(self):
        self.test_generator = UnsignedLongLongBlockGenerator(
            block_length=48, number_of_soak_blocks=10)
        self.seed = 42

    def _analyse_messages(self, messages):
        """Analyse messages from the generator.
        Counts the number of each type of message opcode so that it is easy to
        test for the existence and non-existence of opcodes in the results.
        For sample opcodes calculates the minimum and maximum values so that
        the range of generated values can be checked.
        """
        count = 0
        minimum = self.test_generator.UNSIGNED_LONG_LONG_MAXIMUM
        maximum = self.test_generator.UNSIGNED_LONG_LONG_MINIMUM
        opcodes = {"sample": 0, "time": 0, "sample_interval": 0,
                   "flush": 0, "discontinuity": 0, "metadata": 0, "test": 0}
        for m in messages:
            self.assertTrue(m["opcode"] in opcodes)
            opcodes[m["opcode"]] += 1
            if m["opcode"] == "sample":
                self.assertGreater(len(m["data"]), 0)
                count += len(m["data"])
                minimum = min(minimum, min(m["data"]))
                maximum = max(maximum, max(m["data"]))
        return count, minimum, maximum, opcodes

    def test_sample(self):
        messages = self.test_generator.sample(self.seed, "all_zero")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all zero
        self.assertEqual(minimum, 0)
        self.assertEqual(maximum, 0)

        messages = self.test_generator.sample(self.seed, "all_maximum")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all maximum
        self.assertEqual(
            minimum, self.test_generator.UNSIGNED_LONG_LONG_MAXIMUM)
        self.assertEqual(
            maximum, self.test_generator.UNSIGNED_LONG_LONG_MAXIMUM)

        messages = self.test_generator.sample(self.seed, "large_positive")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all large positive
        self.assertGreaterEqual(
            minimum, self.test_generator.UNSIGNED_LONG_LONG_MAXIMUM - self.test_generator.SAMPLE_NEAR_RANGE)
        self.assertLessEqual(
            maximum, self.test_generator.UNSIGNED_LONG_LONG_MAXIMUM)

        messages = self.test_generator.sample(self.seed, "near_zero")
        count, minimum, maximum, opcodes = self._analyse_messages(messages)
        # Only sample opcodes
        self.assertEqual(len(messages), opcodes["sample"])
        self.assertGreaterEqual(count, self.test_generator._block_length)
        self.assertGreaterEqual(count, self.test_generator.SAMPLE_DATA_LENGTH)
        # Values all close to zero
        self.assertEqual(minimum, 0)
        self.assertEqual(maximum, self.test_generator.SAMPLE_NEAR_RANGE)

    def test_generate_sine_wave_samples(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._generate_sine_wave_samples(n, "test")

        # At least one block of data.
        self.assertGreater(len(data), self.test_generator._block_length)
        # Over enough samples the mean of the sine waves will be close to mid value.
        mean = statistics.mean(data)
        self.assertLess(mean, self.test_generator.UNSIGNED_LONG_LONG_MID +
                        self.test_generator.TYPICAL_AMPLITUDE_MEAN)
        self.assertGreater(mean, self.test_generator.UNSIGNED_LONG_LONG_MID -
                           self.test_generator.TYPICAL_AMPLITUDE_MEAN)

    def test_generate_random_samples(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._generate_random_samples(n)

        # At least one block of data.
        self.assertGreater(len(data), self.test_generator._block_length)
        # Check range straddles the mid value.
        self.assertGreater(
            max(data), self.test_generator.UNSIGNED_LONG_LONG_MID)
        self.assertLess(min(data), self.test_generator.UNSIGNED_LONG_LONG_MID)

    def test_full_scale_random_sample_values(self):
        data = []
        for n in range(self.test_generator._block_length):
            data += self.test_generator._full_scale_random_sample_values(n)

        # At least one block of data.
        self.assertGreater(len(data), self.test_generator._block_length)
        # Check range straddles the mid value.
        self.assertGreater(
            max(data), self.test_generator.UNSIGNED_LONG_LONG_MID)
        self.assertLess(min(data), self.test_generator.UNSIGNED_LONG_LONG_MID)
