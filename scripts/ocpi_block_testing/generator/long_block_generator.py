#!/usr/bin/env python3

# Class for generating long sample input data
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Block based generator for Long samples."""

import random
import math
from opencpi.ocpi_testing.ocpi_testing.generator.long_generator import LongGeneratorDefaults
from .base_block_generator import BaseBlockGenerator


class LongBlockGenerator(BaseBlockGenerator):
    """Long protocol test data generator."""

    def __init__(self, block_length, number_of_soak_blocks=25):
        """Initialise long block generator class.

        Defines the default values for the variables that control the values
        and size of messages that are generated.

        Returns:
            An initialised LongGenerator instance.
        """
        super().__init__(block_length, number_of_soak_blocks)

        # Set variables as local as may be modified when set in the specific
        # generator. Keep the same variable names to ensure documentation
        # matches.
        self.MESSAGE_SIZE_LONGEST = LongGeneratorDefaults.MESSAGE_SIZE_LONGEST
        self.LONG_MINIMUM = LongGeneratorDefaults.LONG_MINIMUM
        self.LONG_MAXIMUM = LongGeneratorDefaults.LONG_MAXIMUM
        self.TYPICAL_AMPLITUDE_MEAN = \
            LongGeneratorDefaults.TYPICAL_AMPLITUDE_MEAN
        self.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH = \
            LongGeneratorDefaults.TYPICAL_AMPLITUDE_DISTRIBUTION_WIDTH
        self.TYPICAL_MAXIMUM_AMPLITUDE = \
            LongGeneratorDefaults.TYPICAL_MAXIMUM_AMPLITUDE

    def sample(self, seed, subcase):
        """Generate sample messages to test different supported values.

        Usually called via ``self.generate()``.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)
        number_of_samples = max(self.SAMPLE_DATA_LENGTH, self._block_length)
        if subcase == "all_zero":
            return self._split_samples_into_opcodes([0] * number_of_samples)

        elif subcase == "all_maximum":
            return self._split_samples_into_opcodes([self.LONG_MAXIMUM] * number_of_samples)

        elif subcase == "all_minimum":
            return self._split_samples_into_opcodes([self.LONG_MINIMUM] * number_of_samples)

        elif subcase == "large_positive":
            data = [0] * number_of_samples
            for index in range(number_of_samples):
                data[index] = random.randint(
                    self.LONG_MAXIMUM - self.SAMPLE_NEAR_RANGE,
                    self.LONG_MAXIMUM)
            return self._split_samples_into_opcodes(data)

        elif subcase == "large_negative":
            data = [0] * number_of_samples
            for index in range(number_of_samples):
                data[index] = random.randint(
                    self.LONG_MINIMUM,
                    self.LONG_MINIMUM + self.SAMPLE_NEAR_RANGE)
            return self._split_samples_into_opcodes(data)

        elif subcase == "near_zero":
            data = [0] * number_of_samples
            for index in range(number_of_samples):
                data[index] = random.randint(-self.SAMPLE_NEAR_RANGE,
                                             self.SAMPLE_NEAR_RANGE)
            return self._split_samples_into_opcodes(data)

        else:
            return super().sample(seed, subcase)

    def _generate_sine_wave_samples(self, number_of_samples, subcase):
        """Generate samples with sinusoidal waves.

        Combine a number of sinusoidal waves together, so that the output is
        the superposition of several sinusoidal waves.

        The number_samples_sent retains the current sample number so that repeated
        calls continue the waveform. The generator can be reset by setting
        self._number_samples_sent to zero.

        Args:
            number_of_samples (int): Number of samples to generate.
            subcase (str): Subcase for which to generate waveforms.

        Returns:
            List of samples.
        """
        (frequencies, phases, amplitudes) = self._get_wave_parameters(subcase)
        data = [0] * number_of_samples
        for index in range(len(data)):
            sample = self._number_samples_sent + index

            for frequency, phase, amplitude in zip(frequencies, phases, amplitudes):
                data[index] += int(amplitude * math.sin(2 *
                                                        math.pi * frequency * sample + phase))

        # Increment sample number for next call
        self._number_samples_sent += len(data)

        return data

    def _generate_random_samples(self, number_of_samples):
        """Generate samples with random values.

        The values generated are a subset of the whole supported range that
        longs can represent, this range size is set by
        self.LIMITED_SCALE_FACTOR.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        limited_range_min = round(
            self.LIMITED_SCALE_FACTOR * self.LONG_MINIMUM)
        limited_range_max = round(
            self.LIMITED_SCALE_FACTOR * self.LONG_MAXIMUM)

        data = [0] * number_of_samples
        for index in range(number_of_samples):
            data[index] = random.randint(limited_range_min, limited_range_max)
        return data

    def _full_scale_random_sample_values(self, number_of_samples):
        """Generate samples with random values.

        Args:
            number_of_samples (int): The number of samples to generate.

        Returns:
            List of the generated random values.
        """
        data = [0] * number_of_samples
        for index in range(number_of_samples):
            data[index] = random.randint(
                self.LONG_MINIMUM, self.LONG_MAXIMUM)
        return data
