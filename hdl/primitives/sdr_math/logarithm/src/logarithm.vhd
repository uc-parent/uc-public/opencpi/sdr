-- Pipelined HDL implementation of logarithm calculation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Logarithm calculator
-- Pipelined algorithm to calculate the logarithm of an input data value.
-- input_size_g sets the input data width
-- output_size_g sets the output data width
-- base_g sets the base value of the logarithm
-- clk_en clocks in input data on a rising clock edge when high
-- data_valid_in is the data input valid signal
-- data_in is the input data (16-bit fixed point)
-- data_valid_out is the data output valid signal
-- data_out is the logarithm result (16-bit fixed point)
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use work.log_pkg.all;

entity logarithm is
  generic (
    input_size_g  : integer := 32;
    output_size_g : integer := 32;
    base_g        : real    := 10.0
    );
  port (
    clk            : in  std_logic;
    rst            : in  std_logic;
    clk_en         : in  std_logic;
    data_valid_in  : in  std_logic;
    data_in        : in  unsigned(input_size_g - 1 downto 0);
    data_valid_out : out std_logic;
    data_out       : out signed(output_size_g - 1 downto 0)
    );
end logarithm;

architecture rtl of logarithm is
  -- Set input_width_c so it is a power of two. If input_size_g already is a
  -- power of two, increment it to the next power of two so the logarithm of
  -- the full input range can be calculated without the algorithm running out
  -- of bounds.
  constant log2_input_width_c : integer := integer(ceil(log2(
    real(input_size_g - 16) + 0.5)));
  constant input_width_c : integer           := 2**log2_input_width_c;
  -- Initialise log table
  constant table_c       : log_table_array_t := log_table_init(base_g, input_width_c);
  -- Assign signals and array sizes
  type x_reg_array_t is array (natural range <>) of unsigned(
    input_width_c + 15 downto 0);
  type y_reg_array_t is array (natural range <>) of signed(
    integer(ceil(log2(table_c(0)))) downto 0);
  signal y           : y_reg_array_t(0 to 17 + log2_input_width_c);
  signal x           : x_reg_array_t(0 to y'high-1);
  signal t           : x_reg_array_t(2 + log2_input_width_c to y'high-2);
  signal valid_store : std_logic_vector(y'high downto 0);
begin

  -- Delayed valid signal
  valid_store_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        valid_store <= (others => '0');
      elsif clk_en = '1' then
        valid_store <= valid_store(valid_store'high - 1 downto 0) &
                       data_valid_in;
      end if;
    end if;
  end process;

  -- Pipelined log algorithm
  log_pipeline_p : process(clk)
    variable i, j, n : integer;
  begin
    if rising_edge(clk) then
      if rst = '1' then
        x <= (others => (others => '0'));
        y <= (others => (others => '0'));
        t <= (others => (others => '0'));
      elsif clk_en = '1' then
        -- Stage 0
        x(0)       <= resize(data_in, x(0)'length);
        y(0)       <= to_signed(integer(table_c(0)), y(0)'length);
        -- Stage 1 to N-14
        i          := 0;
        j          := 16;
        while 2**i <= input_width_c loop
          if x(i) < '1' & to_unsigned(0, j-1) then
            x(i+1) <= resize(x(i) &
                             to_unsigned(0, input_width_c / 2**i), x(0)'length);
            y(i+1) <= y(i) - to_signed(integer(table_c(i+1)), y(0)'length);
          else
            x(i+1) <= x(i);
            y(i+1) <= y(i);
          end if;
          i := i + 1;
          j := j + input_width_c / 2**i;
        end loop;
        -- Stage N-15 to N-2
        i := i + 1;
        n := i;
        j := 1;
        while j < 8 loop
          t(i) <= x(i-1) + x(i-1)(t(0)'length - 1 downto j);
          x(i) <= x(i-1);
          y(i) <= y(i-1);
          i    := i + 1;
          j    := J + 1;
          if (t(i-1) and '1' & to_unsigned(0, t(0)'length - 1)) = 0 then
            x(i) <= t(i-1);
            y(i) <= y(i-1) - to_signed(integer(table_c(n)), y(0)'length);
          else
            x(i) <= x(i-1);
            y(i) <= y(i-1);
          end if;
          t(i) <= t(i-1);
          i    := i + 1;
          n    := n + 1;
        end loop;
        -- Stage N-1
        x(i) <= '1' & to_unsigned(0, x(0)'length - 1) - x(i-1);
        y(i) <= y(i-1);
        -- Stage N
        i    := i + 1;
        y(i) <= signed(unsigned(y(i-1)) -
                       x(i-1)(x(0)'length - 1 downto input_width_c - 1));
      end if;
    end if;
  end process;

  -- Output log result
  data_valid_out <= valid_store(valid_store'high);
  data_out       <= resize(y(y'high), output_size_g);

end rtl;
