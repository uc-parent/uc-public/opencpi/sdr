.. divider documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _divider-primitive:


Divider (``divider``)
=====================
Divides a signed integer by another signed integer.

Design
------
The mathematical representation of the implementation is given in :eq:`division_quotient-equation` and :eq:`division_remainder-equation`.

.. math::
   :label: division_quotient-equation

   q[n] = x[n] / y[n]

.. math::
   :label: division_remainder-equation

   r[n] = x[n] % y[n]

In :eq:`division_quotient-equation` and :eq:`division_remainder-equation`:

 * :math:`x[n]` and :math:`y[n]` are the input values.

 * :math:`q[n]` is the quotient output value, i.e. how many times the y can be subtracted from x.

 * :math:`r[n]` is the remainder output value, i.e. what value is left over after y is subtracted from x.


Implementation
--------------
The divider is achieved using the non-restoring divider algorithm, with a modification to allow signed values to be used.

The non-restoring divider is the equivalent of long division, but allows the remainder to be negative or positive on the completion of a single bit position.

This primitive is not pipelined, therefore takes ``data_width_g`` cycles to undertake the calculation, with an additional clock for input and outputting the data. As this primitive takes multiple clock cycles, valid flags are provided to mark when a calculation is completed.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g`` (``integer``): Width of the divider.

 * ``method_g`` (``string``): Method of division to used. (Should be one of ``truncate``, or ``floor``)

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_en`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``data_in_valid``  (``std_logic``), in : Indicates that the value at ``numerator`` and ``denominator`` are both valid.

 * ``data_in_ready``  (``std_logic``), out: Indicates that the core is capable of taking input data.

 * ``numerator``      (``signed``, ``data_width_g``), in : Numerator to use in the divider.

 * ``denominator``    (``signed``, ``data_width_g``), in : Denominator to use in the divider.

 * ``data_valid_out`` (``std_logic``), out: Indicates that ``quotient`` and ``remainder`` are valid.

 * ``quotient``       (``signed``, ``data_width_g``), out: Quotient of the divider

 * ``remainder``      (``signed``, ``data_width_g``), out: Remainder of the divider


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``divider`` are:

 * None.
