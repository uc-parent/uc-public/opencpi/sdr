.. cordic_sin_cos documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: sinusoidal wave trigonometry


.. _cordic_sin_cos-primitive:


CORDIC Sine and Cosine (``cordic_sin_cos``)
===========================================
Calculates sine and cosine using the CORDIC algorithm.

Design
------
The module uses the Coordinate Rotation Digital Computer (CORDIC) algorithm to calculate sine and cosine of an angle.

The CORDIC method is a multi-stage algorithm that uses a small look-up table to perform a variety of mathematical functions. When the CORDIC algorithm is configured in its rotational mode it can be used to calculate both sine and cosine simultaneously. The algorithm requires only shift and add operations meaning its resource requirements are very low. The look-up table required for the CORDIC method is a fraction of the size of the look-up table required to store sine and cosine values directly.

A practical description of the CORDIC algorithm can be found in the Microchip Application note AN1061.

Implementation
--------------
In this implementation each stage of the algorithm is pipelined so that after a fixed latency a new output is generated on every FPGA clock cycle. The latency is determined by the number of pipeline stages which in turn is set by the ``output_size_g`` generic. E.g. when configured to output a 16 bit output the module will have a latency of 16 clock cycles between input and output.

The module has a clock enable signal that allows it to be disabled while retaining the contents of the CORDIC pipeline.

Interface
---------

Generics
~~~~~~~~

 * ``input_width_g`` (``integer``): Sets the width of the angle_in input. Input size must match CORDIC look-up table width which is 32 bits.

 * ``output_width_g`` (``integer``): Sets the width of the ``sine_out`` and ``cosine_out`` outputs. (Maximum 32 bits).

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_enable`` (``std_logic``), in: Enable. If ``enable`` is low the module will not operate.

 * ``angle_in`` (``std_logic_vector``, ``input_width_g-1`` bits, unsigned), in: The angle for which the sine and cosine should be calculated. :math:`\texttt{angle_in} = \frac{\text{angle_radians}*2^{\texttt{input_width_g}}}{2\pi}`

 * ``gain_in`` (``std_logic_vector``, ``output_width_g-1`` bits, unsigned), in: Sets the amplitude of the sine and cosine outputs. :math:`\texttt{gain_in} = \left \lfloor \frac{\text{desired_amplitude}}{1.646760258} \right \rfloor`

 * ``sine_out`` (``std_logic_vector``, ``output_width_g-1`` bits, signed), out: The sine output of the module.

 * ``cosine_out`` (``std_logic_vector``, ``output_width_g-1`` bits, signed), out: The cosine output of the module.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cordic_sin_cos``'s use are:

When the desired amplitude is full scale (for a given output width) then additional care must be taken to avoid overflows. Overflows can occur due to small inaccuracies in the CORDIC calculations (i.e. for a 16 bit output setting *gain_in* to :math:`\left \lfloor \frac{2^{15} - 1}{1.646760258} \right \rfloor = 19897`).

This would likely mean that some sine / cosine values would be one or two counts above 32767 (or below -32768) causing an overflow. Instead the ``gain_in`` input should be set so that the final output scale is several counts below full scale. The table below shows recommended gain values to achieve (as close as possible) a full scale output.

+--------------+------------------------------------+
| Output width | ``gain_in`` setting for full scale |
+==============+====================================+
| 12 bits      | 1240                               |
+--------------+------------------------------------+
| 16 bits      | 19872                              |
+--------------+------------------------------------+
| 32 bits      | 1304065744                         |
+--------------+------------------------------------+
