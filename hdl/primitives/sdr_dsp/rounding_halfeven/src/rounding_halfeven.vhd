-- HDL Implementation of a half even rounder.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rounding_halfeven is
  generic (
    input_width_g   : integer               := 32;
    output_width_g  : integer               := 16;
    saturation_en_g : boolean               := false;
    registers_g     : positive range 1 to 4 := 1
    );
  port (
    clk            : in  std_logic;
    reset          : in  std_logic;
    clk_en         : in  std_logic;
    data_in        : in  signed(input_width_g - 1 downto 0);
    data_valid_in  : in  std_logic;
    binary_point   : in  integer range 0 to input_width_g - 1;
    data_out       : out signed(output_width_g - 1 downto 0);
    data_valid_out : out std_logic
    );
end rounding_halfeven;

architecture rtl of rounding_halfeven is

  constant exactly_half_c : signed(input_width_g downto 0) := shift_left(to_signed(1, input_width_g + 1), input_width_g);

  signal data_in_i                    : signed(input_width_g - 1 downto 0);
  signal internal                     : signed(input_width_g downto 0);
  signal fractional                   : signed(input_width_g downto 0);
  signal aligned                      : signed(input_width_g downto 0);
  signal growth                       : signed(input_width_g + 1 downto 0);
  signal pre_round_growth             : signed(input_width_g + 1 downto 0);
  signal fractional_is_half           : std_logic;
  signal pre_round_fractional_is_half : std_logic;
  signal whole_is_even                : std_logic;
  signal pre_round_whole_is_even      : std_logic;
  signal fraction_to_add              : natural range 0 to 1;
  signal plus_half                    : signed(input_width_g + 1 downto 0);
  signal rounded                      : signed(input_width_g downto 0);
  signal saturation_en                : std_logic;
  signal pre_saturate_rounded         : signed(input_width_g downto 0);
  signal pre_saturate_saturation_en   : std_logic;
  signal data_valid_pipe              : std_logic_vector(registers_g-1 downto 0);

begin

  assert binary_point < input_width_g report "binary point must be less than input_width_g" severity failure;
  assert output_width_g <= input_width_g report "output_width_g must be less or equal to input_width_g" severity failure;

  no_input_register_gen : if (registers_g <= 2) generate
    data_in_i <= data_in;
  end generate;

  -- Optional input register (Register 3)
  input_register_gen : if (registers_g > 2) generate
    input_register_p : process(clk)
    begin
      if rising_edge(clk) then
        if clk_en = '1' then
          data_in_i <= data_in;
        end if;
      end if;
    end process;
  end generate;

  -- Add 1 bit to least significant bit for common shift, even when
  -- binary_point=0
  internal           <= data_in_i & '0';
  -- Shift to remove the integer bits
  fractional         <= shift_left(internal, internal'length-(1+binary_point));
  -- Check if the fraction is exactly 0.5
  fractional_is_half <= '1' when fractional = exactly_half_c    else '0';
  -- Check if the integer is even
  whole_is_even      <= '1' when internal(1+binary_point) = '0' else '0';
  -- Shifted for single fractional least significant bit
  aligned            <= SHIFT_RIGHT(internal, binary_point);
  -- Add 1 bit to most significant bit, so +0.5 can not overflow
  growth             <= resize(aligned, aligned'length+1);

  no_multiplex_register_gen : if (registers_g <= 1) generate
    pre_round_growth             <= growth;
    pre_round_fractional_is_half <= fractional_is_half;
    pre_round_whole_is_even      <= whole_is_even;
  end generate;

  -- Optional multiplex (shift) register (Register 2)
  multiplex_register_gen : if (registers_g > 1) generate
    multiplex_register_p : process(clk)
    begin
      if rising_edge(clk) then
        if clk_en = '1' then
          pre_round_growth             <= growth;
          pre_round_fractional_is_half <= fractional_is_half;
          pre_round_whole_is_even      <= whole_is_even;
        end if;
      end if;
    end process;
  end generate;

  -- 0 if half even otherwise effectively 0.5
  fraction_to_add <= 0   when (pre_round_fractional_is_half = '1' and pre_round_whole_is_even = '1')                else 1;
  -- Add 0 or 0.5 (effectively)
  plus_half       <= pre_round_growth + fraction_to_add;
  -- Truncate, dropping fractional bit to round
  rounded         <= plus_half(plus_half'high downto 1);
  -- Enable saturation
  saturation_en   <= '1' when ((saturation_en_g) and (pre_saturate_rounded'length > output_width_g + binary_point)) else '0';

  no_pre_saturate_register_gen : if (registers_g <= 3) generate
    pre_saturate_rounded       <= rounded;
    pre_saturate_saturation_en <= saturation_en;
  end generate;

  -- pre-saturation register (Register 4)
  pre_saturate_register_gen : if (registers_g > 3) generate
    pre_saturate_register_p : process(clk)
    begin
      if rising_edge(clk) then
        if clk_en = '1' then
          pre_saturate_rounded       <= rounded;
          pre_saturate_saturation_en <= saturation_en;
        end if;
      end if;
    end process;
  end generate;

  -- Saturate, with output register (Register 1)
  saturation_p : process(clk)
  begin
    if rising_edge(clk) then
      if clk_en = '1' then
        -- Saturation check
        if pre_saturate_saturation_en = '1' then
          -- If upper bits to be discarded are +ve, set max positive
          if pre_saturate_rounded(pre_saturate_rounded'high downto output_width_g-1) > 0 then
            data_out(data_out'high)            <= '0';
            data_out(data_out'high-1 downto 0) <= (others => '1');
          -- If upper bits to be discarded are -ve, set max negative
          elsif pre_saturate_rounded(pre_saturate_rounded'high downto output_width_g-1) < -1 then
            data_out(data_out'high)            <= '1';
            data_out(data_out'high-1 downto 0) <= (others => '0');
          else
            data_out <= pre_saturate_rounded(output_width_g-1 downto 0);
          end if;
        else
          data_out <= pre_saturate_rounded(output_width_g-1 downto 0);
        end if;
      end if;
    end if;
  end process;

  -- Align valid to data (single delay)
  valid_register_gen : if (registers_g <= 1) generate
    round_valid_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          data_valid_pipe(0) <= '0';
        elsif clk_en = '1' then
          data_valid_pipe(0) <= data_valid_in;
        end if;
      end if;
    end process;
  end generate;

  -- Align valid to data (multi-delay)
  valid_pipe_gen : if (registers_g > 1) generate
    round_valid_p : process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          data_valid_pipe <= (others => '0');
        elsif clk_en = '1' then
          data_valid_pipe <= data_valid_pipe(data_valid_pipe'high-1 downto 0) & data_valid_in;
        end if;
      end if;
    end process;
  end generate;

  data_valid_out <= data_valid_pipe(data_valid_pipe'high);

end rtl;
