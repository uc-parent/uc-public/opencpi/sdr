-- HDL Implementation of Tap RAM Select package.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Converts the input tap address to seperate RAM select and RAM address
-- integers.
-- Due to the calculations used, these functions should only be used to create
-- constants.


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

package fir_tap_ram_select_pkg is

  type integer_array_t is array (natural range <>) of integer;

  -- Convert tap addressing to RAM select
  function tap_ram_select_f(addresses, stages : integer) return integer_array_t;

  -- Convert tap addressing to RAM address
  function tap_ram_address_f(addresses, stages : integer) return integer_array_t;

end package fir_tap_ram_select_pkg;

package body fir_tap_ram_select_pkg is

  -----------------------------------------------------------------------------
  -- Convert tap addressing to RAM select
  --
  -- Converts every tap address to an integer representing the RAM to be
  -- selected using the quotient of input tap-address divided by the number of
  -- stages (over which each multiplier is active).
  -----------------------------------------------------------------------------
  function tap_ram_select_f (addresses, stages : integer) return integer_array_t is
    variable temp : integer_array_t(0 to (addresses - 1));
  begin
    for i in temp'range loop
      temp(i) := i/stages;
    end loop;

    return temp;
  end function tap_ram_select_f;

  -----------------------------------------------------------------------------
  -- Convert tap addressing to RAM address
  --
  -- Converts every tap address to an integer representing the RAM address to
  -- be selected using the modulo of input tap-address divided by the number of
  -- stages (over which each multiplier is active).
  -----------------------------------------------------------------------------
  function tap_ram_address_f (addresses, stages : integer) return integer_array_t is
    variable temp_a : integer;
    variable temp_b : integer_array_t(0 to (addresses - 1));
  begin
    for i in temp_b'range loop
      temp_a    := i/stages;
      temp_b(i) := i - (temp_a * stages);
    end loop;

    return temp_b;
  end function tap_ram_address_f;

end package body fir_tap_ram_select_pkg;
