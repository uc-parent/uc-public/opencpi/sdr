.. cdc_bits_feedback documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _cdc_bits_feedback-primitive:


Clock Domain Crossing - Bits Feedback (``cdc_bits_feedback``)
=============================================================
Multiple bits with feedback clock domain crossing (CDC) synchroniser.

Design
------
Capture a multi-bit bus on the source clock domain and transfer it to a bus on the destination clock domain, using clock domain crossing (CDC) synchronisers. Indicate to the source clock domain when it is ready to accept a new input value.

Implementation
--------------

If the input enable (``src_enable``) is set high but the primitive is not ready to receive it (``src_ready`` is low) then the input will be ignored. Alternatively, if the primitive is ready, the input bus (``src_in``) will be captured in the source (``src_clk``) clock domain and the input enable will be transferred through a :ref:`pulse handshake primitive <cdc_pulse_handshake-primitive>` from the source (``src_clk``) to the destination (``dst_clk``) clock domain. Once there, it will register the captured source bus in the destination clock domain and drive it onto the output bus (``dst_out``). The :ref:`pulse handshake primitive <cdc_pulse_handshake-primitive>` will acknowledge that the bus has been sent and indicate that the primitive is ready to receive another value on the input bus. The captured input bus will be held in a steady state, from when the input enable is applied to the synchroniser until it is acknowledged, i.e. whilst the primitive is indicating it is not ready to receive a new input value. This ensures that the captured bus does not change state and, since the synchroniser takes a number of destination clock cycles to transfer the enable and then again to acknowledge it, that sufficient setup and hold time is provided for the capture of the bus in the destination clock domain.

A block diagram representation of the implementation is given in :numref:`cdc_bits_feedback-diagram`.

.. _cdc_bits_feedback-diagram:

.. figure:: cdc_bits_feedback.svg
   :alt: Block diagram of a multiple bits with feedback clock domain crossing (CDC) synchroniser implementation
   :align: center

   Block diagram of a multiple bits with feedback clock domain crossing (CDC) synchroniser implementation.

Further information on metastability and synchronisation can be found at:

 * http://www.sunburst-design.com/papers/CummingsSNUG2008Boston_CDC.pdf

 * https://www.edn.com/crossing-the-abyss-asynchronous-signals-in-a-synchronous-world/

The primitive passes signals between different clock domains and has been designed for that purpose. The primitive may contain attributes and should be paired with constraints to control their placement, optimising the MTBF through the synchronisers, and preventing the timing analyser from needlessly attempting to time the crossing signals. The Xilinx Vivado tool has the capability to report on the quality of the clock domain crossings in the design and can accept waivers for warnings generated for CDC architectures that it can not automatically verify. Platform/application developers should therefore consider adding the constraints, or similar if not using Vivado, to designs that instantiate the primitive. Refer to constraints listed below together with the documentation for all clock domain clocking primitives identified in the dependencies section, and all of their dependencies.

.. code-block::

   ### CDC constraints for cdc_bits_feedback.vhd instantiated throughout the design ###
   # Applies Max delay (controlling skew) to D input of the CDC registers. This is
   # valid because data is held steady whilst clocking into the registers using a
   # synchronised enable. The constraint applies:
   #   from the source of signals feeding the D input of the *cdc_data* registers
   #   to the destination D input of the *cdc_data* registers
   #   to a single period of the clock which drives the *cdc_data* registers
   #   Does not apply to similarly named signals, only to those in the cdc_bits_feedback primitive
   set_max_delay -datapath_only \
      -from [all_fanin -startpoints_only -flat -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hier -filter {NAME =~ *cdc_data* && FILE_NAME =~ */cdc_bits_feedback.vhd}]]] \
      -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hier -filter {NAME =~ *cdc_data* && FILE_NAME =~ */cdc_bits_feedback.vhd}]] \
      [get_property PERIOD [get_clocks -of_objects [get_cells -hier -filter {NAME =~ *cdc_data* && FILE_NAME =~ */cdc_bits_feedback.vhd}]]]
   # Waiver warning when running a CDC report
   create_waiver -type CDC -id CDC-15 \
      -to [get_pins -filter {REF_PIN_NAME == D} -of [get_cells -hier -filter {NAME =~ *cdc_data* && FILE_NAME =~ */cdc_bits_feedback.vhd}]] \
      -user "username" -description "The implementation ensures that the source data is held steady whilst the parallel bus crosses the clock domains"

Interface
---------

Generics
~~~~~~~~

 * ``width_g``  (``positive``): Width of the data buses ``src_in`` and ``dst_out``.

Ports
~~~~~

 * ``src_clk`` (``std_logic``), in: Source clock. Input registered on rising edge.

 * ``src_reset`` (``std_logic``), in: Source reset. Active high, synchronous with rising edge of source clock.

 * ``src_enable`` (``std_logic``), in: Source clock enable. Module is enabled when ``src_enable`` is high.

 * ``src_ready`` (``std_logic``), out: Output active high indicating ready to receive input data and enable on ``src_in`` and ``src_enable``.

 * ``src_in`` (``std_logic_vector``, ``width_g`` bits), in: Input bus to be resynchronised.

 * ``dst_clk`` (``std_logic``), in: Destination clock. Synchronisation registers and output registered on rising edge.

 * ``dst_reset`` (``std_logic``), in: Active high, synchronous with rising edge of clock.

 * ``dst_out`` (``std_logic_vector``, ``width_g`` bits), out: Output bus synchronised to the destination clock.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Clock Domain Crossing - Pulse handshake primitive <cdc_pulse_handshake-primitive>`.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cdc_bits_feedback`` are:

 * None
