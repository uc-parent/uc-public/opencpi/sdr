.. ram documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _ram-primitive:


Random Access Memory (``ram``)
==============================
A RAM implementation, which allows selection of standard BRAM options

Design
------
This is a RAM buffer that allows the data to be presented in a write or read first manner, along with single or simple dual port operation.

Implementation
--------------
Inputs are read into the primitive on the rising edge of ``clk``. Outputs are set on rising edge of ``clk``.

If the ``dual_port_g`` generic is set to ``true`` then the ``rd_addr`` input port is used to drive the index of the ``rd_data`` port. If this generic is set to ``false``, then only the ``wr_addr`` is used for both the read and write positions within the memory.

The primitive allows either read first (read before writing new value into the ram position), or write first (also called fall-through, this is where the newly written value is directly placed onto the read port). This is controlled through the ``write_first_g`` generic.

The ``rd_pipeline_g`` allows controlling of the number of register stages on the output of the read port of the RAM buffer. This allows controlling the latency of the output, but also has a large impact on how easy the design can be routed. This defaults to 2, which is seen as a good balance (and is generally capable of being absorbed into the RAM hardware on the FPGA).

The RAM block must be given a memory initialisation values, these are specified through the ``initialization_g`` generic, there is no nice way of defining a default value for this port, as the size depends on other generics. As such this should always be included, and if not desired, then set to: ``initialization_g := ((data_width_g * buffer_depth_g)-1 downto 0 => 'U')``.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g``          (``integer``): The bit width of the ram and data ports, (defaults to 8).

 * ``buffer_depth_g``        (``integer``): The ram-depth of the primitive (in words of ``data_width_g``), (defaults to 16).

 * ``dual_port_g``           (``boolean``): Defines whether the read and write have independent addresses (default to ``false``).

 * ``rd_pipeline_g``         (``integer``): Read data pipeline length (defaults to 2).

 * ``write_first_g``         (``boolean``): Defines the read/write ordering on assertion of ``wr_enable``, (defaults to ``false``).

 * ``initialization_g``      (``std_logic_vector``): Defines the power on initialisation values for the ram block.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``enable`` (``std_logic``), in: Primitive begins to count each rising clock edge when high.

 * ``wr_enable``  (``std_logic``), in : Data on ``wr_data`` is written to the RAM (at address ``wr_addr``) when asserted.

 * ``wr_addr``    (``std_logic_vector``), in : Address to write a value into the RAM.

 * ``wr_data``    (``std_logic_vector``), in : Data to write into RAM.

 * ``rd_addr``    (``std_logic_vector``), in : Address to read from within the RAM (only used when ``dual_port_g`` is ``true``).

 * ``rd_data``    (``std_logic_vector``), out: Data read from the ram (located at ``wr_addr`` or ``rd_addr`` depending upon ``dual_port_g``).


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``ram`` are:

 * None
