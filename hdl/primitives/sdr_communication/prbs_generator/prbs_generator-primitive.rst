.. prbs_generator documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _prbs_generator-primitive:


Pseudo-random Binary Sequence (PRBS) Generator (``prbs_generator``)
===================================================================
Generates a stream of pseudo-random boolean symbols.

Design
------
A pseudo-random binary sequence (PRBS) is a deterministic sequence that is statistically similar to a truly random sequence.

This PRBS generator is implemented using linear feedback shift registers (LFSR). A polynomial specifies which taps of the LFSR are used as feedback to generate the next input. Polynomials that generate maximum length sequences are typically used as they provide the longest period before the PRBS pattern repeats. The length of a maximum length PRBS pattern is given by :math:`\text{PRBS_LENGTH} = 2^n - 1`. Where :math:`n` is the order of the LFSR polynomial.

A block diagram representation of the implementation is given in :numref:`prbs_generator-diagram`.

.. _prbs_generator-diagram:

.. figure:: prbs_generator.svg
   :alt: rrangement of feedback registers to implement PRBS generator.
   :align: center

   Fibonacci style LSFR PRBS generator implementing the polynomial :math:`x^n + x^e + x^d + 1`.

Implementation
--------------
A Fibonacci LFSR is used in this implementation. This is sometimes referred to as a backwards counter LFSR implementation. In a Fibonacci LFSR all specified taps of the LFSR polynomial are XOR together and fed back into the LFSR as the feedback bit.

The alternative LFSR implementation is known as a Galois LFSR. Fibonacci and Galois LFSRs will output a different sequence for the same polynomial and initial value. To achieve the same sequence in a Galois LFSR as with a Fibonacci LFSR, subtract each of the tap values from the polynomial order. For example a Fibonacci LFSR with polynomial :math:`x^7 + x^6 + 1` is equivalent to a Galois LFSR with polynomial :math:`x^7 + x^1 + 1`.

The polynomial order sets the size of the shift register required to implement the LFSR. The larger the order the more space the design requires. Additionally the number of polynomial taps sets the number of registers that need to be XOR together each clock cycle. The more taps specified the slower the maximum speed of the design. For this reason both the polynomial order and taps are compile time settable properties and can not be changed during runtime.

In many applications it is desirable to set the initial starting values of the LFSR so that the starting point of the PRBS can be controlled. This initial seed can be set during runtime. When the PRBS generator is held in reset (``reset = '1'``) the initial seed is loaded into the LFSR.

In many applications it is desirable to be able to invert the PRBS pattern. In this application this is done by changing the feedback into the LFSR to use an XNOR gate rather than XOR. Specifying if the data is inverted can be done at any time during runtime.

This implementation of a PRBS generator can generate a new output every clock cycle. A clock enable input is provided to allow the PRBS output to be controlled. The LFSR state will not advance when the clock enable is low.

Interface
---------

Generics
~~~~~~~~

 * ``LFSR_WIDTH_G`` (``positive``): Sets width of LFSR.

 * ``LFSR_TAP_1_G`` (``integer``): First tap of LFSR PRBS Polynomial. Set to 0 if not used.

 * ``LFSR_TAP_2_G`` (``integer``): Second tap of LFSR PRBS Polynomial. Set to 0 if not used.

 * ``LFSR_TAP_3_G`` (``integer``): Third tap of LFSR PRBS Polynomial. Set to 0 if not used.

 * ``LFSR_TAP_4_G`` (``integer``): Fourth tap of LFSR PRBS Polynomial. Set to 0 if not used.

 * ``LFSR_TAP_5_G`` (``integer``): Fifth tap of LFSR PRBS Polynomial. Set to 0 if not used.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``clk_en`` (``std_logic``), in: Enable. If ``enable`` is low the module will not operate.

 * ``invert_i`` (``std_logic``), in: Set to '0' for XOR feedback into LFSR. Set to '1' for XNOR feedback into LFSR.

 * ``initial_i`` (``std_logic_vector``, ``LFSR_WIDTH_G``), in: Sets the value to be loaded into the LFSR when ``rst`` is '1'.

 * ``data_o`` (``std_logic_vector``, ``LFSR_WIDTH_G``), out: Outputs the contents of the LFSR.

 * ``data_msb_o`` (``std_logic``), out: Outputs the most significant bit of the LFSR. Can be used to generate a PRBS.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``prbs_generator`` are:

 * A maximum of six taps can be specified. i.e. :math:`x^a + x^b + x^c + x^d + x^e + x^f + 1`
