-- HDL Implementation of a parallel CRC generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Parallel Cyclic Redundancy Check (CRC) Generator
-- Generates a CRC result for a parallel data input stream. It has a one
-- clock cycle latency.
-- input_size_g sets the input data width
-- polynomial_g sets the polynomial
-- input_reflect_g enables / disables input data reflection
-- output_reflect_g enables / disables output crc reflection
-- clk_en clocks in input data on a rising clock edge when high
-- data_in is the parallel input data
-- seed sets the initial value
-- final_xor sets the final xor value
-- crc_out is the crc result
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity crc_parallel is
  generic (
    input_size_g     : integer          := 8;
    polynomial_g     : std_logic_vector := X"8005";
    input_reflect_g  : std_logic        := '0';
    output_reflect_g : std_logic        := '0'
    );
  port (
    clk       : in  std_logic;
    rst       : in  std_logic;
    clk_en    : in  std_logic;
    data_in   : in  std_logic_vector(input_size_g - 1 downto 0);
    seed      : in  std_logic_vector(polynomial_g'length - 1 downto 0);
    final_xor : in  std_logic_vector(polynomial_g'length - 1 downto 0);
    crc_out   : out std_logic_vector(polynomial_g'length - 1 downto 0)
    );
end crc_parallel;

architecture behavioral of crc_parallel is

  type data_array_t is array (input_size_g downto 1) of
    std_logic_vector(polynomial_g'length - 1 downto 1);
  type lfsr_array_t is array (input_size_g downto 1) of
    std_logic_vector(polynomial_g'length - 1 downto 0);
  signal data        : data_array_t;
  signal lfsr_q_high : data_array_t;
  signal lfsr_c      : lfsr_array_t;
  signal lfsr_q      : std_logic_vector(polynomial_g'length - 1 downto 0);
  signal crc_result  : std_logic_vector(polynomial_g'length - 1 downto 0);

begin

  -- Create array of data input signals
  data_i_g : for i in 1 to input_size_g generate
    data_j_g : for j in 1 to polynomial_g'high generate
      data(i)(j) <= data_in(input_size_g - i) when input_reflect_g = '0' else
                    data_in(i - 1);
    end generate data_j_g;
  end generate data_i_g;

  -- Assign array of feedback signals
  lfsr_q_high_g : for i in 1 to polynomial_g'high generate
    lfsr_q_high(1)(i) <= lfsr_q(polynomial_g'high);
  end generate lfsr_q_high_g;
  lfsr_q_high_i_g : for i in 2 to input_size_g generate
    lfsr_q_high_j_g : for j in 1 to polynomial_g'high generate
      lfsr_q_high(i)(j) <= lfsr_c(i - 1)(polynomial_g'high);
    end generate lfsr_q_high_j_g;
  end generate lfsr_q_high_i_g;

  -- XOR division
  lfsr_c(1)(0) <= data(1)(1) xor lfsr_q(polynomial_g'high);
  lfsr_c(1)(polynomial_g'high downto 1) <=
    lfsr_q(polynomial_g'high - 1 downto 0) xor
    ((data(1) xor lfsr_q_high(1)) and polynomial_g(polynomial_g'high downto 1));
  xor_g : for i in 2 to input_size_g generate
    lfsr_c(i)(0) <= data(i)(1) xor lfsr_c(i - 1)(polynomial_g'high);
    lfsr_c(i)(polynomial_g'high downto 1) <=
      lfsr_c(i - 1)(polynomial_g'high - 1 downto 0) xor
      ((data(i) xor lfsr_q_high(i)) and
       polynomial_g(polynomial_g'high downto 1));
  end generate xor_g;

  -- Generate crc
  lfsr_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        lfsr_q <= seed;
      elsif clk_en = '1' then
        lfsr_q <= lfsr_c(input_size_g);
      end if;
    end if;
  end process;

  -- Reflect output
  reflect_output_g : for i in 0 to polynomial_g'length - 1 generate
    crc_result(i) <= lfsr_q(i) when output_reflect_g = '0' else
                     lfsr_q(polynomial_g'length - 1 - i);
  end generate;

  -- Final xor
  crc_out <= crc_result xor final_xor;

end behavioral;
