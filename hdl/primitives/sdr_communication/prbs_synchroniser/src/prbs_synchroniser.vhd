-- HDL Implementation of a PRBS synchroniser.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Pseudo-random Bit Sequence Synchroniser
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library work;
use work.sdr_communication.prbs_generator;

entity prbs_synchroniser is

  generic (
    LFSR_WIDTH_G : positive := 31;
    LFSR_TAP_1_G : natural  := 28;
    LFSR_TAP_2_G : natural  := 0;
    LFSR_TAP_3_G : natural  := 0;
    LFSR_TAP_4_G : natural  := 0;
    LFSR_TAP_5_G : natural  := 0
    );
  port (
    clk                     : in  std_logic;
    reset                   : in  std_logic;
    -- PRBS gen output enabled when clk_en='1', else disabled.
    clk_en                  : in  std_logic;
    -- Set to 0 for normal output or 1 to invert output.
    invert_i                : in  std_logic;
    data_bit_i              : in  std_logic;
    check_period_i          : in  unsigned(15 downto 0);
    error_threshold_i       : in  unsigned(15 downto 0);
    bits_checked_counter_o  : out unsigned(63 downto 0);
    bit_error_counter_o     : out unsigned(63 downto 0);
    bits_received_counter_o : out unsigned(63 downto 0);
    sync_loss_counter_o     : out unsigned(63 downto 0);
    data_o                  : out std_logic_vector(7 downto 0)
    );

end prbs_synchroniser;

architecture behavioral of prbs_synchroniser is

  signal prbs_bit         : std_logic;
  signal data_bit_delayed : std_logic;
  signal delay            : std_logic_vector(LFSR_WIDTH_G downto 0);

  signal lfsr_seed  : std_logic_vector(LFSR_WIDTH_G - 1 downto 0);
  signal enable     : std_logic;
  signal load_lfsr  : std_logic;
  signal lfsr_reset : std_logic;

  signal sync_counter           : unsigned(7 downto 0);
  signal bit_counter            : unsigned(check_period_i'length - 1 downto 0);
  signal error_counter          : unsigned(error_threshold_i'length - 1 downto 0);
  signal error_counter_plus_one : unsigned(error_threshold_i'length downto 0);

  signal check_complete    : std_logic;
  signal sync_lost         : std_logic;
  signal bit_error         : std_logic;
  signal inc_sync_lost     : std_logic;
  signal accumulate_errors : std_logic;

  signal error_count_accum : unsigned(bit_error_counter_o'length - 1 downto 0);
  signal bit_count_accum   : unsigned(bits_checked_counter_o'length - 1 downto 0);
  signal sync_lost_counter : unsigned(sync_loss_counter_o'length - 1 downto 0);
  signal bit_rx_counter    : unsigned(bits_received_counter_o'length - 1 downto 0);
  signal prbs_synced       : std_logic;
  signal data_out          : std_logic_vector(2 downto 0);

  -- Interface state machine signals
  type state_t is (SYNC_LOST_s, CHECK_SYNC_s, SYNCED_s);
  signal current_state : state_t;
  signal next_state    : state_t;

begin

  ------------------------------------------------------------------------------
  -- Total input bits counter
  ------------------------------------------------------------------------------
  total_input_bits_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        bit_rx_counter <= (others => '0');
      else
        if (clk_en = '1') then
          bit_rx_counter <= bit_rx_counter + 1;
        end if;
      end if;
    end if;
  end process;

  bits_received_counter_o <= bit_rx_counter;

  ------------------------------------------------------------------------------
  -- Input delay shift register
  ------------------------------------------------------------------------------
  input_symbol_delay_p : process(clk)
  begin
    if rising_edge(clk) then
      if (clk_en = '1') then
        delay <= delay(delay'length - 2 downto 0) & data_bit_i;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- PRBS Generator
  ------------------------------------------------------------------------------
  -- Used to generate a local version of the PRBS in order to check for errors.

  -- PRBS Seed is the last LFSR_WIDTH_G inputs.
  lfsr_seed <= delay(LFSR_WIDTH_G - 1 downto 0);

  -- Output of the local PRBS generator is delayed by LFSR_WIDTH_G input samples
  -- compared with the most recent data_bit_i value.
  -- When synced data_bit_delayed should equal prbs_bit.
  data_bit_delayed <= delay(LFSR_WIDTH_G);

  -- Hold the PRBS in reset while the initial seed is loaded.
  lfsr_reset <= load_lfsr or reset;

  prbs_generator_i : prbs_generator
    generic map (
      LFSR_WIDTH_G => LFSR_WIDTH_G,
      LFSR_TAP_1_G => LFSR_TAP_1_G,
      LFSR_TAP_2_G => LFSR_TAP_2_G,
      LFSR_TAP_3_G => LFSR_TAP_3_G,
      LFSR_TAP_4_G => LFSR_TAP_4_G,
      LFSR_TAP_5_G => LFSR_TAP_5_G
      )
    port map (
      clk        => clk,
      reset      => lfsr_reset,
      clk_en     => clk_en,
      invert_i   => invert_i,
      initial_i  => lfsr_seed,
      data_o     => open,
      data_msb_o => prbs_bit
      );

  -- ---------------------------------------------------------------------------
  -- Check period complete, bit error detected and sync loss signals
  -- ---------------------------------------------------------------------------
  -- Indicates the runtime settable check period has completed
  check_complete <= '1' when bit_counter >= check_period_i else '0';

  -- Indicates if the number of errors received during the check period
  -- was greater than the runtime settable threshold.
  -- Also covers edge case where the last bit in the check_period_i is an error.
  sync_lost <= '1' when (error_counter > error_threshold_i) or ((error_counter_plus_one > error_threshold_i) and (bit_error = '1')) else '0';

  -- Compare input bit with local PRBS generator
  bit_error <= '1' when prbs_bit /= data_bit_delayed else '0';

  -- ---------------------------------------------------------------------------
  -- Control state machine
  -- ---------------------------------------------------------------------------
  -- Controls if the PRBS synchroniser is locked to the PRBS signal.
  -- Detects loss of lock.
  synchroniser_state_machine_advance_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        current_state <= SYNC_LOST_s;
      else
        if (clk_en = '1') then
          current_state <= next_state;
        end if;
      end if;
    end if;
  end process;

  synchroniser_state_machine_p : process(current_state, sync_counter, check_complete, sync_lost)
  begin
    next_state <= current_state;
    load_lfsr  <= '0';
    case current_state is

      when SYNC_LOST_s =>
        load_lfsr <= '1';

        if sync_counter = LFSR_WIDTH_G then
          next_state <= CHECK_SYNC_s;
        end if;

      when CHECK_SYNC_s =>

        if check_complete = '1' then
          if sync_lost = '1' then
            next_state <= SYNC_LOST_s;
          else
            next_state <= SYNCED_s;
          end if;
        end if;

      when SYNCED_s =>

        if check_complete = '1' and sync_lost = '1' then
          next_state <= SYNC_LOST_s;
        end if;

      when others =>
        next_state <= SYNC_LOST_s;
    end case;
  end process;

  ------------------------------------------------------------------------------
  -- Sync shift counter
  ------------------------------------------------------------------------------
  -- Counts the number of samples that have been shifted into the PRBS
  -- generator's initial_state register.
  sync_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        sync_counter <= (others => '0');
      else
        if (clk_en = '1') then
          if (load_lfsr = '0') then
            sync_counter <= (others => '0');
          else
            sync_counter <= sync_counter + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Check period error and bit counters
  ------------------------------------------------------------------------------
  bit_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        bit_counter <= (0 => '1', others => '0');
      else
        if (clk_en = '1') then
          if (current_state = SYNC_LOST_s or check_complete = '1') then
            bit_counter <= (0 => '1', others => '0');
          else
            bit_counter <= bit_counter + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  error_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        error_counter <= (others => '0');
      else
        if (clk_en = '1') then
          if (current_state = SYNC_LOST_s or check_complete = '1') then
            error_counter <= (others => '0');
          elsif bit_error = '1' then
            error_counter <= error_counter + 1;
          end if;
        end if;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------
  -- Overall error and bit counters
  ------------------------------------------------------------------------------
  -- Only add errors to the main totals when in the SYNCED state or
  -- when in the CHECK_SYNC state and the sync has been successful.
  error_counter_plus_one <= resize(error_counter, error_counter_plus_one'length) + 1;
  accumulate_errors      <= '1' when check_complete = '1' and (current_state = SYNCED_s or next_state = SYNCED_s) else '0';

  error_accumulator_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        error_count_accum <= (others => '0');
        bit_count_accum   <= (others => '0');
      else
        if (clk_en = '1' and accumulate_errors = '1') then

          bit_count_accum <= bit_count_accum + bit_counter;

          if bit_error = '1' then
            -- If there is an error in last bit of the check period it will not
            -- yet be in the error_counter register so add an additional error.
            error_count_accum <= error_count_accum + error_counter_plus_one;
          else
            error_count_accum <= error_count_accum + error_counter;
          end if;

        end if;
      end if;
    end if;
  end process;

  bit_error_counter_o    <= error_count_accum;
  bits_checked_counter_o <= bit_count_accum;

  ------------------------------------------------------------------------------
  -- Detect loss of sync
  ------------------------------------------------------------------------------
  -- Only increase sync_lost_counter when going from locked state to unlocked.
  inc_sync_lost <= '1' when current_state = SYNCED_s and next_state <= SYNC_LOST_s else '0';

  sync_lost_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        sync_lost_counter <= (others => '0');
      else
        if (clk_en = '1' and inc_sync_lost = '1') then
          sync_lost_counter <= sync_lost_counter + 1;
        end if;
      end if;
    end if;
  end process;

  sync_loss_counter_o <= sync_lost_counter;

  ------------------------------------------------------------------------------
  -- Output
  ------------------------------------------------------------------------------

  data_out <= "000" when current_state = SYNC_LOST_s else
              "100" when current_state = CHECK_SYNC_s else "01" & bit_error;

  data_out_reg_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        data_o <= (others => '0');
      else
        if (clk_en = '1') then
          data_o <= (0 => data_out(0), 1 => data_out(1), 2 => data_out(2), others => '0');
        end if;
      end if;
    end if;
  end process;

end behavioral;
