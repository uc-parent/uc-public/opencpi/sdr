-- Message length limiter primitive.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity message_length_limiter is
  generic (
    DATA_WIDTH_G         : integer          := 8;
    OPCODE_WIDTH_G       : integer          := 3;
    BYTE_ENABLE_WIDTH_G  : integer          := 1;
    MAX_MESSAGE_LENGTH_G : integer          := 2048;
    LIMIT_OPCODE_G       : std_logic_vector := "000"
    );
  port (
    clk                        : in  std_logic;
    reset                      : in  std_logic;
    enable                     : in  std_logic;  -- High when output is ready
    -- Output interface signals
    output_som                 : in  std_logic;
    output_eom                 : in  std_logic;
    output_valid               : in  std_logic;
    output_give                : in  std_logic;
    output_byte_enable         : in  std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
    output_opcode              : in  std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
    output_data                : in  std_logic_vector(DATA_WIDTH_G - 1 downto 0);
    -- Limited output interface signals
    output_limited_som         : out std_logic;
    output_limited_eom         : out std_logic;
    output_limited_valid       : out std_logic;
    output_limited_give        : out std_logic;
    output_limited_byte_enable : out std_logic_vector(BYTE_ENABLE_WIDTH_G - 1 downto 0);
    output_limited_opcode      : out std_logic_vector(OPCODE_WIDTH_G - 1 downto 0);
    output_limited_data        : out std_logic_vector(DATA_WIDTH_G - 1 downto 0)
    );
end message_length_limiter;

architecture rtl of message_length_limiter is

  constant message_counter_size_c : integer := integer(ceil(log2(real(MAX_MESSAGE_LENGTH_G))));

  signal give                   : std_logic;
  signal squash_late_eom        : std_logic;
  signal early_som              : std_logic;
  signal message_length_counter : unsigned(message_counter_size_c - 1 downto 0);

begin

  -- Count the length of the stream message
  message_length_counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        message_length_counter <= (others => '0');
        squash_late_eom        <= '0';
      elsif enable = '1' and output_give = '1' and output_opcode = LIMIT_OPCODE_G then
        if output_eom = '1' then
          message_length_counter <= (others => '0');
          squash_late_eom        <= '0';
        elsif output_valid = '1' then
          if message_length_counter = MAX_MESSAGE_LENGTH_G - 1 then
            message_length_counter <= (others => '0');
            -- Edge case where EOM is late
            squash_late_eom        <= '1';
          else
            message_length_counter <= message_length_counter + 1;
            squash_late_eom        <= '0';
          end if;
        end if;
      end if;
    end if;
  end process;
  -- Detect early SOM signal to avoid outputting two SOM flags on output.
  -- This is because the first valid output is always marked as SOM, so we need
  -- to know if a SOM has already been sent.
  early_som_capture_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        early_som <= '0';
      elsif enable = '1' and output_give = '1' and output_opcode = LIMIT_OPCODE_G then
        if output_som = '1' and output_eom = '0' and output_valid = '0' then
          early_som <= '1';
        elsif output_valid = '1' or output_eom = '1' then
          early_som <= '0';
        end if;
      end if;
    end if;
  end process;
  -- Register the output signal
  output_reg_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        give <= '0';
      elsif enable = '1' then
        -- If stream message and late EOM when we just ended the
        -- message due to it exceeding the length, then squash the message.
        if output_give = '1' and output_valid = '0' and
          output_opcode = LIMIT_OPCODE_G and
          output_som = '0' and output_eom = '1' and
          squash_late_eom = '1' then
          give <= '0';
        -- Otherwise pass message through
        else
          give <= output_give;
        end if;
        -- If stream message, then limit the message length to a max of
        -- MAX_MESSAGE_LENGTH_G, otherwise pass message through
        if output_opcode = LIMIT_OPCODE_G then
          -- SOM set when counter is 0 as counter resets on EOM or
          -- when the message is limited (unless an early SOM was sent).
          if message_length_counter = 0 and early_som = '0' and output_valid = '1' then
            output_limited_som <= '1';
          elsif message_length_counter = 0 and early_som = '1' and output_valid = '1' then
            output_limited_som <= '0';
          else
            output_limited_som <= output_som;
          end if;
          -- Force EOM when message length limit reached.
          if message_length_counter = MAX_MESSAGE_LENGTH_G - 1 then
            output_limited_eom <= '1';
          else
            -- But keep resampled message size if message is not oversized.
            output_limited_eom <= output_eom;
          end if;
        else
          -- Otherwise pass through SOM/EOM.
          output_limited_som <= output_som;
          output_limited_eom <= output_eom;
        end if;
        -- Pass through all valid signals
        output_limited_valid       <= output_valid;
        output_limited_data        <= output_data;
        output_limited_byte_enable <= output_byte_enable;
        output_limited_opcode      <= output_opcode;
      end if;
    end if;
  end process;
  -- Only give when output is ready
  output_limited_give <= give and enable;

end rtl;
